﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vm-glass.aspx.cs" Inherits="WebUI.vm_glass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
         <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Drinks</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
</head>
<body>
    <form id="form1" runat="server">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        
        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li>
        		</ul>
        	</nav>
        </header>
        
        <div class="wrapper">
	        <section class="content virtualMixology">
	        	
	        	<div class="carousel-holder">
	        		<div class="glowing-lights-holder"></div>
	        		<div class="mixHolder">
	        			<div class="bottleHolder"></div>
	        			<div class="glassHolder"></div>
	        			<div class="garnishHolder"></div>
	        		</div>
		        	<div class="progress step2">
                        <div class="progressContent">As with all works of art, cocktails deserve the platform that shows them off as they deserve.</div>
		        	</div>
		        	<div class="glassDescriptionHolder desktop">
		        		<div class="glassName"></div>
		        		<div class="divider-white"></div>
		        		<div class="glassDescription"></div>
		        	</div>
		        	
	        		<div class="head">
			        	<%--<div class="btn-left" id="carouselItem-slider-prev"><div class="backLabel glass desktop"></div></div>
			        	<div class="btn-right" id="carouselItem-slider-next"><div class="nextLabel glass desktop"></div></div>--%>
                        <div class="icons-holder">
                            <div class="glass-icon" id="glass-icon1"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M11.7,4.5l1.8,30.4c0,0,0.2,1.2,4.1,1.5c0,0,2.7,0.1,3.7,0c3.9-0.4,4.1-1.5,4.1-1.5l1.8-30.4"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M11.7,4.5l1.8,30.4c0,0,0.2,1.2,4.1,1.5c0,0,2.7,0.1,3.7,0c3.9-0.4,4.1-1.5,4.1-1.5l1.8-30.4"/></svg><div class="icon-label">Collins</div></div>
                            <div class="glass-icon" id="glass-icon2"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M28,14c0,0-2.3,18.5-2.2,20.4c0,0-1.9,0.9-5.5,0.9H18c-3.6,0-5.5-0.9-5.5-0.9c0.1-1.9-2.2-20.4-2.2-20.4"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M28,14c0,0-2.3,18.5-2.2,20.4c0,0-1.9,0.9-5.5,0.9H18c-3.6,0-5.5-0.9-5.5-0.9c0.1-1.9-2.2-20.4-2.2-20.4"/></svg><div class="icon-label">Lowball</div></div>
                            <div class="glass-icon" id="glass-icon3"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M26.4,4.4c0,0,2.8,8.3,3.3,10.5c0.5,2.2,0.7,5.1-2.3,7.3s-5.2,3.7-6.2,5.2c-0.2,0.3-0.4,0.7-0.4,1.4c-0.1,0.7,0,2.3,0,2.3s-0.1,1.8,2.1,2.5c2.1,0.7,3.8,0.9,3.8,0.9s1.8,0.7-0.1,1.1c-1.9,0.5-5.2,0.6-6,0.6h-1.6c-0.9,0-4.2-0.2-6-0.6c-1.9-0.5-0.1-1.1-0.1-1.1s1.7-0.3,3.8-0.9c2.1-0.6,2.1-2.5,2.1-2.5s0.1-1.6,0-2.3c-0.1-0.7-0.2-1.1-0.4-1.4c-1-1.4-3.2-2.9-6.2-5.2S9.3,17,9.8,14.9c0.5-2.2,3.3-10.5,3.3-10.5"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M26.4,4.4c0,0,2.8,8.3,3.3,10.5c0.5,2.2,0.7,5.1-2.3,7.3s-5.2,3.7-6.2,5.2c-0.2,0.3-0.4,0.7-0.4,1.4c-0.1,0.7,0,2.3,0,2.3s-0.1,1.8,2.1,2.5c2.1,0.7,3.8,0.9,3.8,0.9s1.8,0.7-0.1,1.1c-1.9,0.5-5.2,0.6-6,0.6h-1.6c-0.9,0-4.2-0.2-6-0.6c-1.9-0.5-0.1-1.1-0.1-1.1s1.7-0.3,3.8-0.9c2.1-0.6,2.1-2.5,2.1-2.5s0.1-1.6,0-2.3c-0.1-0.7-0.2-1.1-0.4-1.4c-1-1.4-3.2-2.9-6.2-5.2S9.3,17,9.8,14.9c0.5-2.2,3.3-10.5,3.3-10.5"/></svg><div class="icon-label">Snifter</div></div>
                            <div class="glass-icon" id="glass-icon4"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M31.5,2.7c0,0,2.3,7.6-6.1,11.3c0,0,0.8,3.5-3,5.8c0,0-1.1,0.6-1.1,1.3c0,0-0.4,11.8,0,12.7c0.3,0.8,0.6,1.8,2.5,2.3c1.9,0.5,3.6,0.9,3.6,0.9s2.3,0.8-0.6,1.3s-6.3,0.4-6.3,0.4H20c0,0-3.3,0.1-6.3-0.4c-3-0.4-0.6-1.3-0.6-1.3s1.8-0.4,3.6-0.9c1.8-0.5,2.2-1.5,2.5-2.3c0.4-0.8,0-12.7,0-12.7c0-0.8-1.1-1.3-1.1-1.3c-3.8-2.3-3-5.8-3-5.8C6.6,10.4,9,2.7,9,2.7"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M31.5,2.7c0,0,2.3,7.6-6.1,11.3c0,0,0.8,3.5-3,5.8c0,0-1.1,0.6-1.1,1.3c0,0-0.4,11.8,0,12.7c0.3,0.8,0.6,1.8,2.5,2.3c1.9,0.5,3.6,0.9,3.6,0.9s2.3,0.8-0.6,1.3s-6.3,0.4-6.3,0.4H20c0,0-3.3,0.1-6.3-0.4c-3-0.4-0.6-1.3-0.6-1.3s1.8-0.4,3.6-0.9c1.8-0.5,2.2-1.5,2.5-2.3c0.4-0.8,0-12.7,0-12.7c0-0.8-1.1-1.3-1.1-1.3c-3.8-2.3-3-5.8-3-5.8C6.6,10.4,9,2.7,9,2.7"/></svg><div class="icon-label">Cocktail</div></div>
                            <div class="glass-icon" id="glass-icon5"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M23.9,3.5c0,0,1.1,12.4-1.9,15.4c0,0-1,1.3-1.2,3.1s-0.6,6.7-0.4,10.9c0,0,0.3,1.6,1.4,1.9s2.2,0.6,2.5,0.6c0.4,0.1,1.6,0.6-0.5,0.8c-2,0.3-2.8,0.3-3.5,0.3h-0.8c-0.7,0-1.5,0-3.5-0.3c-2-0.3-0.9-0.8-0.5-0.8s1.5-0.3,2.5-0.6c1.1-0.4,1.4-1.9,1.4-1.9c0.3-4.2-0.2-9-0.4-10.9S17.9,19,17.9,19c-3-3-1.9-15.4-1.9-15.4"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M23.9,3.5c0,0,1.1,12.4-1.9,15.4c0,0-1,1.3-1.2,3.1s-0.6,6.7-0.4,10.9c0,0,0.3,1.6,1.4,1.9s2.2,0.6,2.5,0.6c0.4,0.1,1.6,0.6-0.5,0.8c-2,0.3-2.8,0.3-3.5,0.3h-0.8c-0.7,0-1.5,0-3.5-0.3c-2-0.3-0.9-0.8-0.5-0.8s1.5-0.3,2.5-0.6c1.1-0.4,1.4-1.9,1.4-1.9c0.3-4.2-0.2-9-0.4-10.9S17.9,19,17.9,19c-3-3-1.9-15.4-1.9-15.4"/></svg><div class="icon-label">Champagne flute</div></div>
                            <div class="glass-icon" id="glass-icon6"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M31.1,5c0,0,2.7,5.7-3.9,9.8c-1.1,0.7-2.7,1.6-4.9,2.3c0,0-1.5,0.1-1.5,3.2s0.1,11.3,0.1,11.3s0.3,1.8,2.4,2.4c1.1,0.3,2.7,0.7,3.6,0.8s2,1.1-0.4,1.3c-2.5,0.2-4.5,0.4-5.6,0.3h-1.7c-1.1,0.1-3.1,0-5.6-0.3c-2.5-0.3-1.3-1.2-0.4-1.3c0.8-0.1,2.5-0.6,3.6-0.8c2.1-0.6,2.4-2.4,2.4-2.4s0.1-8.3,0.1-11.3c0-3-1.5-3.2-1.5-3.2c-2.2-0.7-3.8-1.6-4.9-2.3C6.2,10.7,8.9,5,8.9,5"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M31.1,5c0,0,2.7,5.7-3.9,9.8c-1.1,0.7-2.7,1.6-4.9,2.3c0,0-1.5,0.1-1.5,3.2s0.1,11.3,0.1,11.3s0.3,1.8,2.4,2.4c1.1,0.3,2.7,0.7,3.6,0.8s2,1.1-0.4,1.3c-2.5,0.2-4.5,0.4-5.6,0.3h-1.7c-1.1,0.1-3.1,0-5.6-0.3c-2.5-0.3-1.3-1.2-0.4-1.3c0.8-0.1,2.5-0.6,3.6-0.8c2.1-0.6,2.4-2.4,2.4-2.4s0.1-8.3,0.1-11.3c0-3-1.5-3.2-1.5-3.2c-2.2-0.7-3.8-1.6-4.9-2.3C6.2,10.7,8.9,5,8.9,5"/></svg><div class="icon-label">Champagne saucer</div></div>
                            <div class="glass-icon" id="glass-icon7"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M30.7,6c0,0-8,11.5-8.7,12.2c-0.7,0.7-1.1,1.6-1.1,2.5V31c0,0,0,1.9,2.2,2.4c2.2,0.4,3.7,0.6,3.8,1c0.1,0.4,0,1.3-5.3,1.3H19c-5.6,0-5.8-1-5.7-1.3c0.1-0.4,1.6-0.5,3.8-1c2.2-0.4,2.2-2.4,2.2-2.4V20.7c0-0.9-0.4-1.8-1.1-2.5C17.5,17.5,9.5,6,9.5,6"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M30.7,6c0,0-8,11.5-8.7,12.2c-0.7,0.7-1.1,1.6-1.1,2.5V31c0,0,0,1.9,2.2,2.4c2.2,0.4,3.7,0.6,3.8,1c0.1,0.4,0,1.3-5.3,1.3H19c-5.6,0-5.8-1-5.7-1.3c0.1-0.4,1.6-0.5,3.8-1c2.2-0.4,2.2-2.4,2.2-2.4V20.7c0-0.9-0.4-1.8-1.1-2.5C17.5,17.5,9.5,6,9.5,6"/></svg><div class="icon-label">Martini</div></div>
                            <div class="glass-icon" id="glass-icon8"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M25.1,14.9c0,0-0.7,1.6-0.9,3.1s-0.8,2.8-0.5,15.3c0,0-0.8,0.7-3.2,0.8h-1.9c-2.4-0.1-3.2-0.8-3.2-0.8c0.3-12.4-0.3-13.7-0.5-15.2c-0.2-1.5-0.9-3.1-0.9-3.1"/><path fill="none" class="icon-over" stroke="#F47A20" stroke-miterlimit="10" d="M25.1,14.9c0,0-0.7,1.6-0.9,3.1s-0.8,2.8-0.5,15.3c0,0-0.8,0.7-3.2,0.8h-1.9c-2.4-0.1-3.2-0.8-3.2-0.8c0.3-12.4-0.3-13.7-0.5-15.2c-0.2-1.5-0.9-3.1-0.9-3.1"/></svg><div class="icon-label">Shot glass</div></div>
                        </div>
	        		</div>
	        	</div>
	        	<div class="glassDescriptionHolder mobile">
	        		<div class="glassName"></div>
	        		<div class="divider-black"></div>
	        		<div class="glassDescription"></div>
	        	</div>
	        	<div class="stripes">        		
		        	<a href="" class="btn"><p>Next Step</p></a>
	        	</div>
	        	
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>
                
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/vendor/gs/utils/SplitText.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vm.js"></script>
        <script src="js/vm-glass.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-66449050-1','auto');ga('send','pageview');
        </script>
    </form>
</body>
</html>
