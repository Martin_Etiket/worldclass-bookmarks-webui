﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Email-Image.aspx.cs" Inherits="WebUI.Email_Image" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Virtual Mixology</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <section class="content virtualMixology">
                <div class="carousel-holder">
                    <div class="mixHolder">
                        <div class="bottleHolder"></div>
                        <div class="colourHolder"></div>
                        <div class="glassHolder"></div>
                        <div class="garnishHolder"></div>
                    </div>
                </div>
                <div class="nameHolder emailer">
                    <div id="cocktailName" runat="server" class="cocktailName"></div>
                    <div class="divider-black"></div>
                    <div class="selector hidden namePage" id="ingredient0">
                        <div class="quantity"></div>
                    </div>
                    <div class="selector hidden namePage" id="ingredient1">
                        <div class="quantity"></div>
                    </div>
                    <div class="selector hidden namePage" id="ingredient2">
                        <div class="quantity"></div>
                    </div>
                    <div class="selector hidden namePage" id="ingredient3">
                        <div class="quantity"></div>
                    </div>
                    <div class="selector" id="ingredient4">
                        <div class="quantity"></div>
                    </div>
                </div>
                
            </section>
        </div>       

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vm.js"></script>
        <script src="js/vm-share.js"></script>
    </form>
</body>
</html>
