﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vm-garnish.aspx.cs" Inherits="WebUI.vm_garnish" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Drinks</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
</head>
<body>
    <form id="form1" runat="server">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        
        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li>
        		</ul>
        	</nav>
        </header>
        
        <div class="wrapper">
	        <section class="content virtualMixology">
	        	
	        	<div class="carousel-holder">
	        		<div class="glowing-lights-holder"></div>
	        		<div class="mixHolder">
	        			<div class="bottleHolder"></div>
	        			<div class="colourHolder"></div>
	        			<div class="glassHolder"></div>
                        <div class="bubblesHolder"></div>
	        			<div class="garnishHolder"></div>
	        		</div>
		        	<div class="progress step4">
		        		 <div class="progressContent">Dress it up. <br>Pick your garnish.</div>
		        	</div>
		        	<div class="glassDescriptionHolder desktop">
		        		<div class="glassName"></div>
		        		<div class="divider-white"></div>
		        		<div class="glassDescription"></div>
		        	</div>
		        	
	        		<div class="head">
			        <%--	<div class="btn-left" id="carouselItem-slider-prev"><div class="backLabel garnish desktop"></div></div>
			        	<div class="btn-right" id="carouselItem-slider-next"><div class="nextLabel garnish desktop"></div></div>--%>
                        <div class="icons-holder">
                            <div class="glass-icon" id="garnish-icon1"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                                                            <g>
	                                                                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M22.1,11.9c0,0,2-2.9,5.7-3.8c0,0,2.1-2.6,3.6-2.7
		                                                                            c1.5-0.1,1.1,3.1-0.4,6.5c0,0-1.2,4.9-1.9,5.4c-1.3,1,3.4,0.6,6.1,6.4c0,0,2.9,0.6,2.2,2c-0.6,1.4-9.4,1.1-11.6,0
		                                                                            c0,0-1.1,5.4-1.9,5.7c0,0-0.4,1.9-0.9,2.6c0,0,0.9,2.8-0.1,4c0,0-1.1,1.6-2.6-3.8c0,0-3.5-4.1-3.5-7.9c0,0,0.9-0.6-2.5-0.6
		                                                                            S5.9,25,4.6,25.1c-1.3,0.1-3.6-0.5,0-2.5s6.1-4.9,7.4-5.2c1.4-0.4,2.6-0.5,2.6-0.5S12.1,10,11.5,6.6c0,0-1.8-4.8,1.9-1.4
		                                                                            S18.9,8.7,22.1,11.9z"/>
	                                                                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M22.2,17.8c0,0,0.5-0.8,1.5-1c0,0,0.6-0.6,0.9-0.7
		                                                                            c0.4-0.1,0.3,0.8-0.1,1.7c0,0-0.4,1.3-0.5,1.4c-0.4,0.3,0.9,0.1,1.6,1.7c0,0,0.8,0.1,0.6,0.5c-0.1,0.4-2.4,0.3-3,0
		                                                                            c0,0-0.3,1.4-0.5,1.5c0,0-0.1,0.5-0.2,0.6c0,0,0.2,0.7-0.1,1.1c0,0-0.3,0.4-0.7-1c0,0-0.9-1.1-0.9-2.1c0,0,0.2-0.1-0.6-0.1
		                                                                            s-2.3-0.2-2.6-0.2c-0.3,0-0.9-0.1,0-0.6s1.6-1.3,1.9-1.4c0.4-0.1,0.7-0.1,0.7-0.1s-0.7-1.8-0.9-2.7c0,0-0.4-1.3,0.5-0.4
		                                                                            C20.9,16.9,21.4,16.9,22.2,17.8z"/>
                                                                            </g>
                                                                            <g>
	                                                                            <path fill="none" class="icon-over" stroke="#F47820" stroke-miterlimit="10" d="M22.1,11.9c0,0,2-2.9,5.7-3.8c0,0,2.1-2.6,3.6-2.7
		                                                                            c1.5-0.1,1.1,3.1-0.4,6.5c0,0-1.2,4.9-1.9,5.4c-1.3,1,3.4,0.6,6.1,6.4c0,0,2.9,0.6,2.2,2c-0.6,1.4-9.4,1.1-11.6,0
		                                                                            c0,0-1.1,5.4-1.9,5.7c0,0-0.4,1.9-0.9,2.6c0,0,0.9,2.8-0.1,4c0,0-1.1,1.6-2.6-3.8c0,0-3.5-4.1-3.5-7.9c0,0,0.9-0.6-2.5-0.6
		                                                                            S5.9,25,4.6,25.1c-1.3,0.1-3.6-0.5,0-2.5s6.1-4.9,7.4-5.2c1.4-0.4,2.6-0.5,2.6-0.5S12.1,10,11.5,6.6c0,0-1.8-4.8,1.9-1.4
		                                                                            S18.9,8.7,22.1,11.9z"/>
	                                                                            <path fill="none" class="icon-over" stroke="#F47820" stroke-miterlimit="10" d="M22.2,17.8c0,0,0.5-0.8,1.5-1c0,0,0.6-0.6,0.9-0.7
		                                                                            c0.4-0.1,0.3,0.8-0.1,1.7c0,0-0.4,1.3-0.5,1.4c-0.4,0.3,0.9,0.1,1.6,1.7c0,0,0.8,0.1,0.6,0.5c-0.1,0.4-2.4,0.3-3,0
		                                                                            c0,0-0.3,1.4-0.5,1.5c0,0-0.1,0.5-0.2,0.6c0,0,0.2,0.7-0.1,1.1c0,0-0.3,0.4-0.7-1c0,0-0.9-1.1-0.9-2.1c0,0,0.2-0.1-0.6-0.1
		                                                                            s-2.3-0.2-2.6-0.2c-0.3,0-0.9-0.1,0-0.6s1.6-1.3,1.9-1.4c0.4-0.1,0.7-0.1,0.7-0.1s-0.7-1.8-0.9-2.7c0,0-0.4-1.3,0.5-0.4
		                                                                            C20.9,16.9,21.4,16.9,22.2,17.8z"/>
                                                                            </g>
                                                                       </svg>
                                <div class="icon-label">Starfruit</div>
                            </div>
                            <div class="glass-icon" id="garnish-icon2"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                        <g>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M7.6,13.6C10.3,10.8,14.1,9,18.3,9C26.4,9,33,15.6,33,23.7
		                                                                        c0,4.3-1.8,8.1-4.7,10.8L7.6,13.6z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M10,16c0,0,7.4-7.3,15.6,0.9c3.1,3.1,3.9,5.9,3.6,8.4
		                                                                        c-0.4,4.1-3.4,6.9-3.4,6.9"/>
	                                                                        <polyline fill="none" stroke="#FFFFFF" stroke-miterlimit="10" points="29.2,25.1 18.1,24.2 25.6,16.8 	"/>
	                                                                        <line fill="none" stroke="#FFFFFF" stroke-miterlimit="10" x1="18.1" y1="24.2" x2="17.2" y2="13"/>
                                                                        </g>
                                                                        <g>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M7.6,13.6C10.3,10.8,14.1,9,18.3,9C26.4,9,33,15.6,33,23.7
		                                                                        c0,4.3-1.8,8.1-4.7,10.8L7.6,13.6z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M10,16c0,0,7.4-7.3,15.6,0.9c3.1,3.1,3.9,5.9,3.6,8.4
		                                                                        c-0.4,4.1-3.4,6.9-3.4,6.9"/>
	                                                                        <polyline class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" points="29.2,25.1 18.1,24.2 25.6,16.8 	"/>
	                                                                        <line class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" x1="18.1" y1="24.2" x2="17.2" y2="13"/>
                                                                        </g>
                                                                       </svg>
                                <div class="icon-label">Lemon Slice</div></div>
                            <div class="glass-icon" id="garnish-icon3"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                        <g>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M6.7,14.2c0,0,5.4-3.3,9.1-2.8s7,1.3,9.7,5c0,0,0.2,0.8,0.2,1.8
		                                                                        c0,0,0.3-6.9-5.6-10.9c0,0-4.1-2.9-8.6-1.9c0,0-1.6-0.6-2.7,2.1S5.6,14,6.7,14.2z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M21.1,12.7c0,0,1,3.3-0.5,6.9"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M19.9,26.4c0,0-2.7-0.8-3.4-2.3c-0.7-1.4,1.2-2.7,1.9-3.2
		                                                                        c0.7-0.5,4.7-4.1,9.6-2c0,0,2.6,1.4,2.9,3.6c0,0-4.7-2.8-7.8-0.4S19.9,26.4,19.9,26.4z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M30.9,22.6c0,0,1.6,1.5-0.3,6s-2.6,6.2-2.4,6.6"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M27.9,21.4c0,0,2.7,2.1,1.1,7.3c-1.7,5.3-2.4,5.6-1.9,6.1l0.1,1.1
		                                                                        c4.2,1.9,6.9-3.6,6.9-3.6s-5.2,3.6-7,2.5"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M12.5,11.7c0,0-0.3,1.1,1.4,1.8l-3.2,2.4c0,0-2.9-0.3-4-1.8"/>
                                                                        </g>
                                                                        <g>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M6.7,14.2c0,0,5.4-3.3,9.1-2.8s7,1.3,9.7,5c0,0,0.2,0.8,0.2,1.8
		                                                                        c0,0,0.3-6.9-5.6-10.9c0,0-4.1-2.9-8.6-1.9c0,0-1.6-0.6-2.7,2.1S5.6,14,6.7,14.2z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M21.1,12.7c0,0,1,3.3-0.5,6.9"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M19.9,26.4c0,0-2.7-0.8-3.4-2.3c-0.7-1.4,1.2-2.7,1.9-3.2
		                                                                        c0.7-0.5,4.7-4.1,9.6-2c0,0,2.6,1.4,2.9,3.6c0,0-4.7-2.8-7.8-0.4S19.9,26.4,19.9,26.4z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M30.9,22.6c0,0,1.6,1.5-0.3,6s-2.6,6.2-2.4,6.6"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M27.9,21.4c0,0,2.7,2.1,1.1,7.3c-1.7,5.3-2.4,5.6-1.9,6.1l0.1,1.1
		                                                                        c4.2,1.9,6.9-3.6,6.9-3.6s-5.2,3.6-7,2.5"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M12.5,11.7c0,0-0.3,1.1,1.4,1.8l-3.2,2.4c0,0-2.9-0.3-4-1.8"/>
                                                                        </g>
                                                                       </svg>
                                <div class="icon-label">Lemon Twist</div></div>
                            <div class="glass-icon" id="garnish-icon4"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"
                                                                        <g>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M38,28.6c0,0-2.1,1.4-3.1,1.6l0.6,0.7c0,0-3.1,0.6-3.9,0.4l0.6,0.8
		                                                                        c0,0-3.4,0.7-5.4-0.5l0.7,1c0,0-2.3-0.1-4.6-1.6l0.2,1.2c0,0-2.6-0.6-4.1-3.6s-0.3-3.9,2-4.1c2.3-0.2,10.2,1.6,11.5,2.4
		                                                                        c0,0-6.6-3.4-12.9-2.8c0,0-1.9-1-0.1-2.9s2.6-2.2,4.6-2.5l-0.4,0.7c0,0,3.2-0.6,5.7,0.1l-0.7,0.7c0,0,3.5,0.4,4.9,1.4l-0.8,0.6
		                                                                        c0,0,2.6,0.9,3.4,2.9l-0.8,0.6C35.4,25.9,37.1,26.6,38,28.6z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M24.4,17.4c0.2-0.4,0.4-0.7,0.4-0.7L24,16.9c1.1-1,1.2-3.1,1.2-3.1
		                                                                        L24.6,14c0.3-0.5,0.4-2.4,0.4-2.4l-0.5,0.3c0.1-0.6-0.4-2.1-0.4-2.1c-1.4,0.1-2.1,1.1-2.1,1.1l-0.2-0.6c-1.3,0.1-2.3,1.5-2.3,1.5
		                                                                        l-0.2-0.6c-0.8,0.6-1.6,2.6-1.6,2.6l-0.3-0.5c-0.9,1.4-1.1,3.4-1.1,3.4l-0.4-0.4c-0.2,1.2-0.1,1.8,0.6,3.2c0.7,1.4,1.7,0.4,1.7,0.4
		                                                                        c0.7-3.9,3.9-7.1,3.9-7.1c-0.6,0.6-3.1,5-3.4,6.4c0,0-0.1,0.7,0.1,1.4c0,0,3.1-2.8,5.2-2.9L24.4,17.4z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M2.5,22.6c0,0,1.2,1.6,1.8,1.9l-0.6,0.4c0,0,2.1,1.3,2.7,1.4
		                                                                        l-0.6,0.4c0,0,2.3,1.4,4.1,1.1l-0.7,0.5c0,0,1.6,0.5,3.8,0.1l-0.5,0.9c0,0,2,0.2,3.9-1.4s1.2-2.7-0.4-3.4c-1.6-0.7-7.7-1.6-8.9-1.4
		                                                                        c0,0,5.6-0.7,10,1.4c0,0,1.6-0.2,0.8-2c-0.9-1.8-1.4-2.3-2.7-3l0.1,0.6c0,0-2.1-1.3-4.1-1.4l0.3,0.6c0,0-2.6-0.6-3.9-0.4l0.4,0.6
		                                                                        c0,0-2.1-0.1-3.1,1.1l0.4,0.6C5.1,21.2,3.7,21.3,2.5,22.6z"/>
                                                                        </g>
                                                                        <g>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M38,28.6c0,0-2.1,1.4-3.1,1.6l0.6,0.7c0,0-3.1,0.6-3.9,0.4l0.6,0.8
		                                                                        c0,0-3.4,0.7-5.4-0.5l0.7,1c0,0-2.3-0.1-4.6-1.6l0.2,1.2c0,0-2.6-0.6-4.1-3.6s-0.3-3.9,2-4.1c2.3-0.2,10.2,1.6,11.5,2.4
		                                                                        c0,0-6.6-3.4-12.9-2.8c0,0-1.9-1-0.1-2.9s2.6-2.2,4.6-2.5l-0.4,0.7c0,0,3.2-0.6,5.7,0.1l-0.7,0.7c0,0,3.5,0.4,4.9,1.4l-0.8,0.6
		                                                                        c0,0,2.6,0.9,3.4,2.9l-0.8,0.6C35.4,25.9,37.1,26.6,38,28.6z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M24.4,17.4c0.2-0.4,0.4-0.7,0.4-0.7L24,16.9c1.1-1,1.2-3.1,1.2-3.1
		                                                                        L24.6,14c0.3-0.5,0.4-2.4,0.4-2.4l-0.5,0.3c0.1-0.6-0.4-2.1-0.4-2.1c-1.4,0.1-2.1,1.1-2.1,1.1l-0.2-0.6c-1.3,0.1-2.3,1.5-2.3,1.5
		                                                                        l-0.2-0.6c-0.8,0.6-1.6,2.6-1.6,2.6l-0.3-0.5c-0.9,1.4-1.1,3.4-1.1,3.4l-0.4-0.4c-0.2,1.2-0.1,1.8,0.6,3.2c0.7,1.4,1.7,0.4,1.7,0.4
		                                                                        c0.7-3.9,3.9-7.1,3.9-7.1c-0.6,0.6-3.1,5-3.4,6.4c0,0-0.1,0.7,0.1,1.4c0,0,3.1-2.8,5.2-2.9L24.4,17.4z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M2.5,22.6c0,0,1.2,1.6,1.8,1.9l-0.6,0.4c0,0,2.1,1.3,2.7,1.4
		                                                                        l-0.6,0.4c0,0,2.3,1.4,4.1,1.1l-0.7,0.5c0,0,1.6,0.5,3.8,0.1l-0.5,0.9c0,0,2,0.2,3.9-1.4s1.2-2.7-0.4-3.4c-1.6-0.7-7.7-1.6-8.9-1.4
		                                                                        c0,0,5.6-0.7,10,1.4c0,0,1.6-0.2,0.8-2c-0.9-1.8-1.4-2.3-2.7-3l0.1,0.6c0,0-2.1-1.3-4.1-1.4l0.3,0.6c0,0-2.6-0.6-3.9-0.4l0.4,0.6
		                                                                        c0,0-2.1-0.1-3.1,1.1l0.4,0.6C5.1,21.2,3.7,21.3,2.5,22.6z"/>
                                                                        </g>
                                                                       </svg>
                                <div class="icon-label">Mint</div></div>
                            <div class="glass-icon" id="garnish-icon5"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                        <g>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M28.7,14.4c0,0,5.2-2.6,7.3-3.1c0,0-6.1,3.9-6.8,4.2"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M11.7,23.7c-0.6,0.3-7.1,4.6-7.1,4.6s6.8-3.2,7.6-3.6"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M19.8,17.3c0.1,0.1,1,3.6,3.7,4.4s5.2-1.8,5.6-2.9s1.1-4.2-2.4-6
		                                                                        s-4.7-1.1-5.7,0C20,13.9,19.3,16,19.8,17.3z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M21.4,14c0,0-0.9,1.1-0.5,1.6c0.4,0.5,1.2,1.2,2.1,0.8
		                                                                        c1-0.4,2-1.9,1.2-2.9c-0.8-0.9-2.1-0.9-2.4-0.5C21.6,13.4,21.9,13.8,21.4,14z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M11,22c0.1,0.1,1,3.8,3.9,4.6c2.9,0.8,5.4-1.9,5.8-3
		                                                                        c0.4-1.1,1.1-4.4-2.4-6.2s-4.9-1.1-5.9,0C11.3,18.4,10.5,20.6,11,22z"/>
	                                                                        <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M12.7,18.5c0,0-0.9,1.1-0.5,1.6c0.4,0.5,1.2,1.3,2.3,0.8
		                                                                        c1-0.4,2.1-2,1.3-3c-0.8-0.9-2.1-1-2.5-0.6C12.9,17.9,13.2,18.3,12.7,18.5z"/>
                                                                        </g>
                                                                        <g>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M28.7,14.4c0,0,5.2-2.6,7.3-3.1c0,0-6.1,3.9-6.8,4.2"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M11.7,23.7c-0.6,0.3-7.1,4.6-7.1,4.6s6.8-3.2,7.6-3.6"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M19.8,17.3c0.1,0.1,1,3.6,3.7,4.4s5.2-1.8,5.6-2.9s1.1-4.2-2.4-6
		                                                                        s-4.7-1.1-5.7,0C20,13.9,19.3,16,19.8,17.3z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M21.4,14c0,0-0.9,1.1-0.5,1.6c0.4,0.5,1.2,1.2,2.1,0.8
		                                                                        c1-0.4,2-1.9,1.2-2.9c-0.8-0.9-2.1-0.9-2.4-0.5C21.6,13.4,21.9,13.8,21.4,14z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M11,22c0.1,0.1,1,3.8,3.9,4.6c2.9,0.8,5.4-1.9,5.8-3
		                                                                        c0.4-1.1,1.1-4.4-2.4-6.2s-4.9-1.1-5.9,0C11.3,18.4,10.5,20.6,11,22z"/>
	                                                                        <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M12.7,18.5c0,0-0.9,1.1-0.5,1.6c0.4,0.5,1.2,1.3,2.3,0.8
		                                                                        c1-0.4,2.1-2,1.3-3c-0.8-0.9-2.1-1-2.5-0.6C12.9,17.9,13.2,18.3,12.7,18.5z"/>
                                                                        </g>
                                                                       </svg>
                                <div class="icon-label">Olives</div></div>
                            <div class="glass-icon" id="garnish-icon6"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                                <g>
	                                                                                <polygon fill="none" stroke="#FFFFFF" stroke-miterlimit="10" points="4.1,33.6 5.1,34.1 10.7,29.4 10,28.6 	"/>
	                                                                                <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M24.1,18.1l6.5-5.9c0,0-1.8-5.5,0.4-6.4s5.3-0.4,5.4,5.1
		                                                                                c0,0-1.9,2.8-3.9,2c0,0-0.6,1,0.1,3.3l-1.4,1l-0.9-3.2l-5.6,5.3L24.1,18.1z"/>
	                                                                                <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M32.6,10.7c0,0,3.7-1.7,1.8-2.9c-1.9-1.2-3.4-1.4-3,1
		                                                                                C31.7,11.3,31.6,11.3,32.6,10.7z"/>
	                                                                                <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M24.9,20.9c0,0-0.6-2.2-1-3.8s-4.9-3.9-6.3,1.1s0.9,7.9,3.4,6.9
		                                                                                c2.5-1,3.9-3.8,3.9-3.8l-2.4,1.4l1.8-1.6l-1.9-1.3l1.9,0.7l-1.1-1.9L24.9,20.9z"/>
	                                                                                <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M11.6,22.4c0,0-1.1,2-1.9,3.4c-0.8,1.4,0.8,6.1,5.3,3.6
		                                                                                c2.3-1.4,3.5-2.9,3.8-4.4c0,0-0.8-0.8-1.1-1.4c-0.3-0.4-0.4-1.6-0.4-1.6c-2.5-1-5.4,0.1-5.4,0.1l2.6,0.6l-2.4-0.2l0.5,2.2l-0.9-1.9
		                                                                                L11.3,25L11.6,22.4z"/>
                                                                                </g>
                                                                                <g>
	                                                                                <polygon class="icon-over" fill="none" stroke="#F47820" stroke-width="1" stroke-miterlimit="10" points="4.1,33.6 5.1,34.1 10.7,29.4 10,28.6 	"/>
	                                                                                <path class="icon-over" fill="none" stroke="#F47820" stroke-width="1" stroke-miterlimit="10" d="M24.1,18.1l6.5-5.9c0,0-1.8-5.5,0.4-6.4s5.3-0.4,5.4,5.1
		                                                                                c0,0-1.9,2.8-3.9,2c0,0-0.6,1,0.1,3.3l-1.4,1l-0.9-3.2l-5.6,5.3L24.1,18.1z"/>
	                                                                                <path class="icon-over" fill="none" stroke="#F47820" stroke-width="1" stroke-miterlimit="10" d="M32.6,10.7c0,0,3.7-1.7,1.8-2.9c-1.9-1.2-3.4-1.4-3,1
		                                                                                C31.7,11.3,31.6,11.3,32.6,10.7z"/>
	                                                                                <path class="icon-over" fill="none" stroke="#F47820" stroke-width="1" stroke-miterlimit="10" d="M24.9,20.9c0,0-0.6-2.2-1-3.8s-4.9-3.9-6.3,1.1s0.9,7.9,3.4,6.9
		                                                                                c2.5-1,3.9-3.8,3.9-3.8l-2.4,1.4l1.8-1.6l-1.9-1.3l1.9,0.7l-1.1-1.9L24.9,20.9z"/>
	                                                                                <path class="icon-over" fill="none" stroke="#F47820" stroke-width="1" stroke-miterlimit="10" d="M11.6,22.4c0,0-1.1,2-1.9,3.4c-0.8,1.4,0.8,6.1,5.3,3.6
		                                                                                c2.3-1.4,3.5-2.9,3.8-4.4c0,0-0.8-0.8-1.1-1.4c-0.3-0.4-0.4-1.6-0.4-1.6c-2.5-1-5.4,0.1-5.4,0.1l2.6,0.6l-2.4-0.2l0.5,2.2l-0.9-1.9
		                                                                                L11.3,25L11.6,22.4z"/>
                                                                                </g>
                                                                            </svg>
                                <div class="icon-label">Onions</div></div>
                            <div class="glass-icon" id="garnish-icon7"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"<g>
	                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M13,20.4c0,0-0.8-0.6-2.2-0.4c-1.5,0.3-5.4,1.9-4.9,7.3
		                            s5.6,6.6,7.5,6.5c1.9,0,7.7-1.9,7.3-7.7c-0.4-5.9-4.6-6.2-5.6-5.9C14.1,20.4,13.4,20.6,13,20.4z"/>
	                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M20.3,29.1c1.4,3.9,5.5,4.7,7.1,4.6c1.9,0,7.7-1.9,7.3-7.7
		                            c-0.4-5.8-4.6-6.2-5.6-5.9s-1.7,0.5-2.1,0.2c0,0-0.8-0.6-2.2-0.4c-1.1,0.2-3.4,1.1-4.4,3.7"/>
	                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M13.5,22.7C9.8,11.4,20.3,8.2,20.3,8.2s10.5,3.2,7,14.5"/>
	                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M25.2,22.1c0,0,2.1,1.4,4.1,0"/>
	                            <path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M10.9,22.1c0,0,2.1,1.4,4.1,0"/>
                            </g>
                            <g>
	                            <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M13,20.4c0,0-0.8-0.6-2.2-0.4c-1.5,0.3-5.4,1.9-4.9,7.3
		                            s5.6,6.6,7.5,6.5c1.9,0,7.7-1.9,7.3-7.7c-0.4-5.9-4.6-6.2-5.6-5.9C14.1,20.4,13.4,20.6,13,20.4z"/>
	                            <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M20.3,29.1c1.4,3.9,5.5,4.7,7.1,4.6c1.9,0,7.7-1.9,7.3-7.7
		                            c-0.4-5.8-4.6-6.2-5.6-5.9s-1.7,0.5-2.1,0.2c0,0-0.8-0.6-2.2-0.4c-1.1,0.2-3.4,1.1-4.4,3.7"/>
	                            <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M13.5,22.7C9.8,11.4,20.3,8.2,20.3,8.2s10.5,3.2,7,14.5"/>
	                            <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M25.2,22.1c0,0,2.1,1.4,4.1,0"/>
	                            <path class="icon-over" fill="none" stroke="#F47820" stroke-miterlimit="10" d="M10.9,22.1c0,0,2.1,1.4,4.1,0"/>
                            </g></svg>
                                <div class="icon-label">Cherries</div></div>
                        </div>
	        		</div>
	        	</div>
	        	<div class="glassDescriptionHolder mobile">
	        		<div class="glassName"></div>
	        		<div class="divider-black"></div>
	        		<div class="glassDescription"></div>
	        	</div>
	        	<div class="stripes">        		
		        	<a href="" class="btn"><p>Next Step</p></a>
	        	</div>
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>
                
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
         <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/vendor/gs/utils/SplitText.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vm.js"></script>
        <script src="js/vm-garnish.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-66449050-1','auto');ga('send','pageview');
        </script>
    </form>
</body>
</html>
