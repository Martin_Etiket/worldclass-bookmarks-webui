﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bars.aspx.cs" Inherits="WebUI.bars" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bars</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">

</head>
<body>
    <form id="form1" runat="server">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues" class="active"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li>
        		</ul>
        	</nav>
        </header>
        
        <div class="wrapper">
	        <section class="content bars">
	        	<div class="bars-header">
	        		<h1>Bars</h1>
                    <video id="bar-video" autoplay>
                        <source src="video/bars-video.mp4" type="video/mp4" />
                        <source src="video/bars-video.ogg" type="video/ogg" />
                        <source src="video/bars-video.webm" type="video/webm" />
                        <img src="images/bars-video.jpg" />
                    </video>
                    <div class="btn-bar reserve">
                        <div class="btn-content">
                            <p>Reserve<br />Club Bars</p>
                            <a href="barview.aspx?area=all&type=2" class="btn-readmore-white"></a>
                        </div>
                    </div>
	        	</div>
                <div class="buttons-holder">
                    <div class="bar-buttons">
                        <div class="btn-bar gauteng">
                            <div class="btn-content">
                                <p>WORLD CLASS BARS<br /><span class="province-name">Gauteng</span></p>
                                <a href="barview.aspx?area=3&type=1" class="btn-readmore-white"></a>
                            </div>
                        </div>
                        <div class="btn-bar wc">
                            <div class="btn-content">
                                <p>WORLD CLASS BARS<br /><span class="province-name">Western Cape</span></p>
                                <a href="barview.aspx?area=3&type=1" class="btn-readmore-white"></a>
                            </div>
                        </div>
                        <div class="btn-bar kzn">
                            <div class="btn-content">
                                <p>WORLD CLASS BARS<br /><span class="province-name">Kwazulu-Natal</span></p>
                                <a href="barview.aspx?area=4&type=1" class="btn-readmore-white"></a>
                            </div>
                        </div>
                        <div class="btn-bar all">
                            <div class="btn-content">
                                <p>WORLD CLASS BARS<br /><span class="province-name">All</span></p>
                                <a href="barview.aspx?area=all&type=1" class="btn-readmore-white"></a>
                            </div>
                        </div>
                    </div>
                </div>
	        	
<%--	        	<div class="map">
	        		<a href="barview.aspx?area=3&type=1"><div class="map-pin-bar" id="pin-gauteng"></div></a>
	        		<a href="barview.aspx?area=4&type=1"><div class="map-pin-bar" id="pin-KZN"></div></a>
	        		<a href="barview.aspx?area=9&type=1"><div class="map-pin-bar" id="pin-cape"></div></a>

	        		<a href="barview.aspx?area=all&type=2"><div class="map-pin-reserve" id="pin-reserve-gauteng"></div></a>
	        		<div class="dropdown">
	        			<div class="btn-dropdown" id="btn-dropdown-bar">World Class Bars</div>
	        			<a href="barview.aspx?area=3&type=1"><div class="btn-dropdown-option" id="gauteng">Gauteng</div></a>
	        			<a href="barview.aspx?area=9&type=1"><div class="btn-dropdown-option" id="cape">Western Cape</div></a>
	        			<a href="barview.aspx?area=4&type=1"><div class="btn-dropdown-option" id="kzn">Kwazulu-Natal</div></a>
	        			<a href="barview.aspx?area=all&type=1"><div class="btn-dropdown-option" id="all">View All</div></a>
	        		</div>
	        		<div class="dropdown-reserve">
	        			<a href="barview.aspx?area=all&type=2"><div class="btn-dropdown" id="btn-dropdown-reserve">Reserve Club Bars</div></a>
	        		</div>
	        	</div>--%>
	        	
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/bars.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l; b[l] || (b[l] =
                function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-66449050-1', 'auto'); ga('send', 'pageview');
        </script>
    </form>
</body>
</html>
