﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agegate.aspx.cs" Inherits="WebUI.agegate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>World Class</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.png">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">

    <style>
        .placeholder {
            color: #aaa;
        }
    </style>


</head>
<body>
    <form id="form1" runat="server">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrapper">
            <section class="content agegate">
                <div class="formholder">
                    <div class="logo"></div>
                    <div class="form-content">
                        <p class="LIGHT" >Please enter your date of birth below.</p>
                        <p class="LIGHT-ITALIC errorText show" id="validationMessage" runat="server"></p>
                        <div class="dateHolder">
                            <div class="input">
                                <input type="text" placeholder="DD" name="day" class="input-date-2 numbersOnly" id="input_day" runat="server" maxlength="2" /><div class="error " id="dayValidationStar" runat="server"></div>
                            </div>
                            <div class="input">
                                <input type="text" placeholder="MM" name="month" class="input-date-2 numbersOnly" id="input_month" runat="server" maxlength="2" /><div class="error " id="monthValidationStar" runat="server"></div>
                            </div>
                            <div class="input">
                                <input type="text" placeholder="YYYY" name="year" class="input-date-4 numbersOnly" id="input_year" runat="server" maxlength="4" /><div class="error " id="yearValidationStar" runat="server"></div>
                            </div>
                        </div>
                        <div class="rememberme">
                            <input type="checkbox" id="remember" runat="server">
                            <label for="remember">Remember me next time.</label>
                        </div>
                        <p class="LIGHT-ITALIC">
                            By entering this, you accept the
                            <br>
                            terms &amp; conditions.
                        </p>
                        <%--<a href="#" class="btn-enter">Enter</a>--%>
                        <asp:LinkButton ID="EnterButton" runat="server" OnClientClick="return validateAge();" OnClick="EnterButton_Click" CssClass="btn" Text="<p>Enter</p>"></asp:LinkButton>
                    </div>
                </div>
                <div class="legal">NOT FOR SALE TO PERSONS UNDER THE AGE OF 18.</div>
            </section>
        </div>

        <footer class="footer">
            <div class="footer-icons">
                <ul>
                    <li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank">
                        <div class="footer-icon icon-tanquery"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/donjuliotequila" target="_blank">
                        <div class="footer-icon icon-donjulio"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank">
                        <div class="footer-icon icon-johnniewalker"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/TheSingletonSA" target="_blank">
                        <div class="footer-icon icon-singleton"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank">
                        <div class="footer-icon icon-ronzappa"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/KetelOne " target="_blank">
                        <div class="footer-icon icon-ketel"></div>
                    </a></li>
                </ul>
            </div>
        </footer>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-66449050-1', 'auto');
            ga('send', 'pageview');

        </script>

        <asp:PlaceHolder ID="ScriptPlaceHolder" runat="server">
            
            <script type="text/javascript">

<%--                function dayChange(sender) {
                    var dayValue = $(sender).val();
                    if (dayValue.length == 2 ) {
                        $(document.getElementById("<%= input_month.ClientID %>")).focus();
                    }
            }

            function monthChange(sender) {
                var monthValue = $(sender).val()
                if (monthValue.length == 2 )
                {
                    $(document.getElementById("<%= input_year.ClientID %>")).focus();
                }
                else if (monthValue.length == 0 )
                {
                    $(document.getElementById("<%= input_day.ClientID %>")).focus();
                }
            }

            function yearChange(sender) {
                var yearValue = $(sender).val();
                if(yearValue.length == 0)
                {
                    $(document.getElementById("<%= input_month.ClientID %>")).focus();
                }
            }--%>

            function validateAge() {
                var valid = true;
                var day = $(document.getElementById("<%= input_day.ClientID %>")).val();
                var month = $(document.getElementById("<%= input_month.ClientID %>")).val();
                var year = $(document.getElementById("<%= input_year.ClientID %>")).val();
                var dayStar = document.getElementById("<%= dayValidationStar.ClientID %>");
                var monthStar = document.getElementById("<%= monthValidationStar.ClientID %>");
                var yearStar = document.getElementById("<%= yearValidationStar.ClientID %>");
                var validationMessage = document.getElementById("<%= validationMessage.ClientID %>");

                if (day == "") {
                    $(validationMessage).text("Please fill in your complete date of birth");
                    $(dayStar).addClass("show");
                    valid = false;
                }
                if (month == "") {
                    $(validationMessage).text("Please fill in your complete date of birth");
                    $(monthStar).addClass("show");
                    valid = false;
                }
                if (year == "") {
                    $(validationMessage).text("Please fill in your complete date of birth");
                    $(yearStar).addClass("show");
                    valid = false;
                }

                var age = 18;
                var mydate = new Date();
                mydate.setFullYear(year, month - 1, day);

                var currdate = new Date();
                currdate.setFullYear(currdate.getFullYear() - age);
                if ((currdate - mydate) < 0) {
                    $(validationMessage).text("Sorry. You need to be 18 years of age to enter this site.");
                    valid = false;
                }
                return valid;

            }

            window.onload = function () {
                ApplyPlaceHolders();
            };

            function isIE() {
                var myNav = navigator.userAgent.toLowerCase();
                return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
            };

            function ApplyPlaceHolders() {
                if (isIE() === 9) {
                    $('[placeholder]').focus(function () {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                            input.removeClass('placeholder');
                        }
                    }).blur(function () {
                        var input = $(this);
                        if (input.val() == '' || input.val() == input.attr('placeholder')) {
                            input.addClass('placeholder');
                            input.val(input.attr('placeholder'));
                        }
                    }).blur();
                }
            };

            function textChanged(element, validationElement) {
                if (element.value !== '') {
                    element.className = element.className.replace(' error', '');
                    validationElement.innerHTML = '';
                }
            };


        </script>
        </asp:PlaceHolder>
        

    </form>
</body>
</html>
