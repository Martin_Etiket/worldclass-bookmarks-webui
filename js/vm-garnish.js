var _glassGarnishes;
var _garnishes;
var _numGarnishes;
var _selectedGarnish;
var _mustload = {
    'background': false,
    'brand_loaded': false,
    'glass_loaded': false,
    'garnish_loaded':false
}
$(document).ready(function () {

    new WorldClassPreloader();
    var glowLightHolder = $('.carousel-holder .glowing-lights-holder');
    new GlowingLight({ parent: glowLightHolder, left: 160, top: 40, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 220, top: 75, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 140, top: 140, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 500, top: 280, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 320, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 640, top: 120, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 420, top: 110, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 750, top: 220, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 344, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 700, top: 250, colour: '#ffffff' });

    var progress = new ProgressBlock(4);
    $(document).on('showing', function () {
        progress.display();
    });

	$.getJSON('js/config.json', function(config){
		_config = config;
		
		_brandId = parseInt(getParameterByName("brandId"));
		_selectedGlassId = parseInt(getParameterByName("glassId"));
		getBrands();
		mixers = getParameterByName('MixerIDs').split(",");
		if(mixers.length==1){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[0])];
		}
		if(mixers.length==2){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[1])];
		}
		if(mixers.length==3){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[1]), parseInt(mixers[2])];
		}
		getGlasses(displayGlass);
		getGarnishes();
		displayMask();
		doBubbles();
	});

	$('.carousel-holder').waitForImages(function () {
	    _mustload.background = true;
	    checkImagesLoaded('.content');
	});
	_garnishloader = new WorldClassPreloader({ parent: $('.mixHolder'), id: 'garnishPreloader', top: '150px', left: '290px', r: '10px', top_mobile: '100px', left_mobile: '50%' });
	$('#carouselItem-slider-prev').on('click', prevGarnish);
	$('#carouselItem-slider-next').on('click', nextGarnish);

	new GarnishIcon($('#garnish-icon1'));
	new GarnishIcon($('#garnish-icon2'));
	new GarnishIcon($('#garnish-icon3'));
	new GarnishIcon($('#garnish-icon4'));
	new GarnishIcon($('#garnish-icon5'));
	new GarnishIcon($('#garnish-icon6'));
	new GarnishIcon($('#garnish-icon7'));
});


// "click" first garnish when glasses all loaded
function clickFirst() {
    $('#garnish-icon1').trigger('click');
}


function getGarnishes(){
	$.getJSON(_config.api.glassgarnishes, function (glassgarnishes){
		_glassGarnishes = $.grep(glassgarnishes, function (g){
			return g.GlassId == _selectedGlassId;
		});
		_numGarnishes = _glassGarnishes.length;
		
		$.getJSON(_config.api.garnishes, function (garnishes){
			_garnishes = garnishes;
			for (var i=0; i<_glassGarnishes.length; i++){
				var g = getGarnish(_glassGarnishes[i].GarnishId);
				_glassGarnishes[i].Name = g.Name;
				_glassGarnishes[i].Description = g.Description;
			}
			clickFirst();
		});
	});
}



//function selectGarnish(i){
//	var garnish = _glassGarnishes[i];
//	var img = _config.vmImageFolder + garnish.Image_Name;
//	$('.garnishHolder').css({'background':'url('+img+')', 'background-size':'contain'});
//	$('.glassDescriptionHolder .glassName').html(garnish.Name);
//	$('.glassDescriptionHolder .glassDescription').html(garnish.Description);
	
//	_garnishId = garnish.GarnishId;
//	var url = _config.pages.vmName + '?brandId='+_brandId+'&glassId='+_selectedGlassId+"&MixerIDs="+_selectedMixers.toString()+"&garnishId="+_garnishId;
//	$('.btn').attr('href', url);
	
//	_selectedGarnish = i;
//}

function nextGarnish(){
	if(_selectedGarnish < _numGarnishes-1){
		selectGarnish(_selectedGarnish+1);
	}else{
		selectGarnish(0);
	}
}

function prevGarnish(){
	if(_selectedGarnish > 0){
		selectGarnish(_selectedGarnish-1);
	}else{
		selectGarnish(_numGarnishes-1);
	}
}

function selectGarnish(x) {
    TweenMax.set(_garnishloader, { autoAlpha: 1 });
    if (x == undefined) {
        x = 0;
    }
    var garnish = _glassGarnishes[x];
    var img = _config.vmImageFolder + garnish.Image_Name;
    _garnishId = garnish.GarnishId;
    _selectedGarnish = x;
    var garnishHolder = $('.garnishHolder');

    TweenMax.to(garnishHolder, 0.2, {
        opacity: 0,
        y: -100,
        ease: Quad.easeIn,
        onComplete: function () {

            loadImg(img, function (success) {
                if (success) {
                    garnishHolder.css({ 'background': 'url(' + $(this).attr('src') + ')', 'background-size': 'contain' });
                    TweenMax.fromTo(garnishHolder, 0.6, { opacity: 0, y: -200 }, { opacity: 1, y: 0, ease: Quad.easeOut });
                    _mustload.garnish_loaded = true;
                    checkImagesLoaded('.content');
                    TweenMax.to(_garnishloader, 0.3, { autoAlpha: 0, ease: Quad.easeInOut });

                    $('.glassDescriptionHolder .glassName').html(garnish.Name);
                    TweenMax.from($('.glassDescriptionHolder .glassName'), 0.4, { y: 50, ease: Back.easeOut });
                    $('.glassDescriptionHolder .glassDescription').html(garnish.Description);
                    TweenMax.set($('.glassDescriptionHolder .glassDescription'), { perspective: 400 });
                    var st = new SplitText($('.glassDescriptionHolder .glassDescription'), { type: 'lines' });
                    TweenMax.staggerFrom(st.lines, 0.5, {
                        y: 50,
                        opacity: 0,
                        ease: Back.easeOut
                    }, 0.05);
                    var url = _config.pages.vmName + '?brandId=' + _brandId + "&glassId=" + _selectedGlassId + "&MixerIDs=" + _selectedMixers.toString() + "&garnishId=" + _garnishId;
                    $('.btn').attr('href', url);
                }
            }); // end loadImg
        }
    });
}

function GarnishIcon(element) {
    element.css({ 'cursor': 'pointer' });
    var svg = element.find('.icon-over');
    TweenMax.set(svg, { drawSVG: "50% 50%" });
    var label = element.find('.icon-label');
    TweenMax.set(label, { autoAlpha: 0, x: -15 });

    element.on('mouseover', function () {
        if (!element.hasClass('active')) {
            TweenMax.to(svg, 0.6, { drawSVG: "0% 100%", ease: Quad.easeOut });
            TweenMax.to(label, 0.3, { autoAlpha: 1, x: 0, ease: Quad.easeOut });
        }
    });

    element.on('mouseout', function () {
        if (!element.hasClass('active')) {
            TweenMax.to(svg, 0.5, { drawSVG: "50% 50%", ease: Quad.easeIn });
            TweenMax.to(label, 0.3, { autoAlpha: 0, x: -15, ease: Quad.easeIn});
        }
    });

    element.on('click', function () {
        // reset all
        $('.glass-icon').removeClass('active');
        TweenMax.to($('.glass-icon').find('.icon-over'), 0.5, { drawSVG: "50% 50%", ease: Quad.eastIn });
        TweenMax.to($('.glass-icon').find('.icon-label'), 0.3, { autoAlpha: 0, x: -15, ease: Quad.easeIn });

        // update this
        $(this).addClass('active');
        TweenMax.killTweensOf(svg);
        TweenMax.to(svg, 0.5, { drawSVG: "0% 100%", ease: Quad.easeOut });
        TweenMax.to(label, 0.3, { autoAlpha: 1, x: 0, ease: Quad.easeOut });

        //select glass
        // sorry, this is probably the worst way to do this. :(
        var id;
        switch (element.attr('id')) {
            case 'garnish-icon1':
                id = 0;
                break;
            case 'garnish-icon2':
                id = 1;
                break;
            case 'garnish-icon3':
                id = 2;
                break;
            case 'garnish-icon4':
                id = 3;
                break;
            case 'garnish-icon5':
                id = 4;
                break;
            case 'garnish-icon6':
                id = 5;
                break;
            case 'garnish-icon7':
                id = 6;
                break;
            case 'garnish-icon8':
                id = 7;
                break;

        }
        selectGarnish(id);
    });
}