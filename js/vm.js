
var _brandId;
var _selectedGlassId;

var _bottleBg;
var _glassBg;
var _glasses;
var _numGlasses;
var _selectedGlass;
var _mixers;
var _selectedMixers;
var _garnish;
var _garnishes;
var _allMixers;
var _glassGarnish;
var _brands;
var _brand;
var _amounts=[];
var _displayedMixers;
var _ingredients = [];

function getBrands(){
	$.getJSON(_config.api.brands, function(brands){
	    // console.log('brands: ', JSON.stringify(brands));
	    _brands = brands;
		_brandId = getParameterByName("brandId");
		_brand = brands[brands.map(function(x){return x.ID;}).indexOf(parseInt(_brandId))];
		_bottleBg = _config.vmImageFolder + _brand.Image_DesktopName;
		
		    //$('.bottleHolder').css({ 'background': 'url(' + _bottleBg + ') no-repeat', 'background-size': 'contain' });
		loadImg(_bottleBg, function (loaded) {
		    $('.bottleHolder').css({ 'background': 'url(' + $(this).attr("src") + ') no-repeat', 'background-size': 'contain' });
		    _mustload.brand_loaded = true;
		    checkImagesLoaded('.content');
		});

		//$('.bottleHolder').waitForImages(function () {
		//    _mustload.brand_loaded = true;
		//    checkImagesLoaded('.content');
		//});
		// getGlasses();
	});
}


function getGlasses(callback){
	$.getJSON(_config.api.glasses,function (glasses){
		_glasses = glasses;
		_numGlasses = _glasses.length;
		// console.log(JSON.stringify(glasses));
		// selectGlass(0);
		callback();
	});
}

function getMixersForBrand(callback){
	$.getJSON(_config.api.brandmixers, function (brandmixers){
		
		_mixers = $.grep(brandmixers, function (result){
			return result.BrandId == _brandId;
		});
		
		callback();
	});
}
function getAllMixers(callback){
	$.getJSON(_config.api.mixers, function (mixers){
		_allMixers = mixers;
		callback();
	});
}

function getMixer(id){
	
	var m = $.grep(_allMixers, function(g){
		return g.ID == id;
	})[0];
	
	return m;
}

function displayGlass(){
	var glass = $.grep(_glasses, function(g){
		return g.ID == _selectedGlassId;
	})[0];

	var img = _config.vmImageFolder + glass.Image_DesktopName;
	loadImg(img, function (success) {
	    if (success) {
	        $('.glassHolder').css({ 'background': 'url(' + $(this).attr('src') + ')', 'background-size': 'contain' });
	        _mustload.glass_loaded = true;
	        _mustload.glass_loaded = true;
	        checkImagesLoaded('.content');
	    }
	});
}

function getGarnish(id){
	var garnish = $.grep(_garnishes, function (g){
		return g.ID == id;
	})[0];
	
	return garnish;
}

function getAllGarnishes(callback){
	$.getJSON(_config.api.garnishes,function (garnishes){
		_garnishes = garnishes;
		callback();
	});
}

function displayGarnish(){
	var garnish = $.grep(_garnishes, function(g){
		return g.ID == _garnishId;
	})[0];
	
	$.getJSON(_config.api.glassgarnishes, function (list){
		_glassGarnish = $.grep(list, function (g){
			return (g.GarnishId == _garnishId && g.GlassId == + _selectedGlassId);
		})[0];
		
		var img = _config.vmImageFolder + _glassGarnish.Image_Name;
		loadImg(img, function (success) {
		    if (success) {
		        $('.garnishHolder').css({ 'background': 'url(' + $(this).attr('src') + ')', 'background-size': 'contain' });
		        $('#ingredient4 .quantity').html(getGarnish(_glassGarnish.GarnishId).Name + ' to finish it off');
		        _mustload.garnish_loaded = true;
		        checkImagesLoaded('.content');
		    }
		});

	});
}

function displayMask(){
	var image;
	var ingredients = {
		"GlassId": _selectedGlassId,
		"BrandId": _brandId,
		"MixerIds": _selectedMixers
	};
	$.getJSON(_config.api.mask, { input: JSON.stringify(ingredients) }).done(function (response) {
	    image = response; //Your base64 image encoded. You can map this to a <img>'s Src attribute.
	    loadImg(image, function (success) {
	        if (success) {
	            $('.colourHolder').css({ 'background': 'url(' + $(this).attr('src') + ')', 'background-size': 'contain' });
	            _mustload.mask_loaded = true;
	            checkImagesLoaded('.content');
	        }
	    });
		
	}).error(function (errorResponse) {
	    if (console)
	    {
	        console.log(errorResponse.responseText); //Any error message you encounter. Please forward to me. 
	    }
	});
}

function populateMixers(){
    _displayedMixers = [""];
    var amounts = [];
    var doAmounts = false;
    if (getParameterByName("amounts").split(',').length > 0 && getParameterByName("amounts") != undefined && getParameterByName("amounts") != "") {
        _amounts = getParameterByName("amounts").split(',');
    }else {
        doAmounts = true;
    }
    var count = 0;
    // Brand 
    var bn;
    if (_brand.Name == "Johnnie Walker Gold Label Reserve") {
        bn = "Johnnie Walker<br>Gold Label Reserve"
    } else {
        bn = _brand.Name;
    }
    if (doAmounts) {
        amounts.push(10);
        $('#ingredient0 .quantity').html('<b>' + amounts[0] + 'ml</b> ' + bn);
    } else {
        $('#ingredient0 .quantity').html('<b>' + _amounts[0].toString() + 'ml</b> ' + bn);
    }
    
    $('#ingredient0').removeClass('hidden');
    _ingredients.push(bn);


    // mixers
    for (var i = 0; i < _selectedMixers.length; i++) {
        if (_displayedMixers.indexOf(_selectedMixers[i]) < 0) {
            count++;
            _displayedMixers.push(_selectedMixers[i]);

            if (doAmounts) amounts.push(10);

            $('#ingredient' + (count)).removeClass('hidden');
        }
    }
    if(doAmounts) _amounts = amounts;
    for (var j = 1; j < _displayedMixers.length; j++) {
        var mixer = getMixer(parseInt(_displayedMixers[j]));
        $('#ingredient' + j + ' .quantity').html('<b>' + _amounts[j] + 'ml</b> ' + mixer.Name);
        _ingredients.push(mixer.Name);
    }

    for (var i = 0; i < 4; i++) {
        log(i + ": " + $('#ingredient' + j + ' .quantity').html())
        if ($('#ingredient' + i + ' .quantity').html() == "") {
            log("EMPTY ONE: " + i)
            $('#ingredient' + i).addClass('hidden');
        }
    }
}


function ProgressBlock(step) {
    var element = $('.progress');
    if (element.length) {
        var bw = $(element).width();
        var bh = $(element).height();

        var SVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        SVG.setAttribute("x", "0px");
        SVG.setAttribute("y", "0px");
        SVG.setAttribute("width", bw);
        SVG.setAttribute("height", bh);
        SVG.setAttribute("viewBox", "0 0 " + bw + " " + bh);
        SVG.setAttribute("version", "1.1");

        var bg = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        bg.setAttribute('width', bw);
        bg.setAttribute('height', bh);
        bg.setAttribute('fill', '#ffffff');
        bg.setAttribute("x", "0");
        bg.setAttribute("y", "0");
        SVG.appendChild(bg);

        var border = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        border.setAttribute('x', '10');
        border.setAttribute('y', '10');
        border.setAttribute('width', bw - 20);
        border.setAttribute('height', bh - 20);
        border.setAttribute('stroke', '#E75300');
        border.setAttribute('stroke-width', '0.5');
        border.setAttribute('fill', 'none');
        document.body.appendChild(border);
        document.body.removeChild(border);
        SVG.appendChild(border);

        element.append(SVG);
        $(SVG).css({ 'position': 'absolute', 'z-index': '0', 'left': '0', 'top': '0', 'opacity': 0 });
        $('.progressContent').css('opacity', 0);
        TweenMax.set(border, { drawSVG: "0% 0%" });

        var step_top = bh - 19 + 'px';
        //step 1
        var step1 = this.step1 = new ProgressStepSVG({ id: "step1" });
        $(step1.element).css({
            'top': step_top,
            'left': (bw / 2) - 40 + 'px',
            'opacity': 0
        });
        element.append($(step1.element));
        //step 2
        var step2 = this.step2 = new ProgressStepSVG({ id: "step2" });
        $(step2.element).css({
            'top': step_top,
            'left': (bw / 2) - 20 + 'px',
            'opacity': 0
        });
        element.append($(step2.element));
        //step 3
        var step3 = this.step3 = new ProgressStepSVG({ id: "step3" });
        $(step3.element).css({
            'top': step_top,
            'left': (bw / 2) + 'px',
            'opacity': 0
        });
        element.append($(step3.element));
        //step 4
        var step4 = this.step4 = new ProgressStepSVG({ id: "step4" });
        $(step4.element).css({
            'top': step_top,
            'left': (bw / 2) + 20 + 'px',
            'opacity': 0
        });
        element.append($(step4.element));
        //step 5
        var step5 = this.step5 = new ProgressStepSVG({ id: "step5" });
        $(step5.element).css({
            'top': step_top,
            'left': (bw / 2) + 40 + 'px',
            'opacity': 0
        });
        element.append($(step5.element));

        this['step' + step].setActive(true);

        if (step > 1) {
            $(step1.element).css({ 'cursor': 'pointer', 'pointer-events': 'auto' });
            $(step1.element).on('click', progressClicked);
        }
        if (step > 2) {
            $(step2.element).css({ 'cursor': 'pointer', 'pointer-events': 'auto' });
            $(step2.element).on('click', progressClicked);
        }
        if (step > 3) {
            $(step3.element).css({ 'cursor': 'pointer', 'pointer-events': 'auto' });
            $(step3.element).on('click', progressClicked);
        }
        if (step > 4) {
            $(step4.element).css({ 'cursor': 'pointer', 'pointer-events': 'auto' });
            $(step4.element).on('click', progressClicked);
        }


        // display progress block when page is loaded.
        this.display = function () {
            var tl = new TimelineMax();
            tl.to(SVG, 0.2, { autoAlpha: 1, delay: 1 })
            .to(border, 0.8, { drawSVG: "100%" }, "-=0.2")
            .to($('.progressContent'), 0.4, { autoAlpha: 1 }, "-=0.2")
            .to(step1.element, 0.5, { 'opacity': 1, ease: Quad.easeInOut }, "-=0.4")
            .to(step2.element, 0.5, { 'opacity': 1, ease: Quad.easeInOut }, "-=0.4")
            .to(step3.element, 0.5, { 'opacity': 1, ease: Quad.easeInOut }, "-=0.4")
            .to(step4.element, 0.5, { 'opacity': 1, ease: Quad.easeInOut }, "-=0.4")
            .to(step5.element, 0.5, { 'opacity': 1, ease: Quad.easeInOut }, "-=0.4")
        }

        return this;
    }
}

function ProgressStepSVG(options) {
    isActive = options.isActive || false;
    var element = this.element = document.createElement("div");
        element.setAttribute('class', 'stepProgress');
        element.setAttribute('id', options.id);
        $(element).css({
            'width': '8px',
            'height': '8px',
            'float': 'left',
            'position': 'absolute',
            //'top': height-20 + 'px'
        });

    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute('class', 'progressStepSVG');
        svg.setAttribute("x", "0px");
        svg.setAttribute("y", "0px");
        svg.setAttribute("width", "8px");
        svg.setAttribute("height", "8px");
        svg.setAttribute("viewBox", "0 0 8 8");
        svg.setAttribute("version", "1.1");

    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        rect.setAttribute('x', '1.3');
        rect.setAttribute('y', '1.2');
        rect.setAttribute('width', '5.6');
        rect.setAttribute('height', '5.6');
        rect.setAttribute('stroke', '#FFFFFF');
        rect.setAttribute('stroke-width', '1');
        if (isActive) {
            rect.setAttribute('fill', '#f47a20');
        } else {
            rect.setAttribute('fill', '#c0c0c0');
        }
        rect.setAttribute('transform', 'matrix(0.7071 0.7072 -0.7072 0.7071 3.9738 -1.7269)');

    svg.appendChild(rect);
    element.appendChild(svg);

    this.setActive = function (isActive) {
        if (isActive) {
            rect.setAttribute('fill', '#f47a20');
        } else {
            rect.setAttribute('fill', '#c0c0c0');
        }
    }

    return this;
}

function progressClicked(event) {
    log('clicked: ' + $(this).attr('id'));
    // redirect to spep clicked...
    switch ($(this).attr('id')) {
        case 'step1':
            window.location = "spirits.aspx?isvm=true";
            break;
        case 'step2':
            window.location = "vm-glass.aspx?brandId="+_brandId;
            break;
        case 'step3':
            window.location = "vm-mixers.aspx?brandId=" + _brandId+"&glassId="+_selectedGlassId;
            break;
        case 'step4':
            window.location = "vm-garnish.aspx?brandId=" + _brandId + "&glassId=" + _selectedGlassId + "&MixerIDs=" + _selectedMixers;
            break;
    }
}

var mixersWithBubbles = [29]; // add more mixers with bubbles here.
function doBubbles() {
    log('>>> doBubbles:')
    log('Mixers: ' + _selectedMixers);
    log('glass: ' + _selectedGlassId);
    var haveBubbles = false;
    for (var i = 0; i < mixersWithBubbles.length; i++) {
        for(var j=0; j<_selectedMixers.length; j++){
            if(_selectedMixers[j] == mixersWithBubbles[i]){
                haveBubbles = true;
                break;
            }
            if (haveBubbles) {
                break;
            }
        }
    }
    log('have bubbles: ' + haveBubbles);

    //var bubblesHolder = $('.bubblesHolder');
    //new Bubble(true);
    //new Bubble(true);
    //new Bubble(true);
    //new Bubble(true);
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
    new Bubble();
}
var testCount = 0;
function Bubble(isTest) {
    var r;
    isTest ? (r = 2) : (r = Math.random() * 2);

    if(isTest) testCount++;

    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute('class', 'bubble');
        svg.setAttribute("x", "0px");
        svg.setAttribute("y", "0px");
        svg.setAttribute("width", 2*r+"px");
        svg.setAttribute("height", 2 * r + "px");
        svg.setAttribute("viewBox", "0 0 "+2*r+" "+2*r);
        svg.setAttribute("version", "1.1");

    var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        circle.setAttribute('cx', r);
        circle.setAttribute('cy', r);
        circle.setAttribute('r', r);
        circle.setAttribute('fill', isTest?'#FF0000':'none');
        circle.setAttribute('stroke', isTest?'#FF0000':'#FFFFFF');
        circle.setAttribute('stroke-width', '0.25');
        svg.appendChild(circle);

        var glassPos = {
            glass1: {
                top: 395, // to the bottom part of the glass
                left: 270, // to the left side of the glass area
                height: 140, // height from thebottom part of the glass
                width: 40 // the width of the glass area
            },
            glass2: {
                top: 395, 
                left: 255,
                height: 70,
                width: 56 
            },
            glass3: {
                top: 346,
                left: 267,
                height: 94,
                width: 69
            },
            glass4: {
                top: 277,
                left: 240,
                height: 41,
                width: 81
            },
            glass5: {
                top: 265,
                left: 270,
                height: 105,
                width: 41
            },
            glass6: {
                top: 295,
                left: 240,
                height: 32,
                width: 92
            },
            glass7: {
                top: 290,
                left: 258,
                height: 47,
                width: 50
            },
            glass8: {
                top: 389,
                left: 256,
                height: 61,
                width: 35
            }
        }
    var element = this.element = document.createElement("div");

    // so we can test where they'll sit on the glass
    if (isTest) {
        switch (testCount) {
            case 1:
                var top = glassPos['glass' + _selectedGlassId].top;
                var left = glassPos['glass' + _selectedGlassId].left;
                break;
            case 2:
                var top = glassPos['glass' + _selectedGlassId].top;
                var left = glassPos['glass' + _selectedGlassId].left + glassPos['glass' + _selectedGlassId].width;
                break;
            case 3:
                var top = glassPos['glass' + _selectedGlassId].top - glassPos['glass' + _selectedGlassId].height;
                var left = glassPos['glass' + _selectedGlassId].left + glassPos['glass' + _selectedGlassId].width;
                break;
            case 4:
                var top = glassPos['glass' + _selectedGlassId].top - glassPos['glass' + _selectedGlassId].height;
                var left = glassPos['glass' + _selectedGlassId].left;
                break;

        }
        $(element).css({
            'left': left + 'px',
            'top': top + 'px'
        });


    };


    $(element).css('position', 'absolute');

    element.appendChild(svg);
    $('.bubblesHolder').append(element);
    if (!isTest) {
        TweenMax.delayedCall(Math.random() * 3, function () {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: Math.random() * 5 });
            tl.set(element, {
                y: glassPos['glass' + _selectedGlassId].top - (Math.random() * glassPos['glass' + _selectedGlassId].height),
                x: glassPos['glass' + _selectedGlassId].left + (Math.random() * glassPos['glass' + _selectedGlassId].width),
                autoAlpha: 0,
                scaleX: 0,
                scaleY: 0
            })
            .to(element, Math.random()*2, {
                scaleX: 1,
                scaleY: 1,
                autoAlpha: Math.random()+0.8
            })
            .to(element, Math.random()*8, {
                y: (glassPos['glass' + _selectedGlassId].top - glassPos['glass' + _selectedGlassId].height),
                autoAlpha: 0,
                delay: Math.random()*5
            })
        });
    }
}