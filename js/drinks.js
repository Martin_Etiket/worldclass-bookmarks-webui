var _config;
var _cocktails;
var _w;
var drinksArr;// = $('.drink').toArray();
_mustload = {
    "drink1": false,
    "drink2": false,
    "drink3": false,
    "drink4": false,
    "drink5": false,
    "drink6": false,
    "drink7": false,
    "drink8": false,
    "drink9": false,
    "drink0": false
}
$(document).ready(function () {
    new WorldClassPreloader();

	$.getJSON('js/config.json', function(config){
	    _config = config;
	    $('.drinks-list').css({'opacity':'0'});
		getDrinks();
		
	});

	/*$('.drink').each(function () {
	    $(this).on('click', function () {

	    });
	});*/
});

var _drinks = [];

function getDrinks(){
	$.getJSON(_config.api.cocktails, function(cocktails){
		_cocktails = cocktails;
		_drinks = [];
		for (var i=0; i<_cocktails.length; i++){
			var c = _cocktails[i];
			// if(c.Image_ThumbName!=""){
				drink = $($('#drinkTemplate').html());
				var img = _config.vmImageFolder + '/' + c.Image_ThumbName;
				drink.addClass('drink');
				drink.attr('id', 'drink' + i);
				//drink.find('.drink-pic').find('img').attr('src', img);
				drink.find('.name').html(c.Name);
				var link = _config.pages.drinksDetail + '?selected=' + i;
				drink.find('a').attr('href', link);
				drink.on('click', function () {
				    var url = $(this).find('a').attr('href');
				    window.location = url;
				});
				drink.appendTo($('.drinks-list'));
				loadImg(img, function (success, params) {
				    if (success) {
				        var c = params.toString();
				        var d = $("#drink" + c);
				        d.find('.drink-pic').find('img').attr('src', $(this).attr('src'));
				        _mustload['drink' + c] = true;
				        checkImagesLoaded('.content');
				    } else {
                        log('ERROR loading image '+params)
				    }
				}, i)
			// };
		}
		drinksArr = $('.drink').toArray();
		setTimeout(onresize, 1000);
		$(window).resize(onresize);



		$('.drink').on('mouseover', function () {
		    $(this).find('a').addClass('hover');
		    $(this).find('.drink-pic').css({
                "overflow":"hidden"
		    })
		    TweenMax.to($(this).find('.drink-pic img'), 0.8, { scaleX: 1.05, scaleY:1.05, transformOrigin: "50% 50%", ease: Quad.easeOut });
		});
		$('.drink').on('mouseout', function () {
		    $(this).find('a').removeClass('hover');
		    TweenMax.to($(this).find('.drink-pic img'), 0.8, { scaleX: 1, scaleY: 1, transformOrigin: "50% 50%", ease: Quad.easeOut });
	    });
	});
}

function onresize() {
	_w = window.innerWidth;
	
	if(_w < 800){
		$('.drink').css('width',$('.drinks-list').width());
		var dw = $('.drink').width();
		$('.drink').css({'height':(dw/2)+'px'});
		for (var i=0; i < drinksArr.length; i++){
			var drink = $(drinksArr[i]);
			if (i%2 === 0){
				drink.addClass('right');
				
			}else{
				drink.removeClass('right');
				
			}
		}
	}else{
		$('.drink').css('width',$('.drinks-list').width()/2);
		var dw = $('.drink').width();
		$('.drink').css({'height':(dw/2)+'px'});
		
		var colCount = 0;
		var rowCount = 0;
		for (var i=0; i<drinksArr.length; i++){
			var drink = $(drinksArr[i]);
			
			if(rowCount==0 || rowCount%2 === 0){
				drink.removeClass('right');
			}else{
				drink.addClass('right');
			}
			
			colCount++;
			if(colCount == 2){
				colCount = 0;
				rowCount++;
			}
		}
	}
	$('.drinks-list').css({'opacity': '1'});
}
