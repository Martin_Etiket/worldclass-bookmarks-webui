/*var items=[
	{
		id: "0",
		name:"Bascule",
		description: "Set on the water’s edge of the marina, here’s where taking in the sights of Cape Town’s seas go hand-in-hand with your World Class cocktail.",
		locationURL:"http://maps.google.com",
		locationName:"Cape Grace Hotel, West Quay Rd, V&A Waterfront.",
		area: "cape",
		type: "bar"
	},
	{
		id: "1",
		name:"Buena Vista",
		description: "Take a great night, fun company and fine dining to the next level with your World Class cocktail.",
		locationURL:"http://maps.google.com",
		locationName:"Palm Grove Centre, corner Main and Church St, Durbanville. ",
		area: "cape",
		type: "bar"
	},
	{
		id: "2",
		name:"The Bungalow ",
		description: "Bring out the beautiful, stylish and everything in between when enjoying your World Class cocktail.",
		locationURL:"http://maps.google.com",
		locationName:"Glen Country Club, 3 Victoria Rd, Clifton. ",
		area: "cape",
		type: "bar"
	},
	{
		id: "3",
		name:"Café Caprice",
		description: "The combination of a café-style menu, relaxing lounges and sidewalk seating to experience the epitome of Cape Town’s cosmopolitan<br>social scene.",
		locationURL:"http://maps.google.com",
		locationName:"37 Victoria Rd, Camps Bay.",
		area: "cape",
		type: "bar"
	},
	{
		id: "4",
		name:"9th Avenue Bistro",
		description: "This award-winning dining venue makes drinking World Class cocktails even easier. ",
		locationURL:"http://maps.google.com",
		locationName:"2 Avonmore Centre, Ninth Ave, Morningside.",
		area: "kzn",
		type: "bar"
	},
	{
		id: "5",
		name:"Asoka",
		description: "Add a Victorian townhouse bar with an open-roof candlelit courtyard and Asian fusion tapas to your World Class experience.",
		locationURL:"http://maps.google.com",
		locationName:"175 Dunkeld Rd, Reservoir Hills.",
		area: "kzn",
		type: "bar"
	},
	{
		id: "6",
		name:"Barrio ",
		description: "This culmination of culture and cuisine lets you sip on your World Class cocktail in <br>pleasure and style.",
		locationURL:"http://maps.google.com",
		locationName:"Shop 7 at 12 on Palm Boulevard, Umhlanga Ridge.",
		area: "kzn",
		type: "bar"
	},
	{
		id: "7",
		name:"Bel Punto ",
		description: "Overlook the ocean, World Class cocktail in your hand and sea breeze in your hair. ",
		locationURL:"http://maps.google.com",
		locationName:"1 South Beach Rd, Umdloti.",
		area: "kzn",
		type: "bar"
	},
	{
		id: "8",
		name:"African Pride Hotel",
		description: "This ideal setting makes luxury seem effortless and enjoying a World Class cocktail is just as fitting.",
		locationURL:"http://maps.google.com",
		locationName:"Melrose Square, Melrose Arch. ",
		area: "gauteng",
		type: "bar"
	},
	{
		id: "9",
		name:"Anti establishment",
		description: "Set in a trendy side of Johannesburg, here’s where your World Class cocktails come <br>with a twist.",
		locationURL:"http://maps.google.com",
		locationName:"73 Juta St, Braamfontein.",
		area: "gauteng",
		type: "bar"
	},
	{
		id: "10",
		name:"Cafe Del Sol",
		description: "It's all in the name; superb food and an atmosphere that speaks to the heart and soul, much like a World Class cocktail.",
		locationURL:"http://maps.google.com",
		locationName:"Olivedale Corner Shopping Centre, Randburg. ",
		area: "gauteng",
		type: "bar"
	},
	{
		id: "11",
		name:"Kong, Fourways",
		description: "Find your ideal Asian food pairing for your World Class cocktail here.",
		locationURL:"http://maps.google.com",
		locationName:"Cedar Rd Entrance, The Terrace At Cedar Square, Willow Ave, Fourways",
		area: "gauteng",
		type: "bar"
	},
	{
		id: "12",
		name:"Bahamas ",
		description: "World Class gets the tropical treatment at the beach-themed Bahamas – perfect for a cocktail made by the world’s best mixologists.",
		locationURL:"http://maps.google.com",
		locationName:"10632 Kgaswane St, Kwathema, Johanneburg.",
		area: "gauteng",
		type: "reserve"
	},
	{
		id: "13",
		name:"THE Bassment  ",
		description: "The Bassment is an independent bar/club featuring a diverse mix of live music and entertainment. Add a splash of World Class for an unforgettable experience.",
		locationURL:"http://maps.google.com",
		locationName:"Aspen House, Aspen Lakes Dr, Aspen Hills, Johannesburg.",
		area: "gauteng",
		type: "reserve"
	},
	{
		id: "14",
		name:"Blue Room Hatfield",
		description: "Sophisticated style and VIP treatment, this club features some of the hottest music acts and offers you the choice of four different spaces to celebrate your World Class cocktails in.",
		locationURL:"http://maps.google.com",
		locationName:"1057 Arcadia St, Pretoria.",
		area: "gauteng",
		type: "reserve"
	},
	{
		id: "15",
		name:"BRA PIKES",
		description: "A traditional-styled bar and restaurant inspired by real South African culture, Bra Pikes is a World Class experience waiting to be discovered.",
		locationURL:"http://maps.google.com",
		locationName:"Corner Club and Linksfield Rd, Johannesburg.",
		area: "gauteng",
		type: "reserve"
	}
];*/
var _items;
var _numItems;// = items.length;

var _filterItems=[];
var _numFilterItems;
var w = $(window).width();

var _type;
var _area;
var _mustload = {
    "allbardetails": false,
    "background1": false,
    "background2":false
}

$(document).ready(function () {

    new WorldClassPreloader();

	_type = getParameterByName('type');
	_area = getParameterByName('area');

	$.getJSON('js/config.json', function(config){
		_config = config;
		
		$.getJSON(_config.api.bars, function (items){
			_numItems = items.length;
			_items = items;
			
			getBarDetails(function(){
			    // setTimeout(populateBars, 1000);
			    _mustload.allbardetails = true;
			    checkImagesLoaded('.content');
				populateBars();
			});
			
		});

		loadImg("images/bg-bars.jpg", function (success) {
		    if (success) {
		        _mustload.background1 = true;
		        checkImagesLoaded('.content');
		    }
		});
		loadImg("images/bg-reservebars.jpg", function (success) {
		    if (success) {
		        _mustload.background2 = true;
		        checkImagesLoaded('.content');
		    }
		});
	});
	
	

	
	/*
	setTimeout(function(){
	
		for (var f=0; f < numItems; f++){
			if(area.toLowerCase() != "all"){
				if(items[f].type == type && items[f].area == area){
					filterItems.push(items[f]);
				}
			}else{
				if(items[f].type == type){
					filterItems.push(items[f]);
				}
			}
		}
		numFilterItems = filterItems.length;
		
		for (var i=0; i<numFilterItems; i++){
			
			var item = $($('#template').html());
			
			item.attr('id', filterItems[i].id);
			item.attr('area', filterItems[i].area); 
			item.attr('type', filterItems[i].type);
			item.find('.name').html(filterItems[i].name);
			item.find('.bar-description').html(filterItems[i].description);
			item.find('.location-link').attr('href', filterItems[i].locationURL);
			item.find('.location-name').html(filterItems[i].locationName);
			
			if(w < 1024){
				item.css({'width':w+'px'}); 
			}else{
				item.css({'width':'1024px'});
			}
			
			item.appendTo('#carousel');
		}
		var index = 0;
		$("#carousel").caroufredsel({
			auto:{
				play: false
			},
			prev: '#carouselItem-slider-prev',
			next: '#carouselItem-slider-next',
			items:{
				start: index,
				visible: 1
			},
			scroll:{
				onBefore:updateIndicator
			}
		});
		updateIndicator();
		
	},1000);
	*/
	
	$('.carousel-holder').on('click', function () {
	    doCarouselClick(mousex);
	});
	$('.carousel-holder').on('mousemove', function (event) {
	    mousex = event.pageX - $(this).parent().offset().left;
	    mousey = event.pageY - $(this).parent().offset().top;
	    //log('mouse pos: ' + mousex + ', ' + mousey);
	    if (mousex < 150) {
	        $('.btn-left').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    } else if (mousex > $('.carousel-holder').width() - 150) {
	        $('.btn-right').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    } else {
	        $('.btn-left').removeClass('hover');
	        $('.btn-right').removeClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "auto"
	        });
	    }
	})
	
});

function doCarouselClick(x) {
    if (x) {
        if (x < 150) {
            $("#carousel").trigger('prev');
        }
        if (x > $('.carousel-holder').width() - 150) {
            $("#carousel").trigger('next');
        }
    }
}

var _barTypes;
var _provinces;
function getBarDetails(callback){
	$.getJSON(_config.api.bartypes, function (bartypes){
		_barTypes = bartypes;
		$.getJSON(_config.api.barprovinces, function (provinces){
			_provinces = provinces;
			for (var i=0; i<_numItems; i++){
				var bar = _items[i];
				bar.area = getProvinceName(bar.ProvinceId);
				bar.type = getBarTypeName(bar.BarTypeId);
			}
			callback();
		});
	});
}

function getProvinceName(id){
	var province = $.grep(_provinces, function(p){
		return p.ID == id;
	})[0];
	return province.Name;
}

function getBarTypeName(id){
	var barType = $.grep(_barTypes, function(t){
		return t.ID == id;
	})[0];
	return barType.Name;
}

function populateBars(){
	
	for (var f=0; f < _numItems; f++){
		if(_area.toLowerCase() != "all"){
			// if(_items[f].type.toLowerCase() == _type.toLowerCase() && _items[f].area.toLowerCase() == _area.toLowerCase()){
			if(_items[f].BarTypeId == parseInt(_type) && _items[f].ProvinceId == parseInt(_area)){
				_filterItems.push(_items[f]);
			}
		}else{
			// if(_items[f].type.toLowerCase() == _type.toLowerCase()){
			if(_items[f].BarTypeId == parseInt(_type)){
				_filterItems.push(_items[f]);
			}
		}
	}
	_numFilterItems = _filterItems.length;
	
	for (var i=0; i<_numFilterItems; i++){
		
		var item = $($('#template').html());
		
		item.attr('id', _filterItems[i].id);
		item.attr('area', _filterItems[i].area); 
		item.attr('type', _filterItems[i].type);
		item.find('.name').html(_filterItems[i].Name);
		item.find('.bar-description').html(_filterItems[i].Description);
		// item.find('.location-link').attr('href', _filterItems[i].locationURL);
		// item.find('.location-name').html(_filterItems[i].locationName);
		item.find('.location-name').html(_filterItems[i].Address);
		
		if(w < 1024){
			item.css({'width':w+'px'}); 
		}else{
			item.css({'width':'1024px'});
		}
		
		item.appendTo('#carousel');
	}
	var index = 0;
	$("#carousel").caroufredsel({
		auto:{
			play: false
		},
		prev: '#carouselItem-slider-prev',
		next: '#carouselItem-slider-next',
		items:{
			start: index,
			visible: 1
		},
		scroll:{
			onBefore:updateIndicator
		}
	});
	
	updateIndicator();
}


function updateIndicator(){
	$('#carousel').trigger('currentPage', function(x){
		$('.slide-indicator').html((x+1)+"/"+_numFilterItems);
		if(_filterItems[x].type == "Bars"){
		    $('.head').css({
		        "background": "url(images/bg-bars.jpg) no-repeat"
		    })
		}else{
            $('.head').css({
                "background": "url(images/bg-reservebars.jpg) no-repeat"
            })
		}
	});
}
