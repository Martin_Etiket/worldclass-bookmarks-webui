var _amounts=[];
var _cocktailName;
var qs;
var _mustload = {
    'background': false,
    'brand_loaded': false,
    'glass_loaded': false,
    'garnish_loaded': false,
    'mask_loaded': false
}
$(document).ready(function () {


    new WorldClassPreloader();
    var glowLightHolder = $('.carousel-holder .glowing-lights-holder');
    new GlowingLight({ parent: glowLightHolder, left: 160, top: 40, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 220, top: 75, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 140, top: 140, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 500, top: 280, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 320, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 640, top: 120, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 420, top: 110, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 750, top: 220, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 344, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 700, top: 250, colour: '#ffffff' });

    var progress = new ProgressBlock(5);

    $(document).on('showing', function () {
        progress.display();

        var tl = new TimelineMax();
        tl.staggerFrom($('.nameHolder').children(), 0.8, { y: -50, ease: Back.easeOut, opacity: 0, delay: 1.4 }, 0.04)
        tl.staggerFrom($('.btnHolder').children(), 1, { y: 30, ease: Back.easeOut, opacity: 0}, 0.1);
    });


    if (getParameterByName("shared") == "true") {
        $('.normalbuttons').addClass('hidden');
        $('.sharedbuttons').removeClass('hidden');
        $('.progress').addClass('hidden');
    } else {
        $('.normalbuttons').removeClass('hidden');
        $('.sharedbuttons').addClass('hidden');
        $('.progress').removeClass('hidden');
    }

	$.getJSON('js/config.json', function(config){
	    _config = config;

		
		_brandId = parseInt(getParameterByName("brandId"));
		_selectedGlassId = parseInt(getParameterByName("glassId"));
		getBrands();
		_selectedGlassId = getParameterByName("glassId");
		// _selectedMixers = getParameterByName('MixerIDs').split(",");
		mixers = getParameterByName('MixerIDs').split(",");
		//if(mixers.length==1){
		//	_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[0])];
		//}
		//if(mixers.length==2){
		//	_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[1])];
		//}
		//if(mixers.length==3){
		//	_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[1]), parseInt(mixers[2])];
	    //}
		_selectedMixers = mixers;
		_amounts = getParameterByName("amounts").split(',');
		_cocktailName = getParameterByName("name");
		displayMask();
		_garnishId = getParameterByName("garnishId");
		getGlasses(displayGlass);
		getAllGarnishes(displayGarnish);
		getAllMixers(populateMixers);
		doBubbles();
		
		
		
		for(var i=0; i<_selectedMixers.length; i++){
			// _amounts[i] = 10;
			$('#ingredient'+i).removeClass('hidden');
		}
		$('#cocktailName').html(_cocktailName);
		qs = '?brandId='+_brandId+'&glassId='+_selectedGlassId+"&MixerIDs="+_selectedMixers.toString()+"&garnishId="+_garnishId+'&name='+_cocktailName+'&amounts='+_amounts.toString();
		var url = _config.pages.vmSend + qs;
		$('.btn-next').attr('href', url);
	});

	$('.carousel-holder').waitForImages(function () {
	    _mustload.background = true;
	    checkImagesLoaded('.content');
	});

	
});

