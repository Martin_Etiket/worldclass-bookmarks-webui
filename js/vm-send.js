var qs;
var _cocktailName;

$(document).ready(function(){
	$.getJSON('js/config.json', function(config){
		_config = config;
		
		var brandId = parseInt(getParameterByName("brandId"));
		var glassId = parseInt(getParameterByName("glassId"));
		var mixers = getParameterByName('MixerIDs');
		var amounts = getParameterByName("amounts");
		var cocktailName = getParameterByName("name");
		var garnishId = getParameterByName("garnishId");

		qs = '?brandId='+brandId+'&glassId='+glassId+"&MixerIDs="+mixers+"&garnishId="+garnishId+'&name='+cocktailName+'&amounts='+amounts;
	});
	
});