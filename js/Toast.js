﻿
/**
 * Author: Martin McCabe
 * A simple little popup to display quick messages. Seems to be working ok on Mobile too.
 * Free to use as you wish.
 * Requires GSAP TweenMax, TimelineMax, drawSVGPlugin (for club members, just remove drawing of lines if you don't have GSAP Club membership).
 */
(function ($) {

    $.extend({
        toast: function (message, options) {
            var settings = $.extend({
                parent: document.body,
                displayTime: 3,
            }, options);

            var parent = $(settings.parent);
            var container;
            var content;

            if (!container) {
                container = createContainer();
            }

            if (!content) {
                content = createContent();
            }

            content.html("<p>"+message+"</p>");
            container.append(content);
            parent.append(container);

            var cw = content.width();
            var ch = content.height();
            var SVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                SVG.setAttribute("x", "0px");
                SVG.setAttribute("y", "0px");
                SVG.setAttribute("version", "1.1");

            var border = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                border.setAttribute('x', '10');
                border.setAttribute('y', '10');
                border.setAttribute('stroke', '#FFFFFF');
                border.setAttribute('stroke-width', '0.5');
                border.setAttribute('fill', 'none');
            SVG.appendChild(border);
            content.append(SVG);

            $(SVG).css({
                "position": "absolute",
                "left": "0",
                "top": "0",
                "width": "100%",
                "height": "100%"
            });
            SVG.setAttribute("width", $(SVG).width() + "px");
            SVG.setAttribute("height", $(SVG).height() + "px");
            SVG.setAttribute("viewBox", "0 0 " + $(SVG).width() + " " + $(SVG).height());

            $(border).css({
                "width": $(SVG).width() - 20 + "px",
                "height": $(SVG).height() - 20 + "px"
            });
            border.setAttribute('width', $(SVG).width() - 20);
            border.setAttribute('height', $(SVG).height() - 20);

            TweenMax.set(container, { autoAlpha: 0 });
            var tl = new TimelineMax();
                tl.to(container, 0.2, { autoAlpha: 1, ease: Quad.easeInOut })
                .from(content, 0.5, { autoAlpha: 0, y: -20, ease: Quad.easeOut })
                .from(border, 1, { drawSVG: "0% 0%", ease: Quad.easeOut })
                .from(content.find("p"), 0.5, { autoAlpha: 0, ease: Quad.easeOut }, "-=0.5")
                .to(container, 0.5, { autoAlpha: 0, ease: Quad.easeInOut, delay: settings.displayTime });

            function createContainer() {
                var c = $('<div/>');
                c.attr('class', 'toast');
                return c;
            }

            function createContent() {
                var c = $('<div/>');
                c.attr('class', 'toast-content');
                return c;
            }

            return this;
        }
    })

}(jQuery));