function log(str) {
    if (console) {
        console.log(str);
    }
}
function str(o) {
    return JSON.stringify(o);
}

var _ww;
function onResize() {
    _ww = window.innerWidth;
}
$(document).ready(function () {
    new WorldclassButton($('.btn'));
	$('input, textarea').placeholder();
	
	$('.hamburger').on('click', toggleMenuMobile);

	$(window).resize(onResize);
	
	$('.numbersOnly').keyup(function()
	{
		if (validator.isNumeric($(this).val())){
	      //
	    }
	    else
	    {
	      $(this).val("");
	    }
		// return e.charCode >= 48 && e.charCode <= 57;
	});
	
	$('#input_day').keyup(function (e) {
		if(this.value.length == 2){
			$('#input_month').focus();
		}
	});	
	$('#input_month').keyup(function(e){
		if(this.value.length == 2){
			$('#input_year').focus();
		}
	});

	$('#big-button-1').on('mouseenter', function () {
	    if (window.innerWidth > 1023) {
	        var tl = new TimelineMax();
	        tl.to('#home-icon1', 0.1, { rotation: 15 })
            .to($('#home-icon1'), 0.1, { rotation: -15 })
	        .to($('#home-icon1'), 0.1, { rotation: 0 })
	    }
	});
	$('#big-button-2').on('mouseenter', function () {
	    if (window.innerWidth > 1023) {
	        var tl = new TimelineMax();
	        tl.set('#big-button-2 .icon-over', { transformOrigin: "0% 100%" })
	        tl.to('#big-button-2 .icon-over', 0.1, { rotation: 15 })
            .to($('#big-button-2 .icon-over'), 0.1, { rotation: -15 })
	        .to($('#big-button-2 .icon-over'), 0.1, { rotation: 0 })
	    }
	});
});

var menuOpen = false;
function toggleMenuMobile(){
	
	if(menuOpen){
		$('.hamburger').removeClass('open');
		$('.nav').removeClass('open');
	}else{
		$('.hamburger').addClass('open');
		$('.nav').addClass('open');
	}
	
	menuOpen = !menuOpen;
	
	//dropdowns on bars page
	$('.dropdown').removeClass('open');
	$('.dropdown .btn-dropdown-option').removeClass('open'); 
	dropdownBarOpen=false;
	$('.dropdown-reserve').removeClass('open');
	$('.dropdown-reserve .btn-dropdown-option').removeClass('open'); 
	dropdownReserveOpen = false;
}


/**
 * read query string to get parameter by Name
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Preloader to be used on pages while images are loaded.
 */
function WorldClassPreloader(options) {
    options = options || {};
    var parent = options.parent || document.body;
    var element = this.element = document.createElement("div");
    $(element).css({
        'width': options.radius || '100' + 'px',
        'height': options.radius || '100' + 'px',
        'position': 'absolute',
        'top': options.top || '30%',
        'left': options.left || '50%'
    });
    if (window.innerWidth  <= 480) {
        $(element).css({
            'top': options.top_mobile || options.top || '30%',
            'left': options.left_mobile || options.left || '50%'
        });
    }

    // cater for dem mobiles.
    $(window).resize(function () {
        if (window.innerWidth <= 480) {
            $(element).css({
                'top': options.top_mobile || options.top || '30%',
                'left': options.left_mobile || options.left || '50%'
            });
        } else {
            $(element).css({
                'top': options.top || '30%',
                'left': options.left || '50%'
            });
        }
    });

    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("width", "100px");
        svg.setAttribute("height", "100px");
        svg.setAttribute("version", "1.1");
        $(svg).css({
            'position': 'absolute',
            'left':'-50px'
        });
    var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        circle.setAttribute('id', 'worldclasspreloader-circle');
        circle.setAttribute('fill', 'none');
        circle.setAttribute('stroke', options.stroke || '#F47A20');
        circle.setAttribute('stroke-width', options.strokeWidth || '4');
        circle.setAttribute('stroke-miterlimit', options.strokeMiterLimit || '10');
        circle.setAttribute('cx', options.cx || '50');
        circle.setAttribute('cy', options.cy || '50');
        circle.setAttribute('r', options.r || '20');
    document.body.appendChild(circle);
    document.body.removeChild(circle);
    svg.appendChild(circle);
    $(element).attr('id', options.id || 'preloader');
    element.appendChild(svg);
    $(parent).append(element);
    var tl = new TimelineMax({ repeat: -1, repeatDelay: 0});
        tl.set(circle, {drawSVG:"0%"})
        .to(circle, 0.5, { drawSVG: "20% 80%", ease: Quad.easeIn, delay: 0.3 })
        .to(circle, 0.5, { drawSVG: "100% 100%", ease: Quad.easeOut });

        return $(element);
    /*
    var isActive;
    var active = function (show) {
        if (!arguments.length) {
            return isActive;
        }

        if (isActive == show) {
            //deactivate
        } else {
            // activate
        }
    }*/ // Later, maybe.
}
var _mustload;
function checkImagesLoaded(content) {
    var count = 0
    var loaded = 0;
    //log(str(_mustload))
    $.each(_mustload, function (k, v) {
        count++;
        if (v == true) {
            loaded++;
        }
    });
    if (count == loaded && count != 0) {
        log('all loaded');
        showContent(content);
    }
    else {
        log('not all loaded');
    }
}

function showContent(content) {
    TweenLite.to(content, 1, { autoAlpha: 1, ease: Quad.easeInOut });
    TweenLite.to('#preloader', 0.3, { autoAlpha: 0, ease: Quad.easeInOut });
    $(document).trigger('showing');
}

/**
 * glowing effect to simulate lights 'flickering' in backgrounds
 * options: parent, top, left, colour
 */
function GlowingLight(options) {
   
    if (detectIE() == false) { // not in IE, sorry.
        options = options || {};
        var parent = options.parent || document.body;
        var element = this.element = document.createElement("div");
        element.setAttribute('class', 'GlowingLight');

        // randomise this a bit
        var t = Math.random();
        var opacityMin = Math.random() / 3;
        var opacityMax = Math.random() + 0.4;
        var radius = (10 * Math.random()) + 2;
        //var radius = 20;

        // style
        $(element).css({
            //'width': options.radius || '10' + 'px',
            //'height': options.radius || '10' + 'px',
            'width': radius + 'px',
            'height': radius + 'px',
            'position': 'absolute',
            'left': options.left || '100' + 'px',
            'top': options.top || '100' + 'px',
            'background': options.colour || '#fdfdde',
            //'background':'#ff0000',
            'border-radius': '5px',
            //'opacity': opacityMin
        })
        //TweenLite.set(element, {
        //    webkitFilter: "blur(5px)",
        //    filter: "blur(5px)",
        //    msFilter:"blur(5px)"
        //});
        $(element).css({
            '-webkit-filter': 'blur(5px)',
            '-ms-filter': 'blur(5px)',
            'filter': 'blur(5px)'
        });
        $(parent).append(element);

        // animate
        var flickerAnimation = new TimelineMax({ repeat: -1, repeatDelay: t });
        flickerAnimation.fromTo(element, 1, { alpha: opacityMin }, { alpha: opacityMax, delay: t, ease: Quad.easeIn })
        .to(element, 1, { alpha: opacityMin, ease: Quad.easeOut });
    }
} // end GlowingLight

/*
 * found this here: http://stackoverflow.com/questions/27165948/how-to-wait-for-images-loaded-with-javascript
 * Preload an image if not cached, then call a callback function
 * @param {String} the image url
 * @param {Function} the callback function
 *
 *  example callback:
 *  -----------------
 *  var callback = function (loaded){
 *     if(loaded){
 *          var img = this // now 'this' refers to the image itself.
 *     }
 *  }
*/
function loadImg(url, fn, params) {
    var img = new Image();
    img.src = url;
    if (img.complete) { // If the image has been cached, just call the callback
        if (fn) fn.call(img, true, params);
    } else {
        img.onerror = function () { // If fail to load the image
            if (fn) fn.call(img, false, params);
        };
        img.onload = function () { // If loaded successfully
            if (fn) fn.call(img, true, params);
            //On IE6, multiple frames in a image will fire the 'onload' event for multiple times. So set to null
            img.onload = null;
        };
    };
} // end loadImg;

function WorldclassButton(element) {
    if (element.length) {
        var label = element.html();

        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("width", "115px");
        svg.setAttribute("height", "45px");
        svg.setAttribute("viewBox", "0 0 115 45");
        svg.setAttribute("version", "1.1");

        var bg = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        bg.setAttribute("fill", "#f47a20");
        bg.setAttribute("width", "115");
        bg.setAttribute("height", "45");
        svg.appendChild(bg);

        var border = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        border.setAttribute("x", "5");
        border.setAttribute("y", "6");
        border.setAttribute("stroke", "#FFFFFF");
        border.setAttribute("stroke-width", "0.5");
        border.setAttribute("stroke-miterlimit", "10");
        border.setAttribute("width", "104");
        border.setAttribute("height", "34");
        border.setAttribute("fill", "none");
        svg.appendChild(border);

        var dot = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        dot.setAttribute("x", "55");
        dot.setAttribute("y", "38");
        dot.setAttribute("transform", "matrix(0.7072 0.707 -0.707 0.7072 44.8878 -28.5832)");
        dot.setAttribute("fill", "#FFFFFF");
        dot.setAttribute("width", "4");
        dot.setAttribute("height", "4");
        svg.appendChild(dot);
        var dot2 = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        dot2.setAttribute("x", "55");
        dot2.setAttribute("y", "38");
        dot2.setAttribute("transform", "matrix(0.7072 0.707 -0.707 0.7072 44.8878 -28.5832)");
        dot2.setAttribute("fill", "#FFFFFF");
        dot2.setAttribute("width", "4");
        dot2.setAttribute("height", "4");
        svg.appendChild(dot2);

        element.append($(svg));

        TweenMax.from(border, 1, { drawSVG: "0% 0%", delay: 0.8, ease: Quad.easeOut });
        element.on('mouseover', function () {
            TweenMax.to(dot, 0.3, { x: -50, scale: 0.5, y: 1 });
            TweenMax.to(dot2, 0.3, { x: 54, scale: 0.5, y: 1 });
            TweenMax.to(border, 0.5, { strokeWidth: 1, ease: Quad.easeInOut });
        });
        element.on('mouseout', function () {
            TweenMax.to(dot, 0.3, { x: 0, scale: 1, y: 0 });
            TweenMax.to(dot2, 0.3, { x: 0, scale: 1, y: 0 });
            TweenMax.to(border, 0.5, { strokeWidth: 0.5 });
        });
    }
}


function detectIE() {
    var ua = window.navigator.userAgent;

    // test values
    // IE 10
    //ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
    // IE 11
    //ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    // IE 12 / Spartan
    //ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // IE 12 => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}