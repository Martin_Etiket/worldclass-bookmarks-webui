var _w;
var _featured;
var _config;
var _mustload = {
    "featured": false,
    "bt1": false,
    "bt2": false,
    "bt3": false,
    "bt4": false,
    "bt5": false,
}
$(document).ready(function(){
    // onresize();
    new WorldClassPreloader();
	$(window).resize(onresize);
	
	$.getJSON('js/config.json', function(config){
	    _config = config;
	    $('.bartenders-list').css({ 'opacity': '0' });
	    getBartenders();
	});
});



function getBartenders() {
    $.getJSON(_config.api.bartenders, function (bartenders){
        for (var i=0; i<bartenders.length; i++)
        {
            var b = bartenders[i];
            if (b.Featured == true){
                _featured = b;
                var img = _config.vmImageFolder + b.Image_DesktopName;
                loadImg(img, function (success, params) {
                    if (success) {
                        $('.bartenders .featured').css({'background':'url('+$(this).attr("src")+') no-repeat'});
                        $('.bartenders .featured').find('.name').html(params[0].Name);
                        $('.bartenders .featured').find('a').attr('href', _config.pages.bartendersDetail + '?selected=' + params[1]);
                        _mustload.featured = true;
                        checkImagesLoaded('.content');
                    }
                }, [b, i]);
                //$('.bartenders .featured').css({ 'background': 'url(' + img + ') no-repeat' });
                //$('.bartenders .featured').find('.name').html(b.Name);
                //$('.bartenders .featured').find('a').attr('href', _config.pages.bartendersDetail + '?selected=' + i);
				
            }else{
                var template = $($('#btTemplate').html());
                var img = _config.vmImageFolder + b.Image_ThumbName;
                template.addClass('bartender');
                template.attr('id', 'bt' + i);
                
                //template.find('img').attr('src', img);
                template.find('.name').html(b.Name);
                var link = _config.pages.bartendersDetail + '?selected=' + i;
                template.find('a').attr('href', link);
                template.on('click', function () {
                    window.location = $(this).find('a').attr('href');
                });
                template.appendTo($('.bartenders-list'));
                loadImg(img, function (success, id) {
                    if (success) {
                        id = id.toString();
                        log(id)
                        var bartender = $("#bt" + id);
                        bartender.find('.bartender-pic').find('img').attr('src', $(this).attr('src'));
                        _mustload['bt' + id] = true;
                        checkImagesLoaded('.content');
                    }
                }, i)
            }
        }
        setTimeout(onresize, 1000);
		
        $('.bartender').on('mouseover', function () {
            $(this).find('a').addClass('hover');
            $(this).find('.bartender-pic').css({
                "overflow": "hidden"
            });
            TweenMax.to($(this).find('.bartender-pic img'), 0.8, { scaleX: 1.05, scaleY: 1.05, transformOrigin: "50% 50%", force3D:true, ease: Quad.easeOut });
        });
        $('.bartender').on('mouseout', function () {
            $(this).find('a').removeClass('hover');
            TweenMax.to($(this).find('.bartender-pic img'), 0.8, { scaleX: 1, scaleY: 1, transformOrigin: "50% 50%", force3D: true, ease: Quad.easeOut });
        });
       
    });
}

function onresize(){
	_w = window.innerWidth;
	var h = ($('.bartender').width()/2);
	$('.bartender').css('height', h+"px");
	// $('.bartender-info').css({'height':h+'px', 'display':'table-cell','vertical-align':'middle'});
	$('.bartender-info').css({'height':h+'px'});
	
	if(_w<1024){
	    var fh = _w + (_w * 0.07);
	    if (_featured) {
		    var img = _config.vmImageFolder + _featured.Image_MobileName;
	    }
		$('.featured').css({'background':'url('+img+') no-repeat','background-size': 'contain', 'height':fh+"px"});
	}else{
	    if (_featured) {
	    	var img = _config.vmImageFolder + _featured.Image_DesktopName;
	    }
		$('.featured').css({'background':'url('+img+') no-repeat','height':"600px"});
	}
	
	
	var bartenderArr = $('.bartender').toArray();
	if(_w <= 480){
		for(var i=0; i<bartenderArr.length; i++){
			var bt = $(bartenderArr[i]);
			if(i>0 && i % 2 != 0){
				bt.addClass('right');
			}else{
				bt.removeClass('right');
			}
		}	
	}else{
		var colcount = 0;
		var rowcount = 0;
		var lastClass;
		for(var i=0; i<bartenderArr.length; i++){
			var bt = $(bartenderArr[i]);
			if(rowcount==0 || rowcount % 2 === 0){
				bt.removeClass('right');
				lastClass = "left";
			}else{
				bt.addClass('right');
				lastClass = "right";
			}
			if(i==bartenderArr.length-1){
				bt.addClass('center');
				if(lastClass == "right"){
					bt.removeClass('right');
				}else{
					bt.addClass('right');
				}
			}
			colcount++;
			if(colcount == 2){
				rowcount++;
				colcount= 0;
			}
		}
		
	}
	$('.bartenders-list').css({ 'opacity': '1' });
}
