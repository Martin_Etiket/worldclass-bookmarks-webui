_mustload = {
    //"head":false,
    "reserve": false,
    "gauteng": false,
    "wc": false,
    "kzn": false,
    "all":false
}

var imgs = [
    { "file": "btn-bar-reserve.png", "mustload": "reserve" },
    { "file": "btn-bar-gauteng.png", "mustload": "gauteng" },
    { "file": "btn-bar-wc.png", "mustload": "wc" },
    { "file": "btn-bar-kzn.png", "mustload": "kzn" },
    { "file": "btn-bar-all.png", "mustload": "all" },
]
$(document).ready(function () {
    new WorldClassPreloader();

	$('#btn-dropdown-bar').on('click', toggleDropdownBar);
    // $('#btn-dropdown-reserve').on('click', toggleDropdownReserve);

	videoPlay();

	$('.btn-bar').on('mouseover', function () {
	    $(this).find('a').addClass('hover');
	});
	$('.btn-bar').on('mouseout', function () {
	    $(this).find('a').removeClass('hover');
	});
	$('.btn-bar').on('click', function () {
	    window.location = $(this).find('a').attr('href');
	});

	/*$('.reserve').waitForImages(function () {
	    _mustload.reserve = true;
	    checkImagesLoaded('.content');
	});
	$('.gauteng').waitForImages(function () {
	    _mustload.gauteng = true;
	    checkImagesLoaded('.content');
	});
	$('.wc').waitForImages(function () {
	    _mustload.wc = true;
	    checkImagesLoaded('.content');
	});
	$('.kzn').waitForImages(function () {
	    _mustload.kzn = true;
	    checkImagesLoaded('.content');
	});
	$('.all').waitForImages(function () {
	    _mustload.all = true;
	    checkImagesLoaded('.content');
	});*/

	for (var i=0; i<imgs.length; i++)
	{
	    loadImg('images/' + imgs[i].file, function (s, index) {
	        if (s) {
	            _mustload[imgs[index].mustload] = true;
	            checkImagesLoaded('.content');
	        }
	    }, i);
	}

	$(document).on('showing', function () {
	    var tl = new TimelineMax();
	    tl.staggerFrom($('.btn-bar').toArray(), 0.5, { y: 50, opacity: 0, ease: Back.easeOut, delay: 0.4 }, 0.1);
	})
});

function videoPlay() {
    TweenMax.delayedCall(20, function () {
        document.getElementById('bar-video').play();
        videoPlay();
    })
}

var dropdownBarOpen = false;
var dropdownReserveOpen = false;
function toggleDropdownBar(){
	if(dropdownBarOpen){
		$('.dropdown').removeClass('open');
		$('.dropdown .btn-dropdown-option').removeClass('open'); 
	}else{
		$('.dropdown').addClass('open');
		$('.dropdown .btn-dropdown-option').addClass('open'); 
		
		if(dropdownReserveOpen){
			toggleDropdownReserve();
		}
	}
	dropdownBarOpen = !dropdownBarOpen;
}
function toggleDropdownReserve(){
	if(dropdownReserveOpen){
		$('.dropdown-reserve').removeClass('open');
		$('.dropdown-reserve .btn-dropdown-option').removeClass('open'); 
	}else{
		$('.dropdown-reserve').addClass('open');
		$('.dropdown-reserve .btn-dropdown-option').addClass('open'); 
		if(dropdownBarOpen){
			toggleDropdownBar();
		}
	}
	dropdownReserveOpen = !dropdownReserveOpen;
}
