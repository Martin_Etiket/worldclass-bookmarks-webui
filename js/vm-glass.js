var _mustload = {
    'background': false,
    'brand_loaded': false,
    'glass_loaded': false

}
var _glassloader;
$(document).ready(function () {
    new WorldClassPreloader();
    var glowLightHolder = $('.carousel-holder .glowing-lights-holder');
    new GlowingLight({ parent: glowLightHolder, left: 160, top: 40, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 220, top: 75, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 140, top: 140, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 500, top: 280, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 320, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 640, top: 120, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 420, top: 110, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 750, top: 220, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 344, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 700, top: 250, colour: '#ffffff' });

    var progress = new ProgressBlock(2);
    $(document).on('showing', function () {
        progress.display();
    });

	$.getJSON('js/config.json', function(config){
		_config = config;
		getBrands();
		getGlasses(clickFirstGlass);
	});
	
	$('#carouselItem-slider-prev').on('click', prevGlass);
	$('#carouselItem-slider-next').on('click', nextGlass);
	
	$('.carousel-holder').waitForImages(function () {
	    _mustload.background = true;
	    checkImagesLoaded('.content');
	});
	_glassloader = new WorldClassPreloader({ parent: $('.mixHolder'), id: 'glassPreloader', top: '250px', left: '290px', r: '10px', top_mobile: '100px', left_mobile:'50%' });

	

	new GlassIcon($('#glass-icon1'));
	new GlassIcon($('#glass-icon2'));
	new GlassIcon($('#glass-icon3'));
	new GlassIcon($('#glass-icon4'));
	new GlassIcon($('#glass-icon5'));
	new GlassIcon($('#glass-icon6'));
	new GlassIcon($('#glass-icon7'));
	new GlassIcon($('#glass-icon8'));

	
});

// "click" first glass when glasses all loaded
function clickFirstGlass() {
    $('#glass-icon1').trigger('click');
}


function selectGlass(x) {
    TweenMax.set(_glassloader, { autoAlpha: 1 });
	if(x==undefined){
		x = 0;
	}
	var glass = _glasses[x];
	var img = _config.vmImageFolder + glass.Image_DesktopName;
	_selectedGlassId = glass.ID;
	_selectedGlass = x;
	//$('.glassHolder').css({ 'background-image': 'none' }); // to load new image
	/*$('.glassHolder').css({ 'background': 'url(' + img + ')', 'background-size': 'contain' });
	$('.glassHolder').waitForImages(function () {
	    _mustload.glass_loaded = true;
	    checkImagesLoaded('.content');
	    TweenMax.to(_glassloader, 0.3, { autoAlpha: 0, ease: Quad.easeInOut });
	});*/
    //TweenMax.set($('.glassHolder'), { opacity: 0 });
	var glassHolder = $('.glassHolder');

    // move glass away then back from the right when new one's loaded
	TweenMax.to(glassHolder, 0.2, {
	    opacity: 0,
	    x: -100,
	    ease: Quad.easeIn,
	    onComplete: function () {

	        loadImg(img, function (success) {
	            if (success) {
	                glassHolder.css({ 'background': 'url(' + $(this).attr('src') + ')', 'background-size': 'contain' });
	                //TweenMax.from($('.glassHolder'), 0.2, { opacity: 0, y: -10, ease: Quad.easeInOut });
	                TweenMax.fromTo(glassHolder, 0.3, { opacity: 0, x: 400 }, { opacity: 1, x: 0, ease: Quad.easeOut });
	                _mustload.glass_loaded = true;
	                checkImagesLoaded('.content');
	                TweenMax.to(_glassloader, 0.3, { autoAlpha: 0, ease: Quad.easeInOut });

	                $('.glassDescriptionHolder .glassName').html(glass.Name);
	                //TweenMax.to($('.glassDescriptionHolder .glassName'), 0.8, { scrambleText: { text: glass.Name, chars:'lowercase' } });
	                TweenMax.from($('.glassDescriptionHolder .glassName'), 0.4, { y: 50, ease: Back.easeOut });
	                $('.glassDescriptionHolder .glassDescription').html(glass.Description);
	                //TweenMax.to($('.glassDescriptionHolder .glassDescription'), 1, { scrambleText: { text: glass.Description, chars: 'lowercase' } });
	                //TweenMax.from($('.glassDescriptionHolder .glassDescription'), 0.8, {y: 20, opacity: 0});
	                TweenMax.set($('.glassDescriptionHolder .glassDescription'), { perspective: 400 });
	                var st = new SplitText($('.glassDescriptionHolder .glassDescription'), { type: 'lines' });
	                TweenMax.staggerFrom(st.lines, 0.5, {
	                    y: 50,
                        //z: -200,
	                    //rotationX: -90,
	                    opacity: 0,
                        //onComplete: function () {
	                    //    st.revert();
                        //},
                        ease: Back.easeOut
	                }, 0.05);
	                var url = _config.pages.vmMixers + '?brandId=' + _brandId + "&glassId=" + _selectedGlassId;
	                $('.btn').attr('href', url);
	            }
	        }); // end loadImg
	    }
	});
}

function nextGlass(){
	if(_selectedGlass<_numGlasses-1){
		selectGlass(_selectedGlass+1);
	}else{
		selectGlass(0);
	}
}
function prevGlass(){
	if(_selectedGlass>0){
		selectGlass(_selectedGlass-1);
	}
	else{
		selectGlass(_numGlasses-1);
	}
}

function GlassIcon(element) {
    element.css({ 'cursor': 'pointer' });
    var svg = element.find('.icon-over');
    TweenMax.set(svg, { drawSVG: "50% 50%" });

    var label = element.find('.icon-label');
    TweenMax.set(label, { autoAlpha: 0, x: -15 });

    element.on('mouseover', function () {
        if (!element.hasClass('active')) {
            TweenMax.to(svg, 0.6, { drawSVG: "0% 100%", ease: Quad.easeOut });
            TweenMax.to(label, 0.3, { autoAlpha: 1, x: 0, ease: Quad.easeOut });
        }
    });

    element.on('mouseout', function () {
        if(!element.hasClass('active')){
            TweenMax.to(svg, 0.5, { drawSVG: "50% 50%", ease: Quad.easeIn });
            TweenMax.to(label, 0.3, { autoAlpha: 0, x: -15, ease: Quad.easeIn });
        }
    });

    element.on('click', function () {
        // reset all
        $('.glass-icon').removeClass('active');
        TweenMax.to($('.glass-icon').find('.icon-over'), 0.5, { drawSVG: "50% 50%", ease: Quad.eastIn });
        TweenMax.to($('.glass-icon').find('.icon-label'), 0.3, { autoAlpha: 0, x: -15, ease: Quad.easeIn });

        // update this
        $(this).addClass('active');
        TweenMax.killTweensOf(svg);
        TweenMax.to(svg, 0.5, { drawSVG: "0% 100%", ease: Quad.easeOut });
        TweenMax.to(label, 0.3, { autoAlpha: 1, x: 0, ease: Quad.easeOut });

        //select glass
        // sorry, this is probably the worst way to do this. :(
        var id;
        switch (element.attr('id')) {
            case 'glass-icon1':
                id = 0;
                break;
            case 'glass-icon2':
                id = 1;
                break;
            case 'glass-icon3':
                id = 2;
                break;
            case 'glass-icon4':
                id = 3;
                break;
            case 'glass-icon5':
                id = 4;
                break;
            case 'glass-icon6':
                id = 5;
                break;
            case 'glass-icon7':
                id = 6;
                break;
            case 'glass-icon8':
                id = 7;
                break;

        }
        selectGlass(id);
    });
}