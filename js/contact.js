﻿_mustload = {
    'bg':false
}

$(document).ready(function () {
    new WorldClassPreloader();

    loadImg('images/bg-contact.jpg', function (s) {
        if (s) {
            _mustload.bg = true;
            checkImagesLoaded('.content');
        }
    })

    $(document).on('showing', function () {
        var tl = new TimelineMax();
        var arr = $('.social-icons .social-icon').toArray();
        tl.staggerFrom(arr, 1, { y: 30, autoAlpha: 0, delay: 1, ease: Back.easeOut }, 0.1);
    })
});