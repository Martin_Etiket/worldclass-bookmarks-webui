
var items;
var _mustload = {
    "bartender": false
}
var _prelaoder;
$(document).ready(function () {

    _prelaoder = new WorldClassPreloader();

    $('.content').css({'opacity': '1'})
	$.getJSON('js/config.json', function(config){
	    _config = config;

	    selectedId = getParameterByName('selected');
		getBartenders(showFirstItem);
		
	});

	$('.carousel-holder').on('click', function () {
	    doCarouselClick(mousex);
	});
	$('.carousel-holder').on('mousemove', function (event) {
	    mousex = event.pageX - $(this).parent().offset().left;
	    mousey = event.pageY - $(this).parent().offset().top;
	    //log('mouse pos: ' + mousex + ', ' + mousey);
	    if (mousex < 150) {
	        $('.btn-left').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    } else if (mousex > $('.carousel-holder').width() - 150) {
	        $('.btn-right').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    } else {
	        $('.btn-left').removeClass('hover');
	        $('.btn-right').removeClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "auto"
	        });
	    }
	})


});

function doCarouselClick(x) {
    if (x) {
        if (x < 150) {
            prevItem();
        }
        if (x > $('.carousel-holder').width() - 150) {
            nextItem();
        }
    }
}


function getBartenders(callback){
	$.getJSON(_config.api.bartenders, function (bartenders){
		items = bartenders;
		numItems = items.length;
		callback();
	});
}

function showItem(pos) {
    _mustload.bartender = false;
    TweenMax.to($('.content'), 0.2, { autoAlpha: 0, ease: Quad.easeInOut });
    TweenMax.to('#preloader', 0.3, { autoAlpha: 1, ease: Quad.easeInOut });

    var head = $('.carousel-holder .head');
	var item = $('#carousel-item-holder');
	var t = 0.3;
	//var t = 2;
	var pos = pos;
	// hide current slide
	TweenMax.to(item, t, {opacity:0, y: 50, ease: Quad.easeInOut});
    TweenMax.to(head, t, {opacity: 0, x: -800, ease: Quad.easeInOut, onComplete: function(){
		if(_w>=1024)
		{
		    var img = _config.vmImageFolder + items[pos].Image_DesktopName;
		    loadImg(img, function (success) {
		        if (success) {
		            $('.carousel-holder .head').css({ 'background': 'url(' + $(this).attr('src') + ')' });
		            _mustload.bartender = true;
		            checkImagesLoaded('.content');
		        }
		    });
			//$('.carousel-holder .head').css({'background': 'url('+img+')'});
		}else{
			
		    var img = _config.vmImageFolder + items[pos].Image_MobileName;
		    loadImg(img, function (success) {
		        if (success) {
		            $('.carousel-holder .head').css({ 'background': 'url(' + $(this).attr('src') + ') no-repeat', 'width': '100%', 'height': (_w + (_w * 0.07)) + 'px', 'background-size': 'contain' });
		            _mustload.bartender = true;
		            checkImagesLoaded('.content');
		        }
		    });
			//$('.carousel-holder .head').css({'background': 'url('+img+') no-repeat', 'width':'100%','height':(_w+(_w*0.07))+'px', 'background-size':'contain'});
		}
		
		
		var template = $($('#template').html());
		
		
		template.find('.name').html(items[pos].Name);
		template.find('.q1').html(items[pos].Question1);
		template.find('.q2').html(items[pos].Question2);
		template.find('.q3').html(items[pos].Question3);
		template.find('.q4').html(items[pos].Question4);
		template.find('.q5').html(items[pos].Question5);
		template.find('.q6').html(items[pos].Question6);
		template.find('.a1').html(items[pos].Answer1);
		template.find('.a2').html(items[pos].Answer2);
		template.find('.a3').html(items[pos].Answer3);
		template.find('.a4').html(items[pos].Answer4);
		template.find('.a5').html(items[pos].Answer5);
		template.find('.a6').html(items[pos].Answer6);
		
		// $('#carousel-item-holder').html($('#carouselItem'+pos).html()); //template.html()
		$('#carousel-item-holder').html(template.html()); //template.html()
		currentItem = pos;
		TweenMax.set(head, {
		    x: 800
		})
		TweenMax.to(head, t, {opacity:1, x: 0, ease: Quad.easeInOut});
		TweenMax.to(item, t, {opacity:1, y: 0, ease: Quad.easeInOut, delay: t* 0.5});
	}});
}

