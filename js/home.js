
var _mustload = {
    'featured_bartender_desktop': false,
    'featured_bartender_mobile': false,
    'featured_cocktail_desktop': false,
    'featured_cocktail_mobile': false
};

var _preloader;

$(document).ready(function () {

    _preloader = new WorldClassPreloader();

	$(window).resize(onresize);
	onresize();
	
	$.getJSON('js/config.json', function(config){
		_config = config;
		
		$.getJSON(_config.api.bartenders, function (bartenders){
			
			var featuredBartender = $.grep(bartenders, function (b){
				return b.Featured == true;
			})[0];
			
			var bt = _config.vmImageFolder + "/" + featuredBartender.Image_FeaturedDesktopName;
			$('.featured-bartender').css({ 'background': 'url(' + bt + ') no-repeat', 'background-size':'105%'});
			$('#featured-bartender-desktop').waitForImages(function () {
			    _mustload.featured_bartender_desktop = true;
			    checkImagesLoaded('.home');
			})
			var btm = _config.vmImageFolder + "/" + featuredBartender.Image_FeaturedMobileName;
			$('.mobile .featured-bartender').css({ 'background': 'url(' + btm + ') no-repeat', 'background-size': 'contain' });
			$('#featured-bartender-mobile').waitForImages(function () {
			    _mustload.featured_bartender_mobile = true;
			    checkImagesLoaded('.home');
			});
			   
		});
		
		
		$.getJSON(_config.api.cocktails, function (cocktails){
			
			var featuredCocktail = $.grep(cocktails, function (b){
				return b.Featured == true;
			})[0];
			
			var cd = _config.vmImageFolder + "/" + featuredCocktail.Image_FeaturedDesktopName;
			$('.featured-cocktail').css({ 'background': 'url(' + cd + ') no-repeat', 'background-size': '105%' });
			$('#featured-cocktail-desktop').waitForImages(function () {
			    _mustload.featured_cocktail_desktop = true;
			    checkImagesLoaded('.home');
			})

			var cm = _config.vmImageFolder + "/" + featuredCocktail.Image_FeaturedMobileName;
			$('.mobile .featured-cocktail').css({ 'background': 'url(' + cm + ') no-repeat', 'background-size': 'contain' });
			$('#featured-cocktail-mobile').waitForImages(function () {
			    _mustload.featured_cocktail_mobile = true;
			    checkImagesLoaded('.home');
			})
		});
		
	});

	$('.featured').on('mouseover', function () {
	    ////TweenMax.set($(this), { backgroundSize: "105%" });
	    //TweenMax.to($(this), 2, { backgroundSize: "100%" });
	    TweenMax.set($(this), { css: { 'background-size': '105%' } });
	    TweenMax.to($(this), 2, { css: { 'background-size': '100%' }, transformOrigin: "50% 50%", force3D: true });
	});
	$('.featured').on('mouseout', function () {
	    TweenMax.to($(this), 1, { css: { 'background-size': '105%' }, transformOrigin: "50% 50%", force3D: true });
	});

});

/*function checkImagesLoaded() {
    var count = 0
    var loaded = 0;
    $.each(_mustload, function (k, v) {
        count++;
        if (v == true) {
            loaded++;
        }
    });
    if (count == loaded && count != 0) {
        log('all loaded');
        showContent();
    }
    else{
        log('not all loaded');
    }
}

function showContent() {
    TweenMax.to('.home', 1, { autoAlpha: 1, ease: Quad.easeInOut });
    TweenMax.to('.preloader', 0.3, { autoAlpha: 0, ease: Quad.easeInOut });
}*/

function onresize(){
	var h = $('.mobile .featured').width();
	$('.mobile .featured').css({'height':h+"px","background-size":"contain"});
}
