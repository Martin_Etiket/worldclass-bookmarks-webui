﻿var _mustload = {
    'background':false
}
$(document).ready(function () {

    new WorldClassPreloader();
    var lightParent = $('.lights');
    new GlowingLight({ parent: lightParent, left: 200, top: 17 });
    new GlowingLight({ parent: lightParent, left: 267, top: 67 });
    new GlowingLight({ parent: lightParent, left: 313, top: 170 });
    new GlowingLight({ parent: lightParent, left: 413, top: 210 });
    new GlowingLight({ parent: lightParent, left: 660, top: 160 });
    new GlowingLight({ parent: lightParent, left: 680, top: 150 });
    new GlowingLight({ parent: lightParent, left: 210, top: 200 });
    new GlowingLight({ parent: lightParent, left: 100, top: 280 });
    new GlowingLight({ parent: lightParent, left: 760, top: 250 });

    $('.lights').css({
        'filter': 'blur(5px)',
        '-webkit-filter': 'blur(5px)',
        '-ms-filter': 'blur(5px)',
    })

    $('.content').waitForImages(function () {
        _mustload.background = true;
        checkImagesLoaded('.content');
    });

    //new WorldclassButton($('#btn-getStarted'));

});