var _mustload = {
    'background': false,
    'mixers': false
}

$(document).ready(function () {
    //TweenMax.delayedCall(2, function () {
    //    $.toast("Neat is great, but are you sure you don't want to choose a mixer? ");
    //});
    //TweenMax.delayedCall(10, function () {
    //    $.toast("this is my second message to you...");
    //})

	$.getJSON('js/config.json', function(config){
		_config = config;
		
	    //$('.btn').addClass('hidden');
		$('.btn').on('click', function () {
		    var url = $(this).attr('href');
		    log('url: ' + url);
		    if (url == "#") {
		        //toastr["info"]("Neat is great, but are you sure you don't want to choose a mixer?");
		        $.toast("Neat is great, but are you sure you don't want to choose a mixer? ");
		        return false;
		    } else {
		        return true;
		    }
		});
		
		_brandId = getParameterByName('brandId');
		_selectedGlassId = getParameterByName('glassId');
		
		getMixersForBrand( getMixerNames );
		
	});
	
	//toastr.options = {
	//  "closeButton": false,
	//  "debug": false,
	//  "newestOnTop": false,
	//  "progressBar": false,
	//  "positionClass": "toast-coverall",
	//  "preventDuplicates": false,
	//  "onclick": null,
	//  "showDuration": "300",
	//  "hideDuration": "1000",
	//  "timeOut": "5000",
	//  "extendedTimeOut": "1000",
	//  "showEasing": "swing",
	//  "hideEasing": "linear",
	//  "showMethod": "fadeIn",
	//  "hideMethod": "fadeOut"
	//};
	
	// toastr["info"]("Sometimes, less is more. Try stick to 3 mixers.");
	
	new WorldClassPreloader();
	var glowLightHolder = $('.carousel-holder .glowing-lights-holder');
	new GlowingLight({ parent: glowLightHolder, left: 160, top: 40, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 220, top: 75, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 140, top: 140, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 500, top: 280, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 550, top: 320, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 640, top: 120, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 420, top: 110, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 750, top: 220, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 344, top: 184, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 550, top: 184, colour: '#ffffff' });
	new GlowingLight({ parent: glowLightHolder, left: 700, top: 250, colour: '#ffffff' });

	var progress = new ProgressBlock(3);
	$(document).on('showing', function () {
	    TweenMax.delayedCall(1, progress.display);
	    //progress.display();
	    TweenMax.staggerFrom('.mixer', 1, { x: 100, autoAlpha: 0, ease: Back.easeOut, delay: 0.8 }, 0.04);
	});

	$('.carousel-holder').waitForImages(function () {
	    _mustload.background = true;
	    checkImagesLoaded('.content');
	});

	
});

var _numMixers;

function getMixerNames(){
	_numMixers = _mixers.length;
	// console.log(JSON.stringify(_mixers));
	getMixerName(0);
}

function getMixerName(i){
	var id = _mixers[i].MixerId;
	$.getJSON(_config.api.mixers+"/"+id, function (data){
		// console.log('_mixers['+i+'].Name: ', _mixers[i].Name, " > id:", id);
		_mixers[i].Name = data.Name;
		if(i<_numMixers-1){
			getMixerName(i+1);
		}
		else{
			// console.log('>>> displayMixers');
		    displayMixers();

		    _mustload.mixers = true;
		    checkImagesLoaded('.content');
		}
	});
}

function displayMixers(){
	for (var i = 0; i < _numMixers; i++){
		var mixer = $($('#mixerTemplate').html());
		mixer.find('input').attr('id', _mixers[i].MixerId);
		mixer.find('label').attr('for', _mixers[i].MixerId);
		mixer.find('label').text(_mixers[i].Name);
		mixer.appendTo($('.mixers'));
	}
	$('.mixerCheckbox').on('change', function(){
		checkSelected(this);
	});
}


function checkSelected(current){
	_selectedMixers = [];
	//$('.btn').addClass('hidden');
	$('.mixerCheckbox').each(function(){
		if($(this).is(':checked')){
		    if (_selectedMixers.length < 3 ) {
				_selectedMixers.push($(this).attr("id"));
				//$('.btn').removeClass('hidden');
				
				var url = _config.pages.vmGarnish + '?brandId='+_brandId+'&glassId='+_selectedGlassId+"&MixerIDs="+_selectedMixers.toString();
				$('.btn').attr('href', url);
				
		    } else {
				$(current).prop("checked", false);
		        //toastr["info"]("Sometimes, less is more. Try stick to 3 mixers.");
				$.toast("Sometimes, less is more. Try stick to 3 mixers.");
			}
		}
		if (_selectedMixers.length == 0) {
		    $('.btn').attr('href', "#");
		}
	});
}

