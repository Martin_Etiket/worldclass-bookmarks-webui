var items = [
"images/competition/competition_d1_001.jpg",
"images/competition/competition_d1_002.jpg",
"images/competition/competition_d1_003.jpg",
"images/competition/competition_d1_004.jpg",
"images/competition/competition_d1_005.jpg",
"images/competition/competition_d1_006.jpg",
"images/competition/competition_d1_007.jpg",
"images/competition/competition_d1_008.jpg",
"images/competition/competition_d1_009.jpg",
"images/competition/competition_d1_010.jpg",
"images/competition/competition_d1_011.jpg",
"images/competition/competition_d1_012.jpg",
"images/competition/competition_d1_013.jpg",
"images/competition/competition_d1_014.jpg",
"images/competition/competition_d1_015.jpg",
"images/competition/competition_d1_016.jpg",
"images/competition/competition_d1_017.jpg",
"images/competition/competition_d1_018.jpg",
"images/competition/competition_d1_019.jpg",
"images/competition/competition_d1_020.jpg",
"images/competition/competition_d1_021.jpg",
"images/competition/competition_d1_022.jpg",
"images/competition/competition_d1_023.jpg",
"images/competition/competition_d1_024.jpg",
"images/competition/competition_d2_025.jpg",
"images/competition/competition_d2_026.jpg",
"images/competition/competition_d2_027.jpg",
"images/competition/competition_d2_028.jpg",
"images/competition/competition_d2_029.jpg",
"images/competition/competition_d2_030.jpg",
"images/competition/competition_d2_031.jpg",
"images/competition/competition_d2_032.jpg",
"images/competition/competition_d2_033.jpg",
"images/competition/competition_d2_034.jpg",
"images/competition/competition_d2_035.jpg",
"images/competition/competition_d2_036.jpg",
"images/competition/competition_d2_037.jpg",
"images/competition/competition_d2_038.jpg",
"images/competition/competition_d2_039.jpg",
"images/competition/competition_d2_040.jpg",
"images/competition/competition_d2_041.jpg",
"images/competition/competition_d2_042.jpg",
"images/competition/competition_d2_043.jpg",
"images/competition/competition_d2_044.jpg",
"images/competition/competition_d2_045.jpg",
"images/competition/competition_d2_046.jpg",
"images/competition/competition_d2_047.jpg",
"images/competition/competition_d2_048.jpg",
"images/competition/competition_d2_049.jpg",
"images/competition/competition_d2_050.jpg",
"images/competition/competition_d2_051.jpg",
"images/competition/competition_d2_052.jpg",
"images/competition/competition_d2_053.jpg",
"images/competition/competition_d2_054.jpg",
"images/competition/competition_d2_055.jpg",
"images/competition/competition_d2_056.jpg",
"images/competition/competition_d2_057.jpg",
"images/competition/competition_d2_058.jpg",
"images/competition/competition_d2_059.jpg",
"images/competition/competition_d2_060.jpg",
"images/competition/competition_d2_061.jpg",
"images/competition/competition_d2_062.jpg",
"images/competition/competition_d2_063.jpg",
"images/competition/competition_d2_064.jpg",
"images/competition/competition_d2_065.jpg",
"images/competition/competition_d2_066.jpg",
"images/competition/competition_d2_067.jpg",
"images/competition/competition_d2_068.jpg",
"images/competition/competition_d2_069.jpg",
"images/competition/competition_d2_070.jpg",
"images/competition/competition_d2_071.jpg",
"images/competition/competition_d2_072.jpg",
"images/competition/competition_d3_073.jpg",
"images/competition/competition_d3_074.jpg",
"images/competition/competition_d3_075.jpg",
"images/competition/competition_d3_076.jpg",
"images/competition/competition_d3_077.jpg",
"images/competition/competition_d3_078.jpg",
"images/competition/competition_d3_079.jpg",
"images/competition/competition_d3_080.jpg",
"images/competition/competition_d3_081.jpg",
"images/competition/competition_d3_082.jpg",
"images/competition/competition_d3_083.jpg",
"images/competition/competition_d3_084.jpg",
"images/competition/competition_d3_085.jpg",
"images/competition/competition_d3_086.jpg",
"images/competition/competition_d3_087.jpg",
"images/competition/competition_d3_088.jpg",
"images/competition/competition_d3_089.jpg",
"images/competition/competition_d3_090.jpg",
"images/competition/competition_d3_091.jpg",
"images/competition/competition_d3_092.jpg",
"images/competition/competition_d3_093.jpg",
"images/competition/competition_d3_094.jpg",
"images/competition/competition_d3_095.jpg",
"images/competition/competition_d3_096.jpg",
"images/competition/competition_d3_097.jpg",
"images/competition/competition_d3_098.jpg",
"images/competition/competition_d3_099.jpg",
"images/competition/competition_d3_100.jpg",
"images/competition/competition_d3_101.jpg",
"images/competition/competition_d3_102.jpg",
"images/competition/competition_d3_103.jpg",
"images/competition/competition_d3_104.jpg",
"images/competition/competition_d3_105.jpg",
"images/competition/competition_d3_106.jpg",
"images/competition/competition_d3_107.jpg"
];

var itemsMobile = [
	"images/competition/Competition_mobile_d1_001.jpg",
	"images/competition/Competition_mobile_d1_002.jpg",
	"images/competition/Competition_mobile_d1_003.jpg",
	"images/competition/Competition_mobile_d1_004.jpg",
	"images/competition/Competition_mobile_d1_005.jpg",
	"images/competition/Competition_mobile_d1_006.jpg",
	"images/competition/Competition_mobile_d1_007.jpg",
	"images/competition/Competition_mobile_d1_008.jpg",
	"images/competition/Competition_mobile_d1_009.jpg",
	"images/competition/Competition_mobile_d1_010.jpg",
	"images/competition/Competition_mobile_d1_011.jpg",
	"images/competition/Competition_mobile_d1_012.jpg",
	"images/competition/Competition_mobile_d1_013.jpg",
	"images/competition/Competition_mobile_d1_014.jpg",
	"images/competition/Competition_mobile_d1_015.jpg",
	"images/competition/Competition_mobile_d1_016.jpg",
	"images/competition/Competition_mobile_d1_017.jpg",
	"images/competition/Competition_mobile_d1_018.jpg",
	"images/competition/Competition_mobile_d1_019.jpg",
	"images/competition/Competition_mobile_d1_020.jpg",
	"images/competition/Competition_mobile_d1_021.jpg",
	"images/competition/Competition_mobile_d1_022.jpg",
	"images/competition/Competition_mobile_d1_023.jpg",
	"images/competition/Competition_mobile_d1_024.jpg",
	"images/competition/Competition_mobile_d2_025.jpg",
	"images/competition/Competition_mobile_d2_026.jpg",
	"images/competition/Competition_mobile_d2_027.jpg",
	"images/competition/Competition_mobile_d2_028.jpg",
	"images/competition/Competition_mobile_d2_029.jpg",
	"images/competition/Competition_mobile_d2_030.jpg",
	"images/competition/Competition_mobile_d2_031.jpg",
	"images/competition/Competition_mobile_d2_032.jpg",
	"images/competition/Competition_mobile_d2_033.jpg",
	"images/competition/Competition_mobile_d2_034.jpg",
	"images/competition/Competition_mobile_d2_035.jpg",
	"images/competition/Competition_mobile_d2_036.jpg",
	"images/competition/Competition_mobile_d2_037.jpg",
	"images/competition/Competition_mobile_d2_038.jpg",
	"images/competition/Competition_mobile_d2_039.jpg",
	"images/competition/Competition_mobile_d2_040.jpg",
	"images/competition/Competition_mobile_d2_041.jpg",
	"images/competition/Competition_mobile_d2_042.jpg",
	"images/competition/Competition_mobile_d2_043.jpg",
	"images/competition/Competition_mobile_d2_044.jpg",
	"images/competition/Competition_mobile_d2_045.jpg",
	"images/competition/Competition_mobile_d2_046.jpg",
	"images/competition/Competition_mobile_d2_047.jpg",
	"images/competition/Competition_mobile_d2_048.jpg",
	"images/competition/Competition_mobile_d2_049.jpg",
	"images/competition/Competition_mobile_d2_050.jpg",
	"images/competition/Competition_mobile_d2_051.jpg",
	"images/competition/Competition_mobile_d2_052.jpg",
	"images/competition/Competition_mobile_d2_053.jpg",
	"images/competition/Competition_mobile_d2_054.jpg",
	"images/competition/Competition_mobile_d2_055.jpg",
	"images/competition/Competition_mobile_d2_056.jpg",
	"images/competition/Competition_mobile_d2_057.jpg",
	"images/competition/Competition_mobile_d2_058.jpg",
	"images/competition/Competition_mobile_d2_059.jpg",
	"images/competition/Competition_mobile_d2_060.jpg",
	"images/competition/Competition_mobile_d2_061.jpg",
	"images/competition/Competition_mobile_d2_062.jpg",
	"images/competition/Competition_mobile_d2_063.jpg",
	"images/competition/Competition_mobile_d2_064.jpg",
	"images/competition/Competition_mobile_d2_065.jpg",
	"images/competition/Competition_mobile_d2_066.jpg",
	"images/competition/Competition_mobile_d2_067.jpg",
	"images/competition/Competition_mobile_d2_068.jpg",
	"images/competition/Competition_mobile_d2_069.jpg",
	"images/competition/Competition_mobile_d2_070.jpg",
	"images/competition/Competition_mobile_d2_071.jpg",
	"images/competition/Competition_mobile_d2_072.jpg",
	"images/competition/Competition_mobile_d3_073.jpg",
	"images/competition/Competition_mobile_d3_074.jpg",
	"images/competition/Competition_mobile_d3_075.jpg",
	"images/competition/Competition_mobile_d3_076.jpg",
	"images/competition/Competition_mobile_d3_077.jpg",
	"images/competition/Competition_mobile_d3_078.jpg",
	"images/competition/Competition_mobile_d3_079.jpg",
	"images/competition/Competition_mobile_d3_080.jpg",
	"images/competition/Competition_mobile_d3_081.jpg",
	"images/competition/Competition_mobile_d3_082.jpg",
	"images/competition/Competition_mobile_d3_083.jpg",
	"images/competition/Competition_mobile_d3_084.jpg",
	"images/competition/Competition_mobile_d3_085.jpg",
	"images/competition/Competition_mobile_d3_086.jpg",
	"images/competition/Competition_mobile_d3_087.jpg",
	"images/competition/Competition_mobile_d3_088.jpg",
	"images/competition/Competition_mobile_d3_089.jpg",
	"images/competition/Competition_mobile_d3_090.jpg",
	"images/competition/Competition_mobile_d3_091.jpg",
	"images/competition/Competition_mobile_d3_092.jpg",
	"images/competition/Competition_mobile_d3_093.jpg",
	"images/competition/Competition_mobile_d3_094.jpg",
	"images/competition/Competition_mobile_d3_095.jpg",
	"images/competition/Competition_mobile_d3_096.jpg",
	"images/competition/Competition_mobile_d3_097.jpg",
	"images/competition/Competition_mobile_d3_098.jpg",
	"images/competition/Competition_mobile_d3_099.jpg",
	"images/competition/Competition_mobile_d3_100.jpg",
	"images/competition/Competition_mobile_d3_101.jpg",
	"images/competition/Competition_mobile_d3_102.jpg",
	"images/competition/Competition_mobile_d3_103.jpg",
	"images/competition/Competition_mobile_d3_104.jpg",
	"images/competition/Competition_mobile_d3_105.jpg",
	"images/competition/Competition_mobile_d3_106.jpg",
	"images/competition/Competition_mobile_d3_107.jpg"
];

var numItems = items.length;
var _w, _preloader, _currentImg;
$(document).ready(function(){
	onresize();
	$(window).resize(onresize);
	
	if(_w<1024){
		items = itemsMobile;
	}

	//var mobileH = 827/768;
	//for (var i=0; i<numItems; i++){
	//	if(_w>=1024){
	//		var item = $('<img width="1024" height="600"/>');
	//	}else{
	//		var h = _w * mobileH;
	//		var item = $('<img width="'+_w+'" height="'+h+'"/>');
	//		$('.head').css({'height':h+"px"});
	//	}
		
	//	item.attr('src', items[i]);
	//	item.appendTo($('#carousel'));
	//}
	
	//$('#carousel').carouFredSel({
	//	prev: '#carouselItem-slider-prev',
	//	next: '#carouselItem-slider-next'
	//});
	//$('.caroufredsel_wrapper').css('z-index', -1);
	
	//for (var x=0; x<108; x++){
	//	var num = "";
	//	if(x<10){
	//		num = "00" + x;
	//	}
	//	else if(x>=10 && x<100){
	//		num = "0" + x;
	//	}
	//	else
	//	{
	//		num = "" + x;
	//	}
    //}

	_preloader = new WorldClassPreloader({parent:$('#carousel')});
	
	showCarouselItem(0);
	$('#carouselItem-slider-next').on('click', nextItem);
	$('#carouselItem-slider-prev').on('click', prevItem);
});

function nextItem() {
    TweenMax.killDelayedCallsTo(nextItem);
    if (_currentImg < numItems - 1) {
        showCarouselItem(_currentImg + 1);
    } else {
        showCarouselItem(0);
    }
}

function prevItem() {
    TweenMax.killDelayedCallsTo(nextItem);
    if (_currentImg > 0) {
        showCarouselItem(_currentImg - 1);
    } else {
        showCarouselItem(numItems-1);
    }
}

function showCarouselItem(i) {
    TweenMax.to(_preloader, 0.2, { autoAlpha: 1, ease: Quad.easeInOut });

    loadImg(items[i], function (success, index) {
        if (success) {
            _currentImg = index;
            var image = this;
            TweenMax.to(_preloader, 0.2, { autoAlpha: 0, ease: Quad.easeInOut });
            TweenMax.to('#carousel', 0.3, {
                autoAlpha: 0,
                x: -900,
                onComplete: function () {
                    log('complete');
                    $('#carousel').find('img').attr('src', $(image).attr('src'));
                    if (_w >= 1024) {
                        $('#carousel img').attr('width', '1024');
                        $('#carousel img').attr('height', '600');
                    } else {
                        var mobileH = 827 / 768;
                        var h = _w * mobileH;
                        $('.head').css({ 'height': h + "px" });
                        $('#carousel img').attr('width', _w);
                        $('#carousel img').attr('height', h);
                    }
                    TweenMax.set('#carousel', { x: 900 });
                    TweenMax.to('#carousel', 0.5, { autoAlpha: 1, x: 0, ease: Quad.easeOut });
                }
            });
            TweenMax.delayedCall(5, nextItem);
        }
    }, i)
}


function onresize(){
	_w = window.innerWidth;
	var imgW = $('.winner-img').width();
	console.log(imgW);
	$('.winner-img').css({'height':imgW+'px'});
}
