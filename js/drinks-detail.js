// var items=[
	// {
		// image:"images/Cocktails_AltosMargarita.jpg",
		// imageMobile:"images/Cocktails_AltosMargarita_Small.jpg"
	// },
	// {
		// image:"images/Cocktails_AppleSoda.jpg",
		// imageMobile:"images/Cocktails_AppleSoda_Small.jpg"
	// },
	// {
		// image:"images/Cocktails_BloodyMary.jpg",
		// imageMobile:"images/Cocktails_BloodyMary_Small.jpg"
// 		
	// },
	// {
		// image:"images/Cocktails_BlueStone.jpg",
		// imageMobile:"images/Cocktails_BlueStone_Small.jpg"
	// },
	// {
		// image:"images/Cocktails_CocoOldFashioned.jpg",
		// imageMobile:"images/Cocktails_CocoOldFashioned_Small.jpg"
	// },
	// {
		// image:"images/Cocktails_DutchMule.jpg",
		// imageMobile:"images/Cocktails_DutchMule_Small.jpg"
	// }
// 	
// ]; 
var items;
var _preloader;
_mustload = {
    'drink_image': false
}
var mousex, mousey;
$(document).ready(function () {

    _preloader = new WorldClassPreloader();

	$.getJSON('js/config.json', function(config){
	    _config = config;

	    selectedId = getParameterByName('selected');
		getDrinks(showFirstItem);
		
	});
	$('.carousel-holder').on('click', function () {
	    doCarouselClick(mousex);
	});
	$('.carousel-holder').on('mousemove', function (event) {
	    mousex = event.pageX - $(this).parent().offset().left;
	    mousey = event.pageY - $(this).parent().offset().top;
	    //log('mouse pos: ' + mousex + ', ' + mousey);
	    if (mousex < 150) {
	        $('.btn-left').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    } else if (mousex > $('.carousel-holder').width() - 150) {
	        $('.btn-right').addClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "pointer"
	        });
	    }else {
	        $('.btn-left').removeClass('hover');
	        $('.btn-right').removeClass('hover');
	        $('.carousel-holder').css({
	            "cursor": "auto"
	        });
	    }
	})
});

function doCarouselClick(x) {
    if (x) {
        if (x < 150) {
            prevItem();
        }
        if (x > $('.carousel-holder').width() - 150) {
            nextItem();
        }
    }
}

function getDrinks(callback){
	$.getJSON(_config.api.cocktails, function (cocktails){
		items = cocktails;
		numItems = items.length;
		callback();
	});
}

function showItem(pos) {
    _mustload.drink_image = false;
    TweenMax.to($('.content'), 0.2, { autoAlpha: 0, ease: Quad.easeInOut });
    TweenMax.to('#preloader', 0.3, { autoAlpha: 1, ease: Quad.easeInOut });

	var head = $('.carousel-holder .head');
	var item = $('#carousel-item-holder');
	var t = 0.3;
	var pos = pos;
	// hide current slide
	TweenMax.to(item, t, {opacity:0, ease: Quad.easeInOut});
	TweenMax.to(head, t, {opacity: 0, x: -800, ease: Quad.easeInOut, onComplete: function(){
		if(_w>=1024)
		{
		    $('#divider').removeClass('divider-black').addClass('divider-white');
		    var img = _config.vmImageFolder + items[pos].Image_DesktopName;
		    loadImg(img, function (success) {
		        if (success) {
		            $('.carousel-holder .head').css({ 'background': 'url(' + $(this).attr('src') + ')' });
		            _mustload.drink_image = true;
		            checkImagesLoaded('.content');
		        }
		    });
		}else{
		    $('#divider').removeClass('divider-white').addClass('divider-black');
			var img = _config.vmImageFolder + items[pos].Image_MobileName;
			loadImg(img, function (success) {
			    if (success) {
			        $('.carousel-holder .head').css({ 'background': 'url(' + img + ') no-repeat', 'width': '100%', 'height': (_w + (_w * 0.07)) + 'px', 'background-size': 'contain' });
			        //$('.carousel-holder .head').css({ 'background': 'url(' + $(this).attr('src') + ')' });
			        _mustload.drink_image = true;
			        checkImagesLoaded('.content');
			    }
			});
		}
		
		
		//var template = $($('#itemTemplate').html());
		var template = $('.carousel-item');
		
		// id?
		// name
		template.find('.name').html(items[pos].Name);
		template.find('.instructions p').html(items[pos].Description);
		
		// $('#carousel-item-holder').html($('#carouselItem'+pos).html()); //template.html()
		//$('#carousel-item-holder').html(template.html()); //template.html()
		currentItem = pos;
		TweenMax.set(head, {
		    x: 800
		})
		TweenMax.to(head, t, {opacity:1, x: 0, ease: Quad.easeInOut});
		TweenMax.to(item, t, { opacity: 1, ease: Quad.easeInOut });

		TweenMax.from([$('.name'), $('.divider-white')], 0.4, { y: 50, ease: Back.easeOut, opacity: 0, delay: 0.5 });
		var st = new SplitText($('.instructions p'), { type: 'lines' });
		TweenMax.staggerFrom(st.lines, 0.5, {
		    y: 50,
		    opacity: 0,
		    //onComplete: function () {
		    //    st.revert();
		    //},
		    ease: Back.easeOut,
		    delay: 0.5
		}, 0.05);
	}});
}

