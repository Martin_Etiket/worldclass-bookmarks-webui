var currentItem;
var selectedId;
var numItems;// = items.length;
var _w;	

$(document).ready(function(){
//    selectedId = getParameterByName('selected');
	// if(selectedId >=0 && selectedId != undefined && selectedId != ""){
		// showItem(selectedId);
	// }
	// else{
		// showItem(0);
	// }
	
	$('#carouselItem-slider-next').on('click',nextItem);
	$('#carouselItem-slider-prev').on('click',prevItem);
	onresize();
	$(window).resize(onresize);
});



function onresize(){
	_w = window.innerWidth;
	// _w = $(window).width();
}

function showFirstItem() {
	if(selectedId >=0 && selectedId != undefined && selectedId != ""){
	    showItem(selectedId); 
	}
	else{
	    showItem(0);
	}
}


/**
 * Display next bartender in the array, if last one currently displayed, go to 1st one 
 */
function nextItem(){
	if(currentItem < numItems-1){
		showItem(parseInt(currentItem)+1);
	}
	else{
		showItem(0);
	}
}

/**
 * Display previous bartender in the array, if currently displaying the first one, go to last one in array 
 */
function prevItem(){
	if(currentItem > 0){
		showItem(parseInt(currentItem)-1);
	}
	else{
		showItem(numItems-1);
	}
}
