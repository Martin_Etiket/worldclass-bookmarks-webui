var _mustload = {
    'background': false,
    'brand_loaded': false,
    'glass_loaded': false,
    'garnish_loaded': false,
    'mask_loaded': false
}

$(document).ready(function () {

    new WorldClassPreloader();
    var glowLightHolder = $('.carousel-holder .glowing-lights-holder');
    new GlowingLight({ parent: glowLightHolder, left: 160, top: 40, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 220, top: 75, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 140, top: 140, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 500, top: 280, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 320, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 640, top: 120, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 420, top: 110, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 750, top: 220, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 344, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 550, top: 184, colour: '#ffffff' });
    new GlowingLight({ parent: glowLightHolder, left: 700, top: 250, colour: '#ffffff' });

    var progress = new ProgressBlock(5);
    $(document).on('showing', function () {
        progress.display();

        var tl = new TimelineMax();
        tl.staggerFrom($('.nameHolder').children(), 0.8, { y: -50, ease: Back.easeOut, opacity: 0, delay: 1.4 }, 0.04);
    });

	$.getJSON('js/config.json', function(config){
		_config = config;
		
		_brandId = parseInt(getParameterByName("brandId"));
		_selectedGlassId = parseInt(getParameterByName("glassId"));
		getBrands();
		_selectedGlassId = getParameterByName("glassId");
		// _selectedMixers = getParameterByName('MixerIDs').split(",");
		mixers = getParameterByName('MixerIDs').split(",");
		/*if(mixers.length==1){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[0])];
		}
		if(mixers.length==2){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[0]), parseInt(mixers[1])];
		}
		if(mixers.length==3){
			_selectedMixers = [parseInt(mixers[0]), parseInt(mixers[1]), parseInt(mixers[2])];
		}*/
		_selectedMixers = mixers;
		displayMask();
		_garnishId = getParameterByName("garnishId");
		getGlasses(displayGlass);
		getAllGarnishes(displayGarnish);
		getAllMixers(populateMixers);
		doBubbles();
		
		
		/*for(var i=0; i<_selectedMixers.length; i++){
			_amounts[i] = 10;
			$('#ingredient'+i).removeClass('hidden');
		}*/
		$('.addButton').on('click', incrementAmount);
		$('.subtractButton').on('click', decreaseAmount);
		
		$('.btn').on('click', checkName);
	});
	
		//toastr.options = {
		//  "closeButton": false,
		//  "debug": false,
		//  "newestOnTop": false,
		//  "progressBar": false,
		//  "positionClass": "toast-coverall",
		//  "preventDuplicates": false,
		//  "onclick": null,
		//  "showDuration": "300",
		//  "hideDuration": "3000",
		//  "timeOut": "3000",
		//  "extendedTimeOut": "1000",
		//  "showEasing": "swing",
		//  "hideEasing": "linear",
		//  "showMethod": "fadeIn",
		//  "hideMethod": "fadeOut"
		//};


		$('.carousel-holder').waitForImages(function () {
		    _mustload.background = true;
		    checkImagesLoaded('.content');
		});
	
});

// function populateMixers(){
// 	
	// for(var i=0; i<_selectedMixers.length; i++){
		// // _amounts[i] = 10;
		// var mixer = getMixer(parseInt(_selectedMixers[i]));
		// $('#ingredient'+i+' .quantity').html('<b>'+_amounts[i]+'ml</b> ' + mixer.Name);
	// }
	// // $('#ingredient4 .quantity').html(getGarnish(_glassGarnish.GarnishId).Name + ' to finish it off');
// }

function updateAmounts() {
    console.log('update!');
    for (var i = 0; i < _ingredients.length; i++)
    {
        $('#ingredient' + i + ' .quantity').html('<b>' + _amounts[i] + 'ml</b> ' + _ingredients[i]);
    }
}

function incrementAmount(){
	// console.log($(this).parent().attr('id'));
	var i;
	if($(this).parent().attr('id') == 'ingredient0'){
		i = 0;
	}
	if($(this).parent().attr('id') == 'ingredient1'){
		i = 1;
	}
	if($(this).parent().attr('id') == 'ingredient2'){
		i = 2;
	}
	if($(this).parent().attr('id') == 'ingredient3'){
		i = 3;
	}

	if(_amounts[i]<50){		
		_amounts[i] += 5;
	}else{
	    //toastr["info"]("That's a heavy hand you've got there.");
	    $.toast("That's a heavy hand you've got there.");
	}
	updateAmounts();
	//populateMixers();
}

function decreaseAmount(){
	// console.log($(this).parent().attr('id'));
	var i;
	if($(this).parent().attr('id') == 'ingredient0'){
		i = 0;
	}
	if($(this).parent().attr('id') == 'ingredient1'){
		i = 1;
	}
	if($(this).parent().attr('id') == 'ingredient2'){
		i = 2;
	}
	if($(this).parent().attr('id') == 'ingredient3'){
		i = 3;
	}
	
	if(_amounts[i]>5){		
		_amounts[i] -= 5;
	}else{
	    //toastr["info"]("Give your taste buds more than that.");
	    $.toast("Give your taste buds more than that.");
	}
	updateAmounts();
	//populateMixers();
}

function checkName(event){
    if ($('#cocktailName').val() != "") {
        
        var url = _config.pages.vmShare + '?brandId=' + _brandId + '&glassId=' + _selectedGlassId + "&MixerIDs=" + _selectedMixers.toString() + "&garnishId=" + _garnishId + '&name=' + $('#cocktailName').val() + '&amounts=' + _amounts.toString();
		$('.btn').attr('href', url);
		
	}else{
		event.preventDefault();
        //toastr["info"]("Nameless isn't fashionable. Give your cocktail a signature calling");
		$.toast("Nameless isn't fashionable.<br/>Give your cocktail a signature calling");
		
	}
}
