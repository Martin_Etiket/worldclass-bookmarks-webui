var spirits = [
	{
		id:"2",
		name: "Tanqueray No. TEN",
		description:"Gin is elevated to a whole other level with Tanqueray No. TEN. The perfect 10 of gins, distilled with fresh citrus to bring a brighter taste, this pristine gin is amongst the finest-tasting spirits in the world.",
		imgMobile: "spirits-tanqueray.png"
	},
	{
		id:"8",
		name: "Don Julio",
		description:"Using only the highest-calibre, fully matured and ripened blue agave, Don Julio is a tequila that caters for the refined palate. The entire handcrafted process, finalised by artful blending, creates a consistent and distinctively rich taste. Don Julio propels tequila into the highest standard of taste, <br>maturity and blend. ",
		imgMobile: "spirits-donjulio.png"
	},
	{
		id:"1",
		name: "Cîroc Vodka",
		description:"The finest of vodkas ask for the finest of processes. Gluten free, Cîroc Vodka, unlike most made from grain, is crafted from specially selected French grapes. Distilled a fifth time at the Disteillerie de Chevanceaux in Southern France, Cîroc Vodka delivers exceptional taste and quality and a feeling of uncompromised luxury.  ",
		imgMobile: "spirits-ciroc.png"
	},
	{
		id:"5",
		name: "JOHNNIE WALKER GOLD LABEL RESERVE",
		description:"\"Pure indulgence in a bottle\" can be used to describe the House of Walker’s offering of JOHNNIE WALKER GOLD LABEL RESERVE. With almost two centuries of blending excellence, Johnnie Walker’s Master Blender has created a work of art, with his handpicked, timeously matured casks that allow a whisky such as this to exude nothing less than the <br>standard of gold. ",
		imgMobile: "spirits-jwgold.png"
	},
	{
		id:"7",
		name: "Ketel One Vodka",
		description:"Ten generations of Nolet family tradition present Ketel One Vodka. With the combination of carefully selected European wheat and years of mastering the art of this special distillation process, this family secret has crafted a vodka that carries the name of luxury and excellence in every sip.",
		imgMobile: "spirits-ketelone.png"
	},
	{
		id:"3",
		name: "THE SINGLETON 12YO",
		description:"With its deep golden colour, hints of espresso and toasted nuts, rich fruit and brown sugar, THE SINGLETON 12YO is a smooth naturally rich Speyside Single Malt Scotch Whisky that leaves you with a lingering, pleasurable warmth of a great tasting, easy to drink single malt. ",
		imgMobile: "spirits-singleton12yo.png"
	},
	{
		id:"4",
		name: "THE SINGLETON TAILFIRE",
		description:"THE SINGLETON TAILFIRE is everything you love about THE SINGLETON. Its deep gold, polished mahogany highlights, notes of fresh cut grass, ripe red berry fruit, stewed plums and raspberry jam give way to toasted oak for that extra flavour and perfect balance.",
		imgMobile: "spirits-singleton-tailfire.png"
	},
	{
		id:"6",
		name: "Ron Zacapa ",
		description:"In the highlands of Quetzaltenango, 2 300m above sea level, lies The House Above the Clouds, the birthplace of <br>Ron Zacapa 23. Wonderfully intricate with honeyed butterscotch, spiced oak and raisened fruit, the complexity of the Sistema Solera ageing process is showcased, setting this rum as a taste above the clouds. ",
		imgMobile: "spirits-ronzacapa.png"
	}
];

var maskActive = false;
var infoOpen = false;
var brands;
var _currentBottle;

var _mustload = {
    'mask':false,
    'spirit_container': false,
    'item0': false,
    'item1': false,
    'item2': false,
    'item3': false,
    'item4': false,
    'item5': false,
    'item6': false,
    'item7': false
};
    
var _preloader;
$(document).ready(function () {
    _preloader = new WorldClassPreloader();

    var progress = new ProgressBlock(1);
    $(document).on('showing', function () {
        progress.display();
    });

    new GlowingLight({ parent: $('.spirit-container'), left: 160, top: 40, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 220, top: 75, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 140, top: 140, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 400, top: 280, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 640, top: 120, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 420, top: 110, colour: '#ffffff' });
    new GlowingLight({ parent: $('.spirit-container'), left: 750, top: 220, colour: '#ffffff' });

    loadImg('images/brands-mask.png', function (s) {
        if (s) {
            $('.mask').css({ 'background': 'url(' + $(this).attr("src") + ') no-repeat' });
            doMouseMove();
            _mustload.mask = true;
            checkImagesLoaded('.content');
        }
    })
   $('.spirit-container').waitForImages(function () {
       _mustload.spirit_container = true;
       checkImagesLoaded('.content');
   });
	
	$.getJSON('js/config.json', function(config){
		_config = config;
		
		getBrands();
		
		
		if(getParameterByName('isvm') == "true"){
		$('.btn-info').addClass('hidden');
		$('.btn-add').removeClass('hidden');
		$('.btn-discover').addClass('hidden');
		$('.btn-next').removeClass('hidden');
		$('h1').addClass('hidden');
		$('.progress').removeClass('hidden');
		$('.spirits .spirit-container').addClass('virtualMixology');
		$('.carousel-holder').addClass('virtualMixology');
		
	}else{
		$('.btn-info').removeClass('hidden');
		$('.btn-add').addClass('hidden');
		$('.btn-discover').removeClass('hidden');
		$('.btn-next').addClass('hidden');
		$('h1').removeClass('hidden');
		$('.progress').addClass('hidden');
		$('.spirits .spirit-container').removeClass('virtualMixology');
		$('.carousel-holder').removeClass('virtualMixology');
	}
	
	
	$(document).mouseup(function () {
	    console.log('CLICK');
	    if (maskActive) {
	        if (getParameterByName('isvm') == "true") {
	            log('isVM: ', _currentBottle);
	            addBottle(_currentBottle);
	        } else {
	            console.log('id', _currentBottle);
	            showInfo(_currentBottle);
	        }
	    }
	});

	$('.btn-info').on('click', function(event){
		showInfo($(this).attr('id'));
	});
	$('.btn-add').on('click', function(event){
		addBottle($(this).attr('id'));
	});
	
	$('.btn-close').on('click', function(){
		$('.info').removeClass('open');
		$('.info-holder').removeClass('open');
		infoOpen = false;
		TweenLite.to(window, 1, { scrollTo: { y: 60 }, ease: Power2.easeOut });
	});
	
	for (var i=0; i<spirits.length; i++){
		
		// var item = $('<img class="carouselImage" src="" />');
		var item = $('<div class="carouselImageHolder"></div>');
		
		// item.attr('src', "images/"+spirits[i].imgMobile); 
		item.css({ 'background': 'url("images/' + spirits[i].imgMobile + '") no-repeat' });
		item.attr('id', 'item' + i);
		$(item).waitForImages(function () {
		    _mustload['item' + i] = true;
		    checkImagesLoaded('.content');
		});
		item.appendTo('#carousel');
	}
	
	var carousel = $('#carousel');
	
	var w = $(window).width();
	$('.carouselImageHolder').css({'width':w+"px", "height":w+"px", 'background-size':"contain"});
	$('.carousel-holder .head').css({'width':w+"px", "height":w+"px", "background-size":w+"px "+w+"px", "background-position-y":"40px"});
	
	var currentPage = 0;
	carousel.carouFredSel({
		auto:{
			play:false
		},
		items: 1,
		prev: '#carouselItem-slider-prev',
		next: '#carouselItem-slider-next',
		scroll:{
			onBefore: updateText
		}
	});
	$('.caroufredsel_wrapper').css('margin','-60px 0 0 0');
	
	updateText();
	function updateText(){
		carousel.trigger('currentPage', function(x){
			currentPage = x;
			$('.description').html(spirits[currentPage].description);
			$('.btn-next').attr('href', _config.pages.vmGlass + '?brandId='+spirits[currentPage].id);
		});
	}
	
	
	});
	
});

function doMouseMove() {
    document.onmousemove = function (event) {
        log("mousemove");
        var containerX = $('.spirit-container').offset().left;
        var containerY = $('.spirit-container').offset().top;
        var mouseX = event.clientX - containerX;
        var mouseY = event.clientY - containerY;
        maskActive = $('.spirit-container').hitTest(event.clientX, event.clientY);
        if (maskActive) {
            if (mouseX > 0 && mouseX < 157) {
                highlightBottle(0);
            }
            if (mouseX >= 157 && mouseX < 311) {
                highlightBottle(1);
            }
            if (mouseX >= 311 && mouseX < 420) {
                highlightBottle(2);
            }
            if (mouseX >= 419 && mouseX < 526) {
                highlightBottle(3);
            }
            if (mouseX >= 526 && mouseX < 625) {
                highlightBottle(4);
            }
            if (mouseX >= 625 && mouseX < 740) {
                highlightBottle(5);
            }
            if (mouseX >= 740 && mouseX < 860) {
                highlightBottle(6);
            }
            if (mouseX >= 860 && mouseX < 1024) {
                highlightBottle(7);
            }
        } else {
            unhighlightBottle();
        }
    };
    $(document).mousemove();
    $('.spirit-container').mouseleave();
    $(document).mousemove();
    $('.spirit-container').mouseenter();
}



$(window).resize(onresize);

function onresize(){
	
}

var maskXarr = ["-880px 0",
				"-750px 0",
				"-620px 0",
				"-500px 0",
				"-390px 0",
				"-285px 0",
				"-170px 0",
				"-50px 0"
];
var infoCoordinates = [
						{
							top: "110px",
							left: "88px"
						},
						{
							top: "167px",
							left: "215px"
						},
						{
							top: "69px",
							left: "349px"
						},
						{
							top: "69px",
							left: "453px"
						},
						{
							top: "69px",
							left: "561px"
						},
						{
							top: "98px",
							left: "671px"
						},
						{
							top: "98px",
							left: "789px"
						},
						{
							top: "98px",
							left: "904px"
						}
					];	
var currentHL;
function highlightBottle(i){
    if (!infoOpen) {
        _currentBottle = spirits[i].id;
        $('.mask').css({ "display": "block", "background-position": maskXarr[i] });
        if (i != currentHL) {
            TweenMax.killTweensOf($('.mask'));
            TweenMax.to($('.mask'), 0.3, { autoAlpha: 1, ease: Quad.easeInOut });
            currentHL = i;
        }
		$('.btn-info').css({"top":infoCoordinates[i].top, "left":infoCoordinates[i].left, "opacity":1});
		$('.btn-add').css({"top":infoCoordinates[i].top, "left":infoCoordinates[i].left, "opacity":1});
		$('.btn-info').attr("id", spirits[i].id);
		$('.btn-add').attr("id", spirits[i].id);
	}
}

function unhighlightBottle(i){
    if (!infoOpen) {
        currentHL = null;
	    //$('.mask').css({"display":"none"});
        TweenMax.killTweensOf($('.mask'));
        TweenMax.to($('.mask'), 0.3, { autoAlpha: 0, ease: Quad.easeInOut });
	    $('.btn-info').css({ "opacity": 0 });
	    $('.btn-add').css({ "opacity": 0 });
	}
}

function showInfo(id){
	for( var i=0; i<spirits.length; i++){
		if(spirits[i].id == id){
			$('.info').find($('.name')).text(spirits[i].name);
			$('.info').find($('.description')).html(spirits[i].description);
			$('.info-holder').addClass('open');
			TweenLite.delayedCall(0.7, function () {
			    $('.info').addClass('open');
            });
			infoOpen = true;
			break;	
		}
	}
	TweenLite.to(window, 1.8, { scrollTo: { y: 600 }, ease: Power2.easeOut });
}

function addBottle(id){
	window.location = _config.pages.vmGlass + '?brandId='+id;
}


function getBrands(callback){
	var url = _config.api.brands;
 	// $.getJSON(url, function(data){
    	// // console.log(JSON.stringify(data));
  	// });
}

/*
 * jQuery "hitTest" plugin
 * @warning: does not work with elements that are "display:hidden"
 * @param {Number} x The x coordinate to test for collision
 * @param {Number} y The y coordinate to test for collision
 * @return {Boolean} True if the given jQuery object's rectangular bounds contain the point defined by params x,y
 */
(function($){
    $.fn.hitTest = function(x, y){
        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();
        return x >= bounds.left && x <= bounds.right && y <= bounds.bottom && y >= bounds.top;
    };
})(jQuery);