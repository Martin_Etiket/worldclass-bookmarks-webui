﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bartenders-detail.aspx.cs" Inherits="WebUI.bartenders_detail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Bartenders</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
</head>
<body>
    <form id="form1" runat="server">
          <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        
        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders" class="active"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li> 
        		</ul>
        	</nav>
        </header>
        
        <div class="wrapper">
	        <section class="content bartenders-detail">
	        	<div class="carousel-holder">
	        		<div class="head">
			        	<h1>Bartenders</h1>
			        	<div class="btn-left" id="carouselItem-slider-prev"></div>
			        	<div class="btn-right" id="carouselItem-slider-next"></div>
	        		</div>
	        		<div id="carouselItem-carousel">
	        			<div id="carousel-item-holder"></div>
		        		<!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem0">
				        	<div class="name">Domenico De Lorenzo</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
					        	<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>I'm a Johannesburg lad.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>At first it was something I did as a student to earn extra cash, but then it quickly became a lifestyle and a passion.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>My thinking centres around using ingredients and their flavours to engage all the senses in order to create a moment and a memory in the customer's life.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>To grow my skills as a bartender and to be ranked among the best in the world in mixology.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>I was a finalist in both Bacardi Legacy and Beefeater, and was the top performer in World Class regional workshops 2 and 3.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>David Woodrich, Nelson Mandela, Francois Pienaar, Jerry Tomas and David Rios.</p>
					        	</div>
					        </div>
				        	<!-- <div class="follow-social">
				        		<div class="social-links">
					        		<div class="instruction">Follow him:</div>
					        		<a href="#" id="follow-facebook"><div class="icon-facebook icon"></div></a>
					        		<a href="#" id="follow-twitter"><div class="icon-twitter icon"></div></a>
					        		<a href="#" id="follow-instagram"><div class="icon-instagram icon"></div></a>
					        	</div>
				        	</div> -->
				        </div> <!-- end carousel Item -->
				        
				        <!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem3">
				        	<div class="name">Assaf YECHIEL</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
					        	<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>Born in Israel, raised in Cape Town.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>Well, my older brother became a bartender around the time I was finishing school. I have always looked up to him and thought it would be a fun thing to do. Turns out I’m not too shabby.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>Wow. My mom. Curiosity. Out of the box. Let me elaborate.  My mom (being the black sheep that she is) has always been intrigued by anything different, weird and wonderful. She created some very curious children with the help of one of her much loved sayings, "think outside the box". So long as it's unusual, it's a go. Life's too short <br>for mediocre.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>The same as I have put in. Everything.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>Tahona Society – National Champion 2012, Bombay Sapphire South Africa’s Most Imaginative Bartender – National Champion 2013, Bombay Sapphire World’s Most Imaginative Bartender – 2nd place globally 2013, Bacardi Legacy – National Champion 2014, World Class – National Finalist 2014, World Class – National Finalist 2015.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>Alan Watts, Jimi Hendrix, Nujabes, Grant Achatz and Ryan Stiles.</p>
					        	</div>
					        </div>
				        	<!-- <div class="follow-social">
				        		<div class="social-links">
					        		<div class="instruction">Follow him:</div>
					        		<a href="#" id="follow-facebook"><div class="icon-facebook icon"></div></a>
					        		<a href="#" id="follow-twitter"><div class="icon-twitter icon"></div></a>
					        		<a href="#" id="follow-instagram"><div class="icon-instagram icon"></div></a>
					        	</div>
				        	</div> -->
				        </div> <!-- end carousel Item -->
				        
				        <!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem1">
				        	<div class="name">AJ SNETLER</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
				        		<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>I was born in The Langkloof, raised in Joubertina and later moved to Alexandria and then Port Elizabeth.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>I became a bartender out of my love for cooking and people. Bartending has helped me to grow as an individual.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>I love the interplay between food and cocktails. If I can give the customer a satisfying taste experience, then I am satisfied with myself.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>World Class has been an amazing journey thus far, personal growth has been outstanding, but I’m aiming at growing our venue as well as the industry in Port Elizabeth and the skills of future bartenders.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>World Class is my first competition so far.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>Marilyn Manson, Abraham Lincoln, Ghandi, Kurt Cobain and Johnny Cash.</p>
					        	</div>
					        </div>
				        </div> <!-- end carousel Item -->
				        
				        <!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem2">
				        	<div class="name">STEVEN SAUNDERS</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
					        	<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>Durban.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>Bartending is my happy place. I feel very much at home slinging drinks behind a bar and interacting with customers.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>I’m always striving to incorporate my knowledge of food and flavours into the drinks I mix. Whenever possible, I use proudly South African ingredients.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>I want to gain as much knowledge as I can around mixology to grow my skills professionally. I want to play my part<br>in improving the art of bartending in<br />South Africa.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>I made it into the World Class national finals twice before, and was in the IBA SA Finals last year.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>Will Smith, Alex Ferguson, Gordon Ramsey, My Fiancé and Salvatore Calabrese.</p>
					        	</div>
					        </div>
				        	<!-- <div class="follow-social">
				        		<div class="social-links">
					        		<div class="instruction">Follow him:</div>
					        		<a href="#" id="follow-facebook"><div class="icon-facebook icon"></div></a>
					        		<a href="#" id="follow-twitter"><div class="icon-twitter icon"></div></a>
					        		<a href="#" id="follow-instagram"><div class="icon-instagram icon"></div></a>
					        	</div>
				        	</div> -->
				        </div> <!-- end carousel Item -->
				        
				        <!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem4">
				        	<div class="name">Tom Savage</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
					        	<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>I was born in Jo'burg and have spent most of my life there.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>I sort of fell into bartending and never found any reason to look back. It’s very fulfilling as a profession.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>I like to use fresh ingredients and herbs as my inspiration. I find that using unique flavours keeps customers interested in the art of mixology.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>I hope to sharpen my skills as a bartender and, above all, have a lot of fun.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>Making it to the World Class national finals is a great accomplishment and I am proud to have made it this far.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>Lance Armstrong, Bill Bailey, Neil Savage, Joseph Stalin and Mao Zedong.</p>
					        	</div>
					        </div>
				        	<!-- <div class="follow-social">
				        		<div class="social-links">
					        		<div class="instruction">Follow him:</div>
					        		<a href="#" id="follow-facebook"><div class="icon-facebook icon"></div></a>
					        		<a href="#" id="follow-twitter"><div class="icon-twitter icon"></div></a>
					        		<a href="#" id="follow-instagram"><div class="icon-instagram icon"></div></a>
					        	</div>
				        	</div> -->
				        </div> <!-- end carousel Item -->
				        
				        <!-- carousel Item -->
		        		<div class="carousel-item" id="carouselItem5">
				        	<div class="name">TRACE VAN DER MERWE</div>
				        	<div class="divider"></div>
				        	<div class="col-holder">
					        	<div class="col-left">
					        		<b>Where were you born and raised?</b>
					        		<p>I was born and raised in Durban.</p>
					        		<b>Why did you want to become a bartender?</b>
									<p>I really enjoy interacting with people from all around the world who belong to different cultures. People make it all worthwhile<br> for me.</p>
									<b>What is the thinking behind your World Class Cocktail creations? Touch on the inspiration, references and ingredients?</b>
									<p>I have gone out my comfort zone a bit here as I’m trying to fade away from my sweet tooth and work more towards texture and flavour components in my cocktails. I also like to draw inspiration from everyday experiences to use in my cocktails.</p>
					        	</div>
					        	<div class="vertical-divider"></div>
					        	<div class="col-right">
					        		<b>What are you hoping to get out of the World Class competition?</b>
					        		<p>As I’m still young and new to the world of mixology, I am always learning and hope to take away as much experience and knowledge as I can.</p>
					        		<b>What are your industry achievements <br>so far?</b>
									<p>In February I won Heineken’s Best South African Bartender.</p>
									<b>Which five people, alive or dead, would you invite to a dinner party?</b>
									<p>Nelson Mandela, Mmusi Maimane, Jacob Zuma, Trevor Noah and Gareth Cliff – I mean, come on, you put all of them in a room and see how amazing your night <br>will be.</p>
					        	</div>
					        </div>
				        	<!-- <div class="follow-social">
				        		<div class="social-links">
					        		<div class="instruction">Follow him:</div>
					        		<a href="#" id="follow-facebook"><div class="icon-facebook icon"></div></a>
					        		<a href="#" id="follow-twitter"><div class="icon-twitter icon"></div></a>
					        		<a href="#" id="follow-instagram"><div class="icon-instagram icon"></div></a>
					        	</div>
				        	</div> -->
				        </div> <!-- end carousel Item -->
					</div>
	        	</div>
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>
                
        <div id="template" class="hidden">
        	<div class="carousel-item">
	        	<div class="name">Domenico De Lorenzo</div>
	        	<div class="divider"></div>
	        	<div class="col-holder">
		        	<div class="col-left">
		        		<b class="q1">Where were you born and raised?</b>
		        		<p class="a1">I'm a Johannesburg lad.</p>
		        		<b class="q2">Where were you born and raised?</b>
		        		<p class="a2">I'm a Johannesburg lad.</p>
		        		<b class="q3">Where were you born and raised?</b>
		        		<p class="a3">I'm a Johannesburg lad.</p>
		        	</div>
		        	<div class="vertical-divider"></div>
		        	<div class="col-right">
		        		<b class="q4">Where were you born and raised?</b>
		        		<p class="a4">I'm a Johannesburg lad.</p>
		        		<b class="q5">Where were you born and raised?</b>
		        		<p class="a5">I'm a Johannesburg lad.</p>
		        		<b class="q6">Where were you born and raised?</b>
		        		<p class="a6">I'm a Johannesburg lad.</p>
		        	</div>
		        </div>
		    </div>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/carousel.js"></script>
        <script src="js/bartenders-detail.js"></script> <!-- must be before main -->
        <script src="js/main.js"></script>
        

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-66449050-1','auto');ga('send','pageview');
        </script>
    </form>
</body>
</html>
