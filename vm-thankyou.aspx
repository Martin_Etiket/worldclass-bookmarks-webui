﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vm-thankyou.aspx.cs" Inherits="WebUI.vm_thankyou" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Virtual Mixology</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
</head>
<body>
    <form id="form1" runat="server">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <header class="header">
            <div class="logo"></div>
            <div class="hamburger mobile"></div>
            <div class="social">
                <a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank">
                    <div class="social-icon icon-facebook"></div>
                </a>
                <a href="https://twitter.com/WorldClassSA" target="_blank">
                    <div class="social-icon icon-twitter"></div>
                </a>
                <a href="https://instagram.com/worldclasssa/" target="_blank">
                    <div class="social-icon icon-instagram"></div>
                </a>
            </div>
            <nav class="nav">
                <ul>
                    <li><a href="index.aspx" id="nav-item-home">
                        <div class="nav-item">Home</div>
                    </a></li>
                    <li><a href="spirits.aspx" id="nav-item-brands">
                        <div class="nav-item">Spirits</div>
                    </a></li>
                    <li><a href="bars.aspx" id="nav-item-venues">
                        <div class="nav-item">Bars</div>
                    </a></li>
                    <li><a href="bartenders.aspx" id="nav-item-bartenders">
                        <div class="nav-item">Bartenders</div>
                    </a></li>
                    <li><a href="competition.aspx" id="nav-item-competition">
                        <div class="nav-item">Competition</div>
                    </a></li>
                    <li><a href="drinks.aspx" id="nav-item-drinks">
                        <div class="nav-item">Drinks</div>
                    </a></li>
                    <!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
                    <li><a href="contact.aspx" id="nav-item-contact">
                        <div class="nav-item">Contact Us</div>
                    </a></li>
                </ul>
            </nav>
        </header>

        <div class="wrapper">
            <section class="content vm-thankyou">

                <div class="carousel-holder ">

                    <h1>THANK YOU</h1>

                    <div class="thanksHolder">
                        <div class="thanks">Your signature recipe has just been sent to you.<br />Try another or visit our World Class Cocktail page for more inspiration. </div>
                        <div class="divider-white"></div>
                        <div class="btns">
                            <a href="vm-intro.aspx" class="btn-vm">Mix Another Cocktail</a>
                            <p>or</p>
                            <a href="drinks.aspx" class="btn-discover">Discover
                                <br>
                                World Class</a>
                        </div>
                    </div>

                </div>


            </section>
        </div>

        <footer class="footer">
            <div class="footer-icons">
                <ul>
                    <li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank">
                        <div class="footer-icon icon-tanquery"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/donjuliotequila" target="_blank">
                        <div class="footer-icon icon-donjulio"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank">
                        <div class="footer-icon icon-johnniewalker"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/TheSingletonSA" target="_blank">
                        <div class="footer-icon icon-singleton"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank">
                        <div class="footer-icon icon-ronzappa"></div>
                    </a></li>
                    <li><a href="https://www.facebook.com/KetelOne " target="_blank">
                        <div class="footer-icon icon-ketel"></div>
                    </a></li>
                </ul>
            </div>
        </footer>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l; b[l] || (b[l] =
                function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-66449050-1', 'auto'); ga('send', 'pageview');
        </script>
    </form>
</body>
</html>
