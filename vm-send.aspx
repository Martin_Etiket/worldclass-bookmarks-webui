﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vm-send.aspx.cs" Inherits="WebUI.vm_send" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Virtual Mixology</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
    <style>
        .sendForm .input .input-text {
            outline:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        
        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li>
        		</ul>
        	</nav>
        </header>
        
        <div class="wrapper">
	        <section class="content virtualMixology sendPage">
	        	
	        	<div class="carousel-holder ">
	        		
	        		<div class="sendForm">
	        			<div class="sendInstruction">Send your cocktail to yourself to try at home and impress all with your World Class bartending skills. </div>
	        			<div class="divider-white"></div>
	        			<div class="input"><input runat="server" type="text" placeholder="NAME" name="firstname" class="input-text" id="firstname" onkeyup="updateStyle(this) " /><div id="nameStar" runat="server" class="error"></div></div>
	        			<div class="input"><input runat="server" type="text" placeholder="SURNAME" name="surname" class="input-text" id="surname" onkeyup="updateStyle(this) " /><div id="surnameStar" runat="server" class="error"></div></div>
	        			<div class="input"><input runat="server" type="text" placeholder="EMAIL" name="email" class="input-text" id="email" onkeyup="updateStyle(this) " /><div id="emailStar" runat="server" class="error"></div></div>
	        			<div class="input"><input runat="server" type="text" placeholder="CELL" name="cell" class="input-text" id="cell" onkeyup="updateStyle(this) " /><div id="telStar" runat="server" class="error"></div></div>
	        			<input runat="server" type="checkbox" id="send">
		        		<label for="send">Send my recipe via email</label>
		        		<input runat="server" type="checkbox" id="optin">
		        		<label for="optin">Receive future marketing material</label>
		        		<asp:Label ID="MainValidationLabel" runat="server" CssClass="LIGHT-ITALIC errorText show">* Please fill in the required fields.</asp:Label>
                        <asp:Button ID="SubmitButton" runat="server" CssClass="btn-next send" Text="Send To Me" OnClick="SubmitButton_Click" OnClientClick="return validateForm()" />
	        		</div>
		        	
	        	</div>
	        
	        	
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>
                
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenLite.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/toastr.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vm.js"></script>
        <script src="js/vm-send.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l; b[l] || (b[l] =
                function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-66449050-1', 'auto'); ga('send', 'pageview');
        </script>

        <asp:PlaceHolder ID="ScriptPlaceholder" runat="server">
            <script>

                function validateForm() {
                    
                    
                    var NameTextBox = document.getElementById('<%=firstname.ClientID%>');
                    var SurnameTextBox = document.getElementById('<%=surname.ClientID%>');
                    var EmailTextBox = document.getElementById('<%=email.ClientID%>');
                    var TelTextBox = document.getElementById('<%=cell.ClientID%>');

                    var nameStar = document.getElementById('<%=nameStar.ClientID%>');
                    var surnameStar = document.getElementById('<%=surnameStar.ClientID%>');
                    var emailStar = document.getElementById('<%=emailStar.ClientID%>');
                    var telStar = document.getElementById('<%=telStar.ClientID%>');
                    
                    var MainValidationLabel = document.getElementById('<%=MainValidationLabel.ClientID%>');
                    MainValidationLabel.textContent = "";

                    var validated = true;

                    /*Tel Textbox*/
                    if (TelTextBox.value === '') {
                        validated = false;
                        MainValidationLabel.textContent = "* Please enter a contact number";
                        if (telStar.className.indexOf(' show') === -1) {
                            telStar.className += ' show';

                        }
                    }
                    else if (TelTextBox.value.length < 8) {
                        validated = false;
                        MainValidationLabel.textContent = "* Contact number is too short";
                        if (telStar.className.indexOf(' show') === -1) {
                            telStar.className += ' show';

                        }
                    }
                    else if (TelTextBox.value !== '') {
                        var telValue = TelTextBox.value.split(' ').join('');

                        var telPatt = /^[0-9 -]{7,13}$/;

                        var telRes = telPatt.test(telValue);

                        if (telRes === false) {
                            validated = false
                            MainValidationLabel.textContent = "* Please enter a valid contact number";
                            if (telStar.className.indexOf(' show') === -1) {
                                telStar.className += ' show';

                            }
                        }
                    }

                    /*Email Textbox*/
                    if (EmailTextBox.value === '') {
                        validated = false;
                        MainValidationLabel.textContent = "* Please enter your email address";
                        if (emailStar.className.indexOf(' show') === -1) {
                            emailStar.className += ' show';

                        }
                    }

                    if (EmailTextBox.value !== '') {
                        var emailPatt = /^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,20}$/;

                        var emailRes = emailPatt.test(EmailTextBox.value);

                        if (emailRes === false) {
                            validated = false;
                            MainValidationLabel.textContent = "* Please enter a valid email address";
                            if (emailStar.className.indexOf(' show') === -1) {
                                emailStar.className += ' show';

                            }
                        }
                    }

                    /*Surname Textbox*/
                    if (SurnameTextBox.value === '') {
                        validated = false;
                        MainValidationLabel.textContent = "* Please enter your surname";
                        if (surnameStar.className.indexOf(' show') === -1) {
                            surnameStar.className += ' show';

                        }
                    }
                    else if (SurnameTextBox.value.length < 3) {
                        validated = false;
                        MainValidationLabel.textContent = "* Surname is too short";
                        if (surnameStar.className.indexOf(' show') === -1) {
                            surnameStar.className += ' show';

                        }
                    }
                    else if (SurnameTextBox.value !== '') {
                        var surnameValue = SurnameTextBox.value.split(' ').join('');

                        var surnamePatt = /^[a-zA-Z ]+$/;

                        var surnameResult = surnamePatt.test(surnameValue);

                        if (surnameResult === false) {
                            validated = false;
                            MainValidationLabel.textContent = "* Please enter a valid surname";
                            if (surnameStar.className.indexOf(' show') === -1) {
                                surnameStar.className += ' show';

                            }
                        }
                    }

                    /*Name Textbox*/
                    if (NameTextBox.value === '') {
                        validated = false;
                        MainValidationLabel.textContent = "* Please enter your name";
                        if (nameStar.className.indexOf(' show') === -1) {
                            nameStar.className += ' show';

                        }
                    }
                    else if (NameTextBox.value.length < 2) {
                        validated = false;
                        MainValidationLabel.textContent = "* Name is too short";
                        if (nameStar.className.indexOf(' show') === -1) {
                            nameStar.className += ' show';

                        }
                    }
                    else if (NameTextBox.value !== '') {
                        var nameValue = NameTextBox.value.split(' ').join('');

                        var patt = /^[a-zA-Z ]+$/;

                        var res = patt.test(nameValue);

                        if (res === false) {
                            validated = false;
                            MainValidationLabel.textContent = "* Please enter a valid name";
                            if (nameStar.className.indexOf(' show') === -1) {
                                nameStar.className += ' show';

                            }
                        }
                    }

                    if (validated === false)
                        return false;
                    else
                        return true;
                }

                function updateStyle(element) {
                    if (element.value !== '') {
                        $(element).next(".error").className = $(element).next(".error").removeClass('show');
                    }
                }

                window.onload = function () {
                    ApplyPlaceHolders();
                };

                function isIE() {
                    var myNav = navigator.userAgent.toLowerCase();
                    return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
                };

                function ApplyPlaceHolders() {
                    if (isIE() === 9) {
                        $('[placeholder]').focus(function () {
                            var input = $(this);
                            if (input.val() == input.attr('placeholder')) {
                                input.val('');
                                input.removeClass('placeholder');
                            }
                        }).blur(function () {
                            var input = $(this);
                            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                                input.addClass('placeholder');
                                input.val(input.attr('placeholder'));
                            }
                        }).blur();
                    }
                };
            </script>
        </asp:PlaceHolder>
    </form>
</body>
</html>
