﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebUI.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>World Class</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.png">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <%--fb--%>
    <meta property="og:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself" />
    <meta name="keywords" content="World Class">
    <meta property="og:url" content="http://www.worldclasssa.com" />
    <meta property="og:image" content="http://www.worldclasssa.com/images/SS_FB.jpg" />

    <%--twitter--%>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@worldclasssa">
    <meta name="twitter:title" content="World Class">
    <meta name="twitter:description" content="Now I know how the masters make Worldclass cocktails. Experience it for yourself.">
    <meta name="twitter:image:src" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:image" content="http://www.worldclasssa.com/images/SS_TW.jpg">
    <meta name="twitter:url" content="http://www.worldclasssa.com/">
</head>
<body>
    <form id="form1" runat="server">
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        
        <header class="header">
        	<div class="logo"></div>
        	<div class="hamburger mobile"></div>
        	<div class="social">
        		<a href="https://www.facebook.com/WorldClassSouthAfrica?brand_redir=383464007800" target="_blank"><div class="social-icon icon-facebook"></div></a>
        		<a href="https://twitter.com/WorldClassSA" target="_blank"><div class="social-icon icon-twitter"></div></a>
        		<a href="https://instagram.com/worldclasssa/" target="_blank"><div class="social-icon icon-instagram"></div></a>
        	</div>
        	<nav class="nav">
        		<ul>
        			<li><a href="index.aspx" id="nav-item-home" class="active"><div class="nav-item">Home</div></a></li>
        			<li><a href="spirits.aspx" id="nav-item-brands"><div class="nav-item">Spirits</div></a></li>
        			<li><a href="bars.aspx" id="nav-item-venues"><div class="nav-item">Bars</div></a></li>
        			<li><a href="bartenders.aspx" id="nav-item-bartenders"><div class="nav-item">Bartenders</div></a></li>
        			<li><a href="competition.aspx" id="nav-item-competition"><div class="nav-item">Competition</div></a></li>
        			<li><a href="drinks.aspx" id="nav-item-drinks"><div class="nav-item">Drinks</div></a></li>
        			<!-- <li><a href="#" id="nav-item-events"><div class="nav-item">Events</div></a></li> -->
        			<li><a href="contact.aspx" id="nav-item-contact"><div class="nav-item">Contact Us</div></a></li>
        		</ul>
        	</nav>
        </header>
        
        <%--<div class="preloader" id="preloader">--%>
          
        </div>
        <div class="wrapper">
	        <section class="content home">
		        	<!-- featured cocktail -->
		        	<a href="drinks-detail.aspx?selected=4">
		        		<div class="featured featured-cocktail desktop" id="featured-cocktail-desktop">
			        		<div class="featured-copy ">
				        		<h3>Featured Cocktail</h3>
				        		<div class="divider"></div>
				        		<h4>Ron Zacapa Chocolate Old Fashioned</h4>
				        	</div>
			        	</div>
		        	</a>
		        	
		        	<!-- center -->
		        	<div class="center">
		        		<div class="logo"></div>
		        		<div class="copy">
		        			<span class="LIGHT-ITALIC">adjective</span>
							<p class="THIN">describing something as the best in the world, the cream of the crop and the cr&#0232;me de la cr&#0232;me. It’s more than just having access to the best, but being so too and living the life of luxury.</p>
							<p>&nbsp;</p>
							<p class="BOOK">It's also deﬁned by a portfolio of the ﬁnest spirits that you're about to experience.</p>
		        		</div>
		        		<div class="CTA">
		        			<div class="btns">
			        			<a href="vm-intro.aspx" class="btn-big-square" id="big-button-1"><div class="icon">
                                                                                <svg version="1.1" id="home-icon1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                                                                                    viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                                <g>
	                                                                                <path fill="#E75300" d="M15.7,8.1l-5.6,5.4c0,0-0.6,1,0.8,1.1h17.9c1.5-0.1,0.8-1.1,0.8-1.1l-5.6-5.4H15.7z"/>
	                                                                                <path fill="#E75300" d="M15.9,39.9c-1,0-1.2-1-1.2-1l-4.7-21.8c-0.4-1.6,1.1-1.6,1.1-1.6h17.6c0,0,1.5-0.1,1.1,1.6l-4.7,21.8
		                                                                                c0,0-0.2,1-1.2,1H15.9z"/>
	                                                                                <path fill="#E75300" d="M24.6,5.6c0,0.8-0.7,1.5-1.5,1.5h-6.5c-0.8,0-1.5-0.7-1.5-1.5v-4c0-0.8,0.7-1.5,1.5-1.5h6.5
		                                                                                c0.8,0,1.5,0.7,1.5,1.5V5.6z"/>
                                                                                </g>
                                                                                </svg>
			        			                                               </div><p>Create Your<br>Own Cocktail</p></a>
			        			<a href="drinks.aspx" class="btn-big-square" id="big-button-2"><div class="icon">
                                                                            <svg version="1.1" id="home-icon2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                                                                                viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                                            <g>
	                                                                            <polygon fill="#E75300" points="2.7,8.2 37.3,8.2 21,21.5 21,38.6 25.2,40 15,40 19,38.6 19,21.5 	"/>
	                                                                            <polygon class="icon-over" fill="#E75300" points="29.5,7.7 30.7,7.7 35,0 34.1,0 	"/>
	                                                                            <path fill="#E75300" d="M18.5,7.3c0-1.2-0.9-2.1-2.1-2.1c-1.2,0-2.1,0.9-2.1,2.1c0,0.2,0,0.3,0.1,0.5h4.1
		                                                                            C18.4,7.7,18.5,7.5,18.5,7.3z"/>
	                                                                            <path fill="#E75300" d="M11.4,4.8c-1.2,0-2.1,0.9-2.1,2.1c0,0.3,0.1,0.7,0.2,0.9h3.8c0.1-0.3,0.2-0.6,0.2-0.9
		                                                                            C13.5,5.7,12.5,4.8,11.4,4.8z"/>
                                                                            </g>
                                                                            </svg>
			        			                                             </div><p>Discover <br>World Class</p></a>
			        		</div>
		        		</div>
		        	</div>
		        	<!-- MOBILE -->
		        	<div class="mobile">
		        		<a href="drinks-detail.aspx?selected=4">
			        		<div class="featured featured-cocktail" id="featured-cocktail-mobile">
				        		<div class="featured-copy">
					        		<h3>Featured Cocktail</h3>
					        		<div class="divider"></div>
					        		<h4>Ron Zacapa Chocolate Old Fashioned</h4>
					        	</div>
				        	</div>
			        	</a>
			        	
			        	<a href="bartenders-detail.aspx?selected=0">
			        	<div class="featured featured-bartender" id="featured-bartender-mobile">
			        		<div class="featured-copy">
				        		<h3>Featured Bartender</h3>
				        		<div class="divider"></div>
				        		<h4>Domenico De Lorenzo</h4>
				        	</div>
			        	</div>
			        </a>
		        	</div> <!-- end MOBILE -->
		        	
		        	<!-- featured bartender -->
		        	<a href="bartenders-detail.aspx?selected=0">
			        	<div class="featured featured-bartender desktop" id="featured-bartender-desktop">
			        		<div class="featured-copy">
				        		<h3>Featured Bartender</h3>
				        		<div class="divider"></div>
				        		<h4>Domenico De Lorenzo</h4>
				        	</div>
			        	</div>
			        </a>
			     
	        </section>	
        </div>
        
        <footer class="footer">
        	<div class="footer-icons">
        		<ul>
        			<li><a href="https://www.facebook.com/TanquerayGinSA" target="_blank"><div class="footer-icon icon-tanquery"></div></a></li>
        			<li><a href="https://www.facebook.com/donjuliotequila" target="_blank"><div class="footer-icon icon-donjulio"></div></a></li>
        			<li><a href="https://www.facebook.com/JohnnieWalkerSouthAfrica" target="_blank"><div class="footer-icon icon-johnniewalker"></div></a></li>
        			<li><a href="https://www.facebook.com/TheSingletonSA" target="_blank"><div class="footer-icon icon-singleton"></div></a></li>
        			<li><a href="https://www.facebook.com/RonZacapaSouthAfrica" target="_blank"><div class="footer-icon icon-ronzappa"></div></a></li>
 	      			<li><a href="https://www.facebook.com/KetelOne " target="_blank"><div class="footer-icon icon-ketel"></div></a></li>
        		</ul>
        	</div>
        </footer>

                
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/vendor/gs/TweenMax.min.js"></script>
        <script src="js/vendor/gs/TimelineMax.min.js"></script>
        <script src="js/vendor/gs/easing/EasePack.min.js"></script>
        <script src="js/vendor/gs/plugins/CSSPlugin.min.js"></script>
        <script src="js/vendor/gs/plugins/DrawSVGPlugin.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/home.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-66449050-1','auto');ga('send','pageview');
        </script>
    </form>
</body>
</html>